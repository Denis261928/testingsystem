package com.friend.testingsystem.api;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkService {
    private static NetworkService mInstance;
    private static final String BASE_URL = "https://test24.com.ua/";
    private Retrofit mRetrofit;

    private static OkHttpClient.Builder okHttpClient = new OkHttpClient.Builder()
            .connectTimeout(1, TimeUnit.MINUTES)
            .readTimeout(30, TimeUnit.SECONDS)
            .writeTimeout(15, TimeUnit.SECONDS);

    private NetworkService() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        okHttpClient.addInterceptor(logging);

        mRetrofit = new Retrofit.Builder()
                .baseUrl(BASE_URL)
                .client(okHttpClient.build())
                .addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized NetworkService getInstance() {
        if (mInstance == null) {
            mInstance = new NetworkService();
        }
        return mInstance;
    }

    public CategoryApi getCategoryApi() {
        return mRetrofit.create(CategoryApi.class);
    }

    public SubCategoryApi getSubCategoryApi() {
        return mRetrofit.create(SubCategoryApi.class);
    }

    public TopicApi getTopicApi() {
        return mRetrofit.create(TopicApi.class);
    }

    public TestApi getTestApi() {
        return mRetrofit.create(TestApi.class);
    }

    public AuthApi getAuthApi() {
        return mRetrofit.create(AuthApi.class);
    }

    public FriendsApi getFriendsApi() {
        return mRetrofit.create(FriendsApi.class);
    }

    public VideoLessonApi getVideoLessonApi() {
        return mRetrofit.create(VideoLessonApi.class);
    }

    public CourseApi getCourseApi() {
        return mRetrofit.create(CourseApi.class);
    }

    public SectionApi getSectionApi() {
        return mRetrofit.create(SectionApi.class);
    }

    public SectionElementApi getSectionElementApi() {
        return mRetrofit.create(SectionElementApi.class);
    }

    public ProfileApi getProfileApi() {
        return mRetrofit.create(ProfileApi.class);
    }

}
