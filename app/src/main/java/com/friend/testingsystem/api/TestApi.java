package com.friend.testingsystem.api;

import com.friend.testingsystem.model.TestHelp;
import com.friend.testingsystem.model.TestingHistory;
import com.friend.testingsystem.model.TestingHistoryItem;
import com.friend.testingsystem.model.Theory;
import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.model.Test;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TestApi {
    @GET("/api/v1/post/test/list/{id}/")
    Call<ResponseListData<Test>> getTestByTopicId(@Path("id") long topicId);

    @GET("/api/v1/post/info/test/list/{id}/")
    Call<List<Theory>> getTheoryByTopicId(@Path("id") long topicId);

    @GET("/api/v1/post/help/test/list/{id}/")
    Call<List<TestHelp>> getTestHelpByTestId(@Path("id") long testId);

    @GET("/api/v1/post/history/test/list/{id}/")
    Call<ResponseListData<TestingHistoryItem>> getTestingHistoryByUserId(@Path("id") long userId,
                                                                         @Query("page") int pageNumber);
    @POST("/api/v1/post/history/test/")
    Call<TestingHistoryItem> createTestingHistory(@Header("Authorization") String authToken,
                                                  @Body TestingHistory testingHistory);
}
