package com.friend.testingsystem.api;

import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.model.SectionElement;
import com.friend.testingsystem.model.SectionElementId;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import retrofit2.http.Streaming;
import retrofit2.http.Url;

public interface SectionElementApi {
    @GET("/api/v1/post/element/section/list/{id}/")
    Call<ResponseListData<SectionElement>> getAllSectionElementListBySectionIdAndPage(@Path("id") long sectionId,
                                                                                      @Query("page") int pageNumber);

    @GET
    @Streaming
    Call<ResponseBody> downloadFile(@Url String fileUrl);

    @POST("/api/v1/post/delete/element/section/")
    Call<String> removeSectionElement(@Body SectionElementId sectionElementId);
}
