package com.friend.testingsystem.api;

import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.model.SubCategory;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SubCategoryApi {
    @GET("/api/v1/post/sub/category/list/{id}/")
    Call<ResponseListData<SubCategory>> getSubCategoryListByCategoryIdAndByPage(@Path("id") long categoryId,
                                                                                @Query("page") int pageNumber);
}
