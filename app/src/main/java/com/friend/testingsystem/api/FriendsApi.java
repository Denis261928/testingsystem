package com.friend.testingsystem.api;

import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.model.User;
import com.friend.testingsystem.model.Friends;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface FriendsApi {
    @GET("/api/v1/user/friends/list/{id}/")
    Call<ResponseListData<Friends>> getFriendsByUserId(@Path("id") long userId,
                                                       @Query("page") int pageNumber);
    @POST("/api/v1/user/add/friends/")
    Call<String> addFriend(@Header("Authorization") String authToken,
                                                  @Body User user);

    @POST("/api/v1/user/remove/friends/")
    Call<String> removeFriend(@Header("Authorization") String authToken,
                                                  @Body User user);
}
