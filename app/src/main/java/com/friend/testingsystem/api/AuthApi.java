package com.friend.testingsystem.api;

import com.friend.testingsystem.model.AuthToken;
import com.friend.testingsystem.model.User;
import com.friend.testingsystem.model.UserInfo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface AuthApi {

    @POST("/api/v1/auth-token/token/login/")
    Call<AuthToken> getAuthToken(@Body User user);

    @GET("/api/v1/user/")
    Call<UserInfo> getUserInfo(@Header("Authorization") String authToken);

    @POST("/api/v1/auth/users/")
    Call<User> registerUser(@Body User user);

}
