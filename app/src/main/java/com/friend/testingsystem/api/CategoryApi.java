package com.friend.testingsystem.api;

import com.friend.testingsystem.model.Category;
import com.friend.testingsystem.model.ResponseListData;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface CategoryApi {
    @GET("/api/v1/post/category/list")
    Call<ResponseListData<Category>> getCategoryListByPage(@Query("page") int pageNumber);
}
