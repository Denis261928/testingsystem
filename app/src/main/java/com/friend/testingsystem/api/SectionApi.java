package com.friend.testingsystem.api;

import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.model.Section;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface SectionApi {
    @GET("/api/v1/post/section/list/{id}/")
    Call<ResponseListData<Section>> getAllSectionListByCourseIdAndPage(@Path("id") long courseId,
                                                                       @Query("page") int pageNumber);

    @POST("/api/v1/post/create/section/")
    Call<Section> createSection(@Header("Authorization") String authToken, @Body Section section);
}
