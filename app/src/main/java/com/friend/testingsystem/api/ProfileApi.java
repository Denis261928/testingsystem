package com.friend.testingsystem.api;

import com.friend.testingsystem.model.BecomeTeacherResponse;
import com.friend.testingsystem.model.Password;
import com.friend.testingsystem.model.UserDetail;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;

public interface ProfileApi {

    @GET("/api/v1/user/profile/{id}/")
    Call<UserDetail> getUserDetailById(@Path("id") long userId);

    @PATCH("/api/v1/user/profile/{id}/")
    Call<UserDetail> updateUserDetail(@Header("Authorization") String authToken,
                                      @Path("id") long userId,
                                      @Body UserDetail userDetail);

    @POST("/api/v1/user/update/password/")
    Call<String> updatePassword(@Header("Authorization") String authToken, @Body Password password);

    @POST("/api/v1/user/status/teacher/create/")
    Call<BecomeTeacherResponse> becomeTeacher(@Header("Authorization") String authToken);
}
