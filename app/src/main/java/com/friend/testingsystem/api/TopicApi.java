package com.friend.testingsystem.api;

import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.model.Topic;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface TopicApi {
    @GET("/api/v1/post/topic/list/{id}/")
    Call<ResponseListData<Topic>> getTopicListBySubCategoryIdAndByPage(
            @Path("id") long subCategoryId,
            @Query("page") int pageNumber);
}
