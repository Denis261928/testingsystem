package com.friend.testingsystem.api;

import com.friend.testingsystem.model.Course;
import com.friend.testingsystem.model.ResponseListData;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface CourseApi {
    @GET("/api/v1/post/courses/list/")
    Call<ResponseListData<Course>> getAllCourseListByPage(@Query("page") int pageNumber);

    @GET("/api/v1/post/search/")
    Call<ResponseListData<Course>> getCourseListBySearchText(@Query("search") String searchText);

    @POST("/api/v1/post/create/courses/")
    Call<Course> createCourse(@Header("Authorization") String authToken, @Body Course course);
}
