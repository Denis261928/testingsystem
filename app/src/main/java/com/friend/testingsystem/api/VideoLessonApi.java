package com.friend.testingsystem.api;

import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.model.VideoLesson;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface VideoLessonApi {
    @GET("/api/v1/post/video/lesson/list/{id}/")
    Call<ResponseListData<VideoLesson>> getVideoLessonListBySubCategoryIdAndByPage(@Path("id") long subCategoryId,
                                                                                   @Query("page") int pageNumber);
}
