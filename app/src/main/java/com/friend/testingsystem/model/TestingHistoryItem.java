package com.friend.testingsystem.model;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Objects;

public class TestingHistoryItem implements Serializable, Parcelable {
    private long id;
    private Topic topic;
    @SerializedName("count_correct_answer")
    private int correctAnswerCount;
    @SerializedName("count_all_answer")
    private int allAnswerCount;
    @SerializedName("percent")
    private int percentageScore;
    @SerializedName("data_time")
    private String dateString;
    @SerializedName("user")
    private long userId;


    protected TestingHistoryItem(Parcel in) {
        id = in.readLong();
        topic = in.readParcelable(Topic.class.getClassLoader());
        correctAnswerCount = in.readInt();
        allAnswerCount = in.readInt();
        percentageScore = in.readInt();
        dateString = in.readString();
        userId = in.readLong();
    }

    public static final Creator<TestingHistoryItem> CREATOR = new Creator<TestingHistoryItem>() {
        @Override
        public TestingHistoryItem createFromParcel(Parcel in) {
            return new TestingHistoryItem(in);
        }

        @Override
        public TestingHistoryItem[] newArray(int size) {
            return new TestingHistoryItem[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public int getCorrectAnswerCount() {
        return correctAnswerCount;
    }

    public void setCorrectAnswerCount(int correctAnswerCount) {
        this.correctAnswerCount = correctAnswerCount;
    }

    public int getAllAnswerCount() {
        return allAnswerCount;
    }

    public void setAllAnswerCount(int allAnswerCount) {
        this.allAnswerCount = allAnswerCount;
    }

    public int getPercentageScore() {
        return percentageScore;
    }

    public void setPercentageScore(int percentageScore) {
        this.percentageScore = percentageScore;
    }

    public String getDateString() {
        return dateString;
    }

    public void setDateString(String dateString) {
        this.dateString = dateString;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestingHistoryItem that = (TestingHistoryItem) o;
        return id == that.id &&
                correctAnswerCount == that.correctAnswerCount &&
                allAnswerCount == that.allAnswerCount &&
                percentageScore == that.percentageScore &&
                userId == that.userId &&
                topic.equals(that.topic) &&
                dateString.equals(that.dateString);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(id, topic, correctAnswerCount, allAnswerCount, percentageScore, dateString, userId);
    }

    @Override
    public String toString() {
        return "TestingHistory{" +
                "id=" + id +
                ", topic=" + topic +
                ", correctAnswerCount=" + correctAnswerCount +
                ", allAnswerCount=" + allAnswerCount +
                ", percentageScore=" + percentageScore +
                ", dateString='" + dateString + '\'' +
                ", userId=" + userId +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeParcelable(topic, i);
        parcel.writeInt(correctAnswerCount);
        parcel.writeInt(allAnswerCount);
        parcel.writeInt(percentageScore);
        parcel.writeString(dateString);
        parcel.writeLong(userId);
    }
}
