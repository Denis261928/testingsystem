package com.friend.testingsystem.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserName implements Serializable {
    private long id;
    @SerializedName("username")
    private String userName;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    @Override
    public String toString() {
        return "UserName{" +
                "id=" + id +
                ", userName='" + userName + '\'' +
                '}';
    }
}
