package com.friend.testingsystem.model;

import android.os.Build;

import androidx.annotation.RequiresApi;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Objects;

public class VideoLesson implements Serializable {
    private long id;
    private String name;
    @SerializedName("url")
    private String videoId;
    @SerializedName("sub_category")
    private long subCategoryId;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getVideoId() {
        return videoId;
    }

    public void setVideoId(String videoId) {
        this.videoId = videoId;
    }

    public long getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(long subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    @Override
    public String toString() {
        return "VideoLesson{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", videoId='" + videoId + '\'' +
                ", subCategoryId=" + subCategoryId +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VideoLesson that = (VideoLesson) o;
        return id == that.id &&
                subCategoryId == that.subCategoryId &&
                name.equals(that.name) &&
                videoId.equals(that.videoId);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(id, name, videoId, subCategoryId);
    }
}
