package com.friend.testingsystem.model;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Objects;

public class TestingHistory implements Serializable, Parcelable {
    @SerializedName("topic")
    private long topicId;
    @SerializedName("count_correct_answer")
    private int correctAnswerCount;
    @SerializedName("count_all_answer")
    private int allAnswerCount;
    @SerializedName("percent")
    private int percentageScore;

    public TestingHistory(long topicId, int correctAnswerCount, int allAnswerCount, int percentageScore) {
        this.topicId = topicId;
        this.correctAnswerCount = correctAnswerCount;
        this.allAnswerCount = allAnswerCount;
        this.percentageScore = percentageScore;
    }

    public TestingHistory() { }

    protected TestingHistory(Parcel in) {
        topicId = in.readLong();
        correctAnswerCount = in.readInt();
        allAnswerCount = in.readInt();
        percentageScore = in.readInt();
    }

    public static final Creator<TestingHistory> CREATOR = new Creator<TestingHistory>() {
        @Override
        public TestingHistory createFromParcel(Parcel in) {
            return new TestingHistory(in);
        }

        @Override
        public TestingHistory[] newArray(int size) {
            return new TestingHistory[size];
        }
    };

    public long getTopicId() {
        return topicId;
    }

    public void setTopicId(long topicId) {
        this.topicId = topicId;
    }

    public int getCorrectAnswerCount() {
        return correctAnswerCount;
    }

    public void setCorrectAnswerCount(int correctAnswerCount) {
        this.correctAnswerCount = correctAnswerCount;
    }

    public int getAllAnswerCount() {
        return allAnswerCount;
    }

    public void setAllAnswerCount(int allAnswerCount) {
        this.allAnswerCount = allAnswerCount;
    }

    public int getPercentageScore() {
        return percentageScore;
    }

    public void setPercentageScore(int percentageScore) {
        this.percentageScore = percentageScore;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestingHistory that = (TestingHistory) o;
        return topicId == that.topicId &&
                correctAnswerCount == that.correctAnswerCount &&
                allAnswerCount == that.allAnswerCount &&
                percentageScore == that.percentageScore;
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(topicId, correctAnswerCount, allAnswerCount, percentageScore);
    }

    @Override
    public String toString() {
        return "TestingHistory{" +
                "topicId=" + topicId +
                ", correctAnswerCount=" + correctAnswerCount +
                ", allAnswerCount=" + allAnswerCount +
                ", percentageScore=" + percentageScore +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(topicId);
        parcel.writeInt(correctAnswerCount);
        parcel.writeInt(allAnswerCount);
        parcel.writeInt(percentageScore);
    }
}
