package com.friend.testingsystem.model;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Task implements Serializable, Parcelable {
    private long id;
    @SerializedName("others_answer")
    private List<Answer> otherAnswers;
    @SerializedName("correct_answer")
    private List<Answer> correctAnswers;
    private String img;
    private String question;
    private String help;

    protected Task(Parcel in) {
        id = in.readLong();
        otherAnswers = in.createTypedArrayList(Answer.CREATOR);
        correctAnswers = in.createTypedArrayList(Answer.CREATOR);
        img = in.readString();
        question = in.readString();
        help = in.readString();
    }

    public static final Creator<Task> CREATOR = new Creator<Task>() {
        @Override
        public Task createFromParcel(Parcel in) {
            return new Task(in);
        }

        @Override
        public Task[] newArray(int size) {
            return new Task[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public List<Answer> getOtherAnswers() {
        return otherAnswers;
    }

    public void setOtherAnswers(List<Answer> otherAnswers) {
        this.otherAnswers = otherAnswers;
    }

    public List<Answer> getCorrectAnswers() {
        return correctAnswers;
    }

    public void setCorrectAnswers(List<Answer> correctAnswers) {
        this.correctAnswers = correctAnswers;
    }

    public String getImg() {
        return img;
    }

    public void setImg(String img) {
        this.img = img;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getHelp() {
        return help;
    }

    public void setHelp(String help) {
        this.help = help;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return id == task.id &&
                otherAnswers.equals(task.otherAnswers) &&
                correctAnswers.equals(task.correctAnswers) &&
                img.equals(task.img) &&
                question.equals(task.question) &&
                help.equals(task.help);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(id, otherAnswers, correctAnswers, img, question, help);
    }

    @Override
    public String toString() {
        return "Task{" +
                "id=" + id +
                ", otherAnswers=" + otherAnswers +
                ", correctAnswers=" + correctAnswers +
                ", img='" + img + '\'' +
                ", question='" + question + '\'' +
                ", help='" + help + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeTypedList(otherAnswers);
        parcel.writeTypedList(correctAnswers);
        parcel.writeString(img);
        parcel.writeString(question);
        parcel.writeString(help);
    }
}
