package com.friend.testingsystem.model;

import com.google.gson.annotations.SerializedName;

public class SectionElementId {
    @SerializedName("element")
    private long id;

    public SectionElementId(long id) {
        this.id = id;
    }

    public SectionElementId() { }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "SectionElementId{" +
                "id=" + id +
                '}';
    }
}
