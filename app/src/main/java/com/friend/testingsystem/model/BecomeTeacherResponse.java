package com.friend.testingsystem.model;

import com.google.gson.annotations.SerializedName;

public class BecomeTeacherResponse {
    private long id;
    private boolean done;
    @SerializedName("user")
    private long userId;

    public long getId() {
        return id;
    }

    public boolean isDone() {
        return done;
    }

    public void setDone(boolean done) {
        this.done = done;
    }

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    @Override
    public String toString() {
        return "BecomeTeacherResponse{" +
                "id=" + id +
                ", done=" + done +
                ", userId=" + userId +
                '}';
    }
}
