package com.friend.testingsystem.model;

import com.google.gson.annotations.SerializedName;

public class AuthToken {
    @SerializedName("auth_token")
    private String authToken;

    public AuthToken(String authToken) {
        this.authToken = authToken;
    }

    public AuthToken() {
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    @Override
    public String toString() {
        return "AuthToken{" +
                "authToken='" + authToken + '\'' +
                '}';
    }
}
