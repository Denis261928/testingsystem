package com.friend.testingsystem.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class UserDetail implements Serializable {
    private long id;
    private UserName user;
    @SerializedName("first_name")
    private String firstName;
    @SerializedName("last_name")
    private String lastName;
    @SerializedName("phone")
    private String phoneNumber;
    private String email;
    @SerializedName("teacher")
    private boolean isTeacher;
    @SerializedName("request_teacher")
    private boolean requestTeacher;

    public long getId() {
        return id;
    }

    public UserName getUser() {
        return user;
    }

    public void setUser(UserName user) {
        this.user = user;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPhoneNumber() {
        return phoneNumber;
    }

    public void setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public boolean isTeacher() {
        return isTeacher;
    }

    public void setTeacher(boolean teacher) {
        isTeacher = teacher;
    }

    public boolean isRequestTeacher() {
        return requestTeacher;
    }

    public void setRequestTeacher(boolean requestTeacher) {
        this.requestTeacher = requestTeacher;
    }

    @Override
    public String toString() {
        return "UserDetail{" +
                "id=" + id +
                ", user=" + user +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", phoneNumber='" + phoneNumber + '\'' +
                ", email='" + email + '\'' +
                ", isTeacher=" + isTeacher +
                ", requestTeacher=" + requestTeacher +
                '}';
    }
}
