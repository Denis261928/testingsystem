package com.friend.testingsystem.model;

public class EncodedToken {
    private String refresh;
    private String access;

    public EncodedToken(String refresh, String access) {
        this.refresh = refresh;
        this.access = access;
    }

    public EncodedToken(String refresh) {
        this.refresh = refresh;
    }

    public EncodedToken() {
    }

    public String getRefresh() {
        return refresh;
    }

    public void setRefresh(String refresh) {
        this.refresh = refresh;
    }

    public String getAccess() {
        return access;
    }

    public void setAccess(String access) {
        this.access = access;
    }

    @Override
    public String toString() {
        return "EncodedToken{" +
                "refresh='" + refresh +
                ", access='" + access +
                '}';
    }
}
