package com.friend.testingsystem.model;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Objects;

public class Topic implements Serializable, Parcelable {
    private long id;
    private String name;
    @SerializedName("sub_category")
    private SubCategory subCategory;
    @SerializedName("open")
    private boolean isTestOpen;
    @SerializedName("password_topic")
    private String testPassword;
    @SerializedName("exam")
    private boolean isExam;
    @SerializedName("max_prize")
    private double maxPrize;

    protected Topic(Parcel in) {
        id = in.readLong();
        name = in.readString();
        subCategory = in.readParcelable(SubCategory.class.getClassLoader());
        isTestOpen = in.readByte() != 0;
        testPassword = in.readString();
        isExam = in.readByte() != 0;
        maxPrize = in.readDouble();
    }

    public static final Creator<Topic> CREATOR = new Creator<Topic>() {
        @Override
        public Topic createFromParcel(Parcel in) {
            return new Topic(in);
        }

        @Override
        public Topic[] newArray(int size) {
            return new Topic[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SubCategory getSubCategory() {
        return subCategory;
    }

    public void setSubCategory(SubCategory subCategory) {
        this.subCategory = subCategory;
    }

    public boolean isTestOpen() {
        return isTestOpen;
    }

    public void setTestOpen(boolean testOpen) {
        isTestOpen = testOpen;
    }

    public String getTestPassword() {
        return testPassword;
    }

    public void setTestPassword(String testPassword) {
        this.testPassword = testPassword;
    }

    public boolean isExam() {
        return isExam;
    }

    public void setExam(boolean exam) {
        isExam = exam;
    }

    public double getMaxPrize() {
        return maxPrize;
    }

    public void setMaxPrize(double maxPrize) {
        this.maxPrize = maxPrize;
    }

    @Override
    public String toString() {
        return "Topic{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", subCategory=" + subCategory +
                ", isTestOpen=" + isTestOpen +
                ", testPassword='" + testPassword + '\'' +
                ", isExam=" + isExam +
                ", maxPrize=" + maxPrize +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Topic topic = (Topic) o;
        return id == topic.id &&
                isTestOpen == topic.isTestOpen &&
                isExam == topic.isExam &&
                Double.compare(topic.maxPrize, maxPrize) == 0 &&
                name.equals(topic.name) &&
                subCategory.equals(topic.subCategory) &&
                testPassword.equals(topic.testPassword);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(id, name, subCategory, isTestOpen, testPassword, isExam, maxPrize);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(name);
        parcel.writeParcelable(subCategory, i);
        parcel.writeByte((byte) (isTestOpen ? 1 : 0));
        parcel.writeString(testPassword);
        parcel.writeByte((byte) (isExam ? 1 : 0));
        parcel.writeDouble(maxPrize);
    }
}
