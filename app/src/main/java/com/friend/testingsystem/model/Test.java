package com.friend.testingsystem.model;

import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import androidx.annotation.RequiresApi;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;
import java.util.Objects;

public class Test implements Serializable, Parcelable {
    private long id;
    private Topic topic;
    @SerializedName("task")
    private List<Task> tasks;
    @SerializedName("general_question")
    private String generalQuestion;
    @SerializedName("open_answer_test")
    private boolean isTestWithOpenAnswers;
    @SerializedName("count_answer")
    private int answerCount;

    protected Test(Parcel in) {
        id = in.readLong();
        topic = in.readParcelable(Topic.class.getClassLoader());
        tasks = in.createTypedArrayList(Task.CREATOR);
        generalQuestion = in.readString();
        isTestWithOpenAnswers = in.readByte() != 0;
        answerCount = in.readInt();
    }

    public static final Creator<Test> CREATOR = new Creator<Test>() {
        @Override
        public Test createFromParcel(Parcel in) {
            return new Test(in);
        }

        @Override
        public Test[] newArray(int size) {
            return new Test[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Topic getTopic() {
        return topic;
    }

    public void setTopic(Topic topic) {
        this.topic = topic;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }

    public String getGeneralQuestion() {
        return generalQuestion;
    }

    public void setGeneralQuestion(String generalQuestion) {
        this.generalQuestion = generalQuestion;
    }

    public boolean isTestWithOpenAnswers() {
        return isTestWithOpenAnswers;
    }

    public void setTestWithOpenAnswers(boolean testWithOpenAnswers) {
        isTestWithOpenAnswers = testWithOpenAnswers;
    }

    public int getAnswerCount() {
        return answerCount;
    }

    public void setAnswerCount(int answerCount) {
        this.answerCount = answerCount;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Test test = (Test) o;
        return id == test.id &&
                isTestWithOpenAnswers == test.isTestWithOpenAnswers &&
                answerCount == test.answerCount &&
                topic.equals(test.topic) &&
                tasks.equals(test.tasks) &&
                generalQuestion.equals(test.generalQuestion);
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(id, topic, tasks, generalQuestion, isTestWithOpenAnswers, answerCount);
    }

    @Override
    public String toString() {
        return "Test{" +
                "id=" + id +
                ", topic=" + topic +
                ", tasks=" + tasks +
                ", generalQuestion='" + generalQuestion + '\'' +
                ", isTestWithOpenAnswers=" + isTestWithOpenAnswers +
                ", answerCount=" + answerCount +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeParcelable(topic, i);
        parcel.writeTypedList(tasks);
        parcel.writeString(generalQuestion);
        parcel.writeByte((byte) (isTestWithOpenAnswers ? 1 : 0));
        parcel.writeInt(answerCount);
    }
}
