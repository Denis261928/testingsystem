package com.friend.testingsystem.model;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

public class Password implements Serializable {
    @SerializedName("password")
    private String oldPassword;
    @SerializedName("new")
    private String newPassword;

    public Password(String oldPassword, String newPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public Password() { }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    @Override
    public String toString() {
        return "Password{" +
                "oldPassword='" + oldPassword + '\'' +
                ", newPassword='" + newPassword + '\'' +
                '}';
    }
}
