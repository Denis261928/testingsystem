package com.friend.testingsystem.model;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Objects;

public class Theory implements Serializable, Parcelable {
    private long id;
    @SerializedName("img")
    private String image;
    private String text;
    @SerializedName("topic")
    private long topicId;

    protected Theory(Parcel in) {
        id = in.readLong();
        image = in.readString();
        text = in.readString();
        topicId = in.readLong();
    }

    public static final Creator<Theory> CREATOR = new Creator<Theory>() {
        @Override
        public Theory createFromParcel(Parcel in) {
            return new Theory(in);
        }

        @Override
        public Theory[] newArray(int size) {
            return new Theory[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getTopicId() {
        return topicId;
    }

    public void setTopicId(long topicId) {
        this.topicId = topicId;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Theory theory = (Theory) o;
        return id == theory.id &&
                topicId == theory.topicId &&
                image.equals(theory.image) &&
                text.equals(theory.text);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(id, image, text, topicId);
    }

    @Override
    public String toString() {
        return "Theory{" +
                "id=" + id +
                ", image='" + image + '\'' +
                ", text='" + text + '\'' +
                ", topicId=" + topicId +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(image);
        parcel.writeString(text);
        parcel.writeLong(topicId);
    }
}
