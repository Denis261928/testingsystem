package com.friend.testingsystem.model;

import android.annotation.TargetApi;
import android.os.Build;
import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.Objects;

public class TestHelp implements Serializable, Parcelable {
    private long id;
    @SerializedName("img")
    private String image;
    private String text;
    @SerializedName("test")
    private long testId;

    protected TestHelp(Parcel in) {
        id = in.readLong();
        image = in.readString();
        text = in.readString();
        testId = in.readLong();
    }

    public static final Creator<TestHelp> CREATOR = new Creator<TestHelp>() {
        @Override
        public TestHelp createFromParcel(Parcel in) {
            return new TestHelp(in);
        }

        @Override
        public TestHelp[] newArray(int size) {
            return new TestHelp[size];
        }
    };

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getTestId() {
        return testId;
    }

    public void setTestId(long testId) {
        this.testId = testId;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        TestHelp testHelp = (TestHelp) o;
        return id == testHelp.id &&
                testId == testHelp.testId &&
                image.equals(testHelp.image) &&
                text.equals(testHelp.text);
    }

    @TargetApi(Build.VERSION_CODES.KITKAT)
    @Override
    public int hashCode() {
        return Objects.hash(id, image, text, testId);
    }

    @Override
    public String toString() {
        return "TestHelp{" +
                "id=" + id +
                ", image='" + image + '\'' +
                ", text='" + text + '\'' +
                ", testId=" + testId +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeLong(id);
        parcel.writeString(image);
        parcel.writeString(text);
        parcel.writeLong(testId);
    }
}
