package com.friend.testingsystem.model;

public class DecodedToken {
    private String token_type;
    private long exp;
    private String jti;
    private long user_id;

    public DecodedToken(String token_type, long exp, String jti, long user_id) {
        this.token_type = token_type;
        this.exp = exp;
        this.jti = jti;
        this.user_id = user_id;
    }

    public DecodedToken() {
    }

    public String getTokenType() {
        return token_type;
    }

    public void setTokenType(String token_type) {
        this.token_type = token_type;
    }

    public long getExp() {
        return exp;
    }

    public void setExp(long exp) {
        this.exp = exp;
    }

    public String getJti() {
        return jti;
    }

    public void setJti(String jti) {
        this.jti = jti;
    }

    public long getUserId() {
        return user_id;
    }

    public void setUserId(long user_id) {
        this.user_id = user_id;
    }

    public String getToken_type() {
        return token_type;
    }

    public void setToken_type(String token_type) {
        this.token_type = token_type;
    }

    @Override
    public String toString() {
        return "DecodedToken{" +
                "token_type='" + token_type + '\'' +
                ", exp=" + exp +
                ", jti='" + jti + '\'' +
                ", user_id=" + user_id +
                '}';
    }
}
