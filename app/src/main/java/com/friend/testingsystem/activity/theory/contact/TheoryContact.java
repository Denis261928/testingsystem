package com.friend.testingsystem.activity.theory.contact;

import com.friend.testingsystem.model.Theory;
import com.friend.testingsystem.model.Topic;

import java.util.List;

public interface TheoryContact {
    interface View {
        void showTheory(List<Theory> theoryList);
        void showProgress();
        void hideProgress();
    }

    interface Presenter {
        void init(Topic topic);
    }
}
