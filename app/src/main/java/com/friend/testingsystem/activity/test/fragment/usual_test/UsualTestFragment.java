package com.friend.testingsystem.activity.test.fragment.usual_test;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.test.TestActivity;
import com.friend.testingsystem.activity.test.dialog.test_help.TestHelpDialog;
import com.friend.testingsystem.activity.test.fragment.usual_test.adapter.UsualTestAnswerAdapter;
import com.friend.testingsystem.activity.test.fragment.usual_test.contact.UsualTestContact;
import com.friend.testingsystem.activity.test.contact.TestContact;
import com.friend.testingsystem.activity.test.model.AnswerWithCheckBox;
import com.friend.testingsystem.activity.test.model.TaskWithCheckBoxes;
import com.friend.testingsystem.activity.test.fragment.usual_test.presenter.UsualTestPresenter;
import com.friend.testingsystem.activity.test.model.TestWithCheckBoxes;
import com.friend.testingsystem.activity.test.model.UserAnswer;
import com.friend.testingsystem.model.Answer;
import com.friend.testingsystem.model.Test;
import com.friend.testingsystem.repository.TestDataRepository;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class UsualTestFragment extends Fragment implements UsualTestContact.View, TestContact.TestFragment {
    public static final String TAG = "UsualTestFragmentLog";

    private static final String POSITION = "position_key";
    private static final String TEST = "test_key";
    private static final String TEST_LIST = "test_list_key";
    private static final String IS_EXAM = "is_test_exam_key";

    private ImageView showHelpButton;
    private LinearLayout generalQuestionContainer;
    private TextView testNumberText, questionText, correctAnswerCountText, generalQuestionText;
    private ImageView questionImage;
    private RelativeLayout questionImgContainer;
    private RecyclerView answersRecyclerView;
    private TableLayout answersTableLayout;
    private Button saveTestButton;

    private int currentPosition;
    private boolean isTestExam;

    private UsualTestPresenter presenter;

    public static UsualTestFragment getNewInstance(int position, Test test, List<Test> testList, boolean isTestExam) {
        UsualTestFragment usualTestFragment = new UsualTestFragment();
        Bundle args = new Bundle();
        args.putInt(POSITION, position);
        args.putParcelable(TEST, test);
        args.putParcelableArrayList(TEST_LIST, (ArrayList<? extends Parcelable>) testList);
        args.putBoolean(IS_EXAM, isTestExam);
        usualTestFragment.setArguments(args);

        return usualTestFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_usual_test, container, false);

        showHelpButton = view.findViewById(R.id.show_help_icon);
        testNumberText = view.findViewById(R.id.test_number_text);
        generalQuestionContainer = view.findViewById(R.id.general_question_container);
        generalQuestionText = view.findViewById(R.id.general_question_text);
        questionText = view.findViewById(R.id.question_text);
        correctAnswerCountText = view.findViewById(R.id.correct_answer_count_text);
        questionImage = view.findViewById(R.id.question_img);
        questionImgContainer = view.findViewById(R.id.question_img_container);
        answersRecyclerView = view.findViewById(R.id.answers_list);
        answersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        answersTableLayout = view.findViewById(R.id.answers_table_layout);
        saveTestButton = view.findViewById(R.id.save_test);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new UsualTestPresenter(this);
        currentPosition = getArguments().getInt(POSITION);
        isTestExam = getArguments().getBoolean(IS_EXAM);
        presenter.init(currentPosition, (Test) getArguments().getParcelable(TEST),
                getArguments().<Test>getParcelableArrayList(TEST_LIST), isTestExam);
        currentPosition = getArguments().getInt(POSITION);

        presenter.setButtonStatus();
        if (TestDataRepository.isTestFinished()) {
            presenter.saveTestAndGetScore();
        }

        for (int i = 0; i < answersTableLayout.getChildCount(); i++) {
            TableRow raw = (TableRow) answersTableLayout.getChildAt(i);
            final int rowIndex = i;
            for (int j = 0; j < raw.getChildCount(); j++) {
                View checkBoxView = raw.getChildAt(j);
                final int answerIndex = j;
                checkBoxView.setOnClickListener(view1 -> {
                    if (presenter.isTestActive() && rowIndex != 0) {
                        presenter.answerClick(answerIndex);
                    }
                });
            }
        }

        showHelpButton.setOnClickListener(view12 -> presenter.showHelp());

        saveTestButton.setOnClickListener(view13 -> {
            if (isTestExam) {
                TestActivity activity = (TestActivity) getActivity();
                activity.goNextTest();
            }
            else {
                if (presenter.isTestActive()) {
                    presenter.saveTestAndGetScore();
                }
                else {
                    TestActivity activity = (TestActivity) getActivity();
                    activity.goNextTest();
                }
            }
        });

    }

    public UsualTestPresenter getPresenter() {
        return presenter;
    }

    @Override
    public void showGeneralQuestion(String generalQuestion) {
        if (generalQuestion == null || generalQuestion.equals(""))
            generalQuestionContainer.setVisibility(View.GONE);
        else
            generalQuestionText.setText(generalQuestion);
    }

    @Override
    public void showQuestion(String testNumberText, String questionText, String correctAnswerCountText, String questionImage) {
        this.testNumberText.setText(testNumberText);
        if (questionText == null || questionText.equals("") || questionText.equals("null"))
            this.questionText.setVisibility(View.GONE);
        else
            this.questionText.setText(questionText);
        this.correctAnswerCountText.setText(correctAnswerCountText);
        if (questionImage != null) {
            questionImgContainer.setVisibility(View.VISIBLE);
            Picasso.get().load(questionImage).into(this.questionImage);
        }
    }

    @Override
    public void showAnswers(List<Answer> answerList) {
        UsualTestAnswerAdapter adapter = new UsualTestAnswerAdapter(getActivity(), answerList);
        answersRecyclerView.setAdapter(adapter);
    }

    @Override
    public void showAnswersTable(List<Answer> answerList, int correctAnswerCount) {
            TableRow headRow = new TableRow(getActivity());
            Log.d("111111", headRow.getId()+"");
            for (int i = 0; i < answerList.size(); i++) {
                View itemView = LayoutInflater.from(getActivity())
                        .inflate(R.layout.item_check_answer_number, null, false);
                TextView answerNumberText = itemView.findViewById(R.id.answer_number);
                answerNumberText.setText(TestActivity.alphabetIndexes[i]);
                headRow.addView(itemView);
            }
            answersTableLayout.addView(headRow);

            TaskWithCheckBoxes taskWithCheckBoxes = new TaskWithCheckBoxes(correctAnswerCount);
            TableRow checkBoxesRow = new TableRow(getActivity());
            for (int i = 0; i < answerList.size(); i++) {
                View itemView = LayoutInflater.from(getActivity())
                        .inflate(R.layout.item_check_test, null, false);
                RelativeLayout iconCheckBoxAnswerContainer = itemView.findViewById(R.id.icon_check_answer_container);
                ImageView iconCheckBoxAnswer = itemView.findViewById(R.id.icon_check_answer);
                checkBoxesRow.addView(itemView);

                AnswerWithCheckBox answerWithCheckBox =
                        new AnswerWithCheckBox(iconCheckBoxAnswerContainer, iconCheckBoxAnswer, answerList.get(i));
                for (int j = 0; j < TestDataRepository.getUserTestList().get(currentPosition)
                                .getUserTaskList().get(0)
                                .getUserAnswerList().size(); j++) {
                    UserAnswer userAnswer = TestDataRepository.getUserTestList().get(currentPosition)
                            .getUserTaskList().get(0)
                            .getUserAnswerList().get(j);
                    if (userAnswer.getAnswer().getId() == answerList.get(i).getId()) {
                        if (userAnswer.getStatusCode() == 0) {
                            if (userAnswer.isChecked()) {
                                answerWithCheckBox.makeAnswerChecked();
                                answerWithCheckBox.setChecked(true);
                            }
                        }
                        else {
                            if (userAnswer.getStatusCode() == 3) {
                                answerWithCheckBox.makeCorrectAnswer();
                                answerWithCheckBox.setChecked(true);
                            }
                            else if (userAnswer.getStatusCode() == 2) {
                                answerWithCheckBox.makeAnswerChecked();
                            }
                            else if (userAnswer.getStatusCode() == 1) {
                                answerWithCheckBox.makeIncorrectAnswer();
                                answerWithCheckBox.setChecked(true);
                            }
                        }
                        presenter.setButtonStatus();
                    }
                }
                taskWithCheckBoxes.addAnswerWithCheckBox(answerWithCheckBox);
            }
            answersTableLayout.addView(checkBoxesRow);
            List<TaskWithCheckBoxes> taskWithCheckBoxesList = new ArrayList<>();
            taskWithCheckBoxesList.add(taskWithCheckBoxes);
            TestWithCheckBoxes testWithCheckBoxes = new TestWithCheckBoxes(taskWithCheckBoxesList);
            presenter.setTestWithCheckBoxes(testWithCheckBoxes);
    }

    @Override
    public void setSaveButtonEnabled(boolean enabled) {
        if (saveTestButton.isEnabled() && !enabled) {
            saveTestButton.setEnabled(false);
            saveTestButton.setBackgroundResource(R.drawable.background_button_disable);
        }
        else if (!saveTestButton.isEnabled() && enabled){
            saveTestButton.setEnabled(true);
            saveTestButton.setBackgroundResource(R.drawable.background_button);
        }
    }

    @Override
    public void setSaveButtonExam() {
        saveTestButton.setText("Наступне");
        saveTestButton.setBackgroundResource(R.drawable.background_button);
        saveTestButton.setEnabled(true);
        if (TestDataRepository.isTestFinished())
            showHelpButton.setVisibility(View.VISIBLE);
        else
            showHelpButton.setVisibility(View.GONE);
    }

    @Override
    public void onTestSaved() {
        saveTestButton.setEnabled(true);
        saveTestButton.setBackgroundResource(R.drawable.background_button);
        saveTestButton.setText("Наступне");
        if (showHelpButton.getVisibility() == View.GONE)
            showHelpButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void showTestHelpDialog(long testId) {
        TestHelpDialog testHelpDialog = new TestHelpDialog(getActivity(), testId);
        testHelpDialog.showDialog();
    }


    @Override
    public int saveTestAndGetScore() {
        return presenter.saveTestAndGetScore();
    }


}
