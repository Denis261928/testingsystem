package com.friend.testingsystem.activity.section_element.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.section_element.SectionElementActivity;
import com.friend.testingsystem.activity.section_element.helper.FileIcon;
import com.friend.testingsystem.model.SectionElement;
import com.friend.testingsystem.repository.UserDataRepository;

import java.util.List;

public class SectionElementAdapter extends BaseAdapter {
    private Context context;
    private UserDataRepository repository;
    private List<SectionElement> sectionElementList;
    private OnSectionElementListener listener;

    public SectionElementAdapter(Context context, UserDataRepository repository,
                                 List<SectionElement> sectionElementList, OnSectionElementListener listener) {
        this.context = context;
        this.repository = repository;
        this.sectionElementList = sectionElementList;
        this.listener = listener;
    }

    public void addListItemsToAdapter(List<SectionElement> list) {
        sectionElementList.addAll(list);
        notifyDataSetChanged();
    }

    @SuppressLint("LongLogTag")
    public void clearSectionElementList() {
        sectionElementList.clear();
        notifyDataSetChanged();
        Log.d(SectionElementActivity.TAG, "clear");
    }

    public List<SectionElement> getSectionElementList() {
        return sectionElementList;
    }

    @Override
    public int getCount() {
        return sectionElementList.size();
    }

    @Override
    public Object getItem(int i) {
        return sectionElementList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        final int position = i;
        View v = View.inflate(context, R.layout.item_section_element, null);

        ImageView sectionElementImage = v.findViewById(R.id.section_element_image);
        TextView sectionElementNameText = v.findViewById(R.id.section_element_name_text);
        RelativeLayout sectionElementRemoveButton = v.findViewById(R.id.section_element_remove_container);
        RelativeLayout sectionElementDownloadButton = v.findViewById(R.id.section_element_download_image_container);

        if (!repository.getUserDetail().isTeacher())
            sectionElementRemoveButton.setVisibility(View.GONE);

        sectionElementNameText.setText(sectionElementList.get(i).getName());

        String fileUrl = sectionElementList.get(i).getFileUrl();
        String fileExtension = fileUrl.substring(fileUrl.lastIndexOf(".")).replace(".", "");

        FileIcon.setFileIconByFileExtension(sectionElementImage, fileExtension);

        sectionElementRemoveButton.setOnClickListener(view1 -> listener.onRemoveClick(sectionElementList.get(position)));

        sectionElementDownloadButton.setOnClickListener(view1 -> listener.onDownloadClick(sectionElementList.get(position)));

        return v;
    }

    public interface OnSectionElementListener {
        void onRemoveClick(SectionElement sectionElement);
        void onDownloadClick(SectionElement sectionElement);
    }
}
