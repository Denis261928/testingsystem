package com.friend.testingsystem.activity.test.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TaskWithCheckBoxes implements Serializable {
    private List<AnswerWithCheckBox> answerWithCheckBoxList;
    private int correctAnswerCount;

    public TaskWithCheckBoxes(int correctAnswerCount) {
        answerWithCheckBoxList = new ArrayList<>();
        this.correctAnswerCount = correctAnswerCount;
    }

    public void addAnswerWithCheckBox(AnswerWithCheckBox answerWithCheckBox) {
        answerWithCheckBoxList.add(answerWithCheckBox);
    }

    public int getCountOfCheckedAnswer() {
        int count = 0;
        for (AnswerWithCheckBox answerWithCheckBox: answerWithCheckBoxList) {
            if (answerWithCheckBox.isChecked()) {
                count++;
            }
        }
        return count;
    }

    public List<AnswerWithCheckBox> getAnswerWithCheckBoxList() {
        return answerWithCheckBoxList;
    }

    public int getCorrectAnswerCount() {
        return correctAnswerCount;
    }

}
