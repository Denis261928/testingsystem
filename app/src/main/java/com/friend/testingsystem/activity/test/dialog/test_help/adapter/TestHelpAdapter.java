package com.friend.testingsystem.activity.test.dialog.test_help.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.model.TestHelp;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TestHelpAdapter extends RecyclerView.Adapter<TestHelpAdapter.TestHelpViewHolder> {
    private List<TestHelp> testHelpList;

    public TestHelpAdapter(List<TestHelp> testHelpList) {
        this.testHelpList = testHelpList;
    }

    @NonNull
    @Override
    public TestHelpViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_theory, parent, false);

        return new TestHelpViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TestHelpViewHolder holder, int position) {
        holder.bind(testHelpList.get(position));
    }

    @Override
    public int getItemCount() {
        return testHelpList.size();
    }

    static class TestHelpViewHolder extends RecyclerView.ViewHolder {
        // private ImageView theoryItemImage;
        private PhotoView theoryItemImage;
        private TextView theoryItemText;

        public TestHelpViewHolder(@NonNull View itemView) {
            super(itemView);

            theoryItemImage = itemView.findViewById(R.id.theory_item_image);
            theoryItemText = itemView.findViewById(R.id.theory_item_text);
        }

        public void bind(TestHelp testHelp) {
            if (testHelp.getImage() != null) {
                theoryItemImage.setVisibility(View.VISIBLE);
                Picasso.get().load(testHelp.getImage()).into(theoryItemImage);
            }
            if (testHelp.getText() != null && !testHelp.getText().equals("")) {
                theoryItemText.setVisibility(View.VISIBLE);
                theoryItemText.setText(testHelp.getText());
            }
        }
    }
}
