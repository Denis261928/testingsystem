package com.friend.testingsystem.activity.category.data_source;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.Category;
import com.friend.testingsystem.model.ResponseListData;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CategoryDataSource extends PageKeyedDataSource<Integer, Category> {

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, Category> callback) {
        NetworkService.getInstance().getCategoryApi().getCategoryListByPage(1)
                .enqueue(new Callback<ResponseListData<Category>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<Category>> call, Response<ResponseListData<Category>> response) {
                        if (response.body() != null) {
                            callback.onResult(response.body().getResults(), null, 2);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<Category>> call, Throwable t) {
                        Log.d(CategoryActivity.TAG, t.getMessage());
                    }
                });
    }

    @Override
    public void loadBefore(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Category> callback) {
        NetworkService.getInstance().getCategoryApi().getCategoryListByPage(params.key)
                .enqueue(new Callback<ResponseListData<Category>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<Category>> call, Response<ResponseListData<Category>> response) {
                        // Integer key = (params.key > 1) ? params.key - 1 : null;
                        if (response.body() != null) {
                            // Integer key = response.body().getPrevious() != null ? params.key - 1 : null;
                            callback.onResult(response.body().getResults(), params.key - 1);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<Category>> call, Throwable t) {

                    }
                });
    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Category> callback) {
        NetworkService.getInstance().getCategoryApi().getCategoryListByPage(params.key)
                .enqueue(new Callback<ResponseListData<Category>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<Category>> call, Response<ResponseListData<Category>> response) {
                        if (response.body() != null) {
                            // Integer key = response.body().getNext() != null ? params.key + 1 : null;
                            callback.onResult(response.body().getResults(), params.key + 1);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<Category>> call, Throwable t) {

                    }
                });

    }
}