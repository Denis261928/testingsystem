package com.friend.testingsystem.activity.theory;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.CommonActivity;
import com.friend.testingsystem.activity.MainActivity;
import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.activity.theory.adapter.TheoryAdapter;
import com.friend.testingsystem.activity.theory.contact.TheoryContact;
import com.friend.testingsystem.activity.theory.presenter.TheoryPresenter;
import com.friend.testingsystem.dialog.LogoutConfirmDialog;
import com.friend.testingsystem.helper.NetworkUtils;
import com.friend.testingsystem.model.Theory;
import com.friend.testingsystem.model.Topic;
import com.friend.testingsystem.repository.UserDataRepository;

import java.util.List;

public class TheoryActivity extends AppCompatActivity implements TheoryContact.View, CommonActivity {
    public static final String TAG = "TheoryActivityLog";

    private ProgressDialog progressDialog;
    private ImageView loginLogoutButton;
    private LinearLayout theoryContentContainer;
    private RelativeLayout theoryContentContainerEmpty;
    private TextView theoryEmptyMessageText;
    private RecyclerView recyclerView;

    private Topic topic;

    private UserDataRepository repository;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_theory);

        ImageView goBack = findViewById(R.id.go_back);
        loginLogoutButton = findViewById(R.id.login_logout_icon);
        theoryContentContainer = findViewById(R.id.theory_content_container);
        theoryContentContainerEmpty = findViewById(R.id.theory_content_container_empty);
        theoryEmptyMessageText = findViewById(R.id.theory_empty_message_text);
        TextView theoryTopicNameText = findViewById(R.id.theory_topic_name_text);
        recyclerView = findViewById(R.id.theory_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        if (!NetworkUtils.getNetworkConnection(this)) {
            startActivity(new Intent(this, MainActivity.class));
        }

        repository = new UserDataRepository(this);
        TheoryPresenter presenter = new TheoryPresenter(this);
        setLoginLogoutIcon();

        topic = (Topic) getIntent().getSerializableExtra("topic");
        if (topic != null) {
            theoryTopicNameText.setText(topic.getName());
            presenter.init(topic);
        }

        loginLogoutButton.setOnClickListener(view -> loginLogoutClick());

        goBack.setOnClickListener(view -> onBackPressed());
    }

    @Override
    public void showTheory(List<Theory> theoryList) {
        if (theoryList.size() > 0) {
            theoryContentContainer.setVisibility(View.VISIBLE);
            theoryContentContainerEmpty.setVisibility(View.GONE);
            TheoryAdapter adapter = new TheoryAdapter(theoryList);
            recyclerView.setAdapter(adapter);
        }
        else {
            theoryContentContainer.setVisibility(View.GONE);
            theoryContentContainerEmpty.setVisibility(View.VISIBLE);
            theoryEmptyMessageText.setText("У теми \"" + topic.getName() + "\" теорії немає");
        }
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Завантаження теорії");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void loginLogoutClick() {
        if (repository.isUserAuthorized()) {
            LogoutConfirmDialog logoutConfirmDialog = new LogoutConfirmDialog(this);
            logoutConfirmDialog.showDialog("Ви впевнені що хочете вийти з акаунту?");
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    @Override
    public void setLoginLogoutIcon() {
        if (repository.isUserAuthorized())
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_logout));
        else
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_login));
    }

    @Override
    public void logout() {
        repository.logoutUser();
        setLoginLogoutIcon();
        startActivity(new Intent(this, CategoryActivity.class));
    }

}
