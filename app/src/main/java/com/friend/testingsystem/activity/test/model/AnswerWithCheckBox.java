package com.friend.testingsystem.activity.test.model;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.friend.testingsystem.R;
import com.friend.testingsystem.model.Answer;

import java.io.Serializable;

public class AnswerWithCheckBox implements Serializable {
    private RelativeLayout iconCheckBoxContainer;
    private ImageView iconCheckBoxAnswer;
    private Answer answer;
    private boolean checked;

    public AnswerWithCheckBox(RelativeLayout iconCheckBoxContainer, ImageView iconCheckBoxAnswer, Answer answer) {
        this.iconCheckBoxContainer = iconCheckBoxContainer;
        this.iconCheckBoxAnswer = iconCheckBoxAnswer;
        this.answer = answer;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public RelativeLayout getIconCheckBoxContainer() {
        return iconCheckBoxContainer;
    }

    public void setIconCheckBoxContainer(RelativeLayout iconCheckBoxContainer) {
        this.iconCheckBoxContainer = iconCheckBoxContainer;
    }

    public ImageView getIconCheckBoxAnswer() {
        return iconCheckBoxAnswer;
    }

    public void setIconCheckBoxAnswer(ImageView iconCheckBoxAnswer) {
        this.iconCheckBoxAnswer = iconCheckBoxAnswer;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public void click() {
        if (checked) {
            iconCheckBoxAnswer.setVisibility(View.GONE);
            this.checked = false;
        }
        else {
            iconCheckBoxAnswer.setVisibility(View.VISIBLE);
            iconCheckBoxAnswer.setImageResource(R.drawable.ic_check);
            this.checked = true;
        }
    }

    public void makeCorrectAnswer() {
        iconCheckBoxContainer.setBackgroundResource(R.drawable.background_check_test_green);

        iconCheckBoxAnswer.setVisibility(View.VISIBLE);
        iconCheckBoxAnswer.setImageResource(R.drawable.ic_check);
    }

    public void makeAnswerChecked() {
        iconCheckBoxAnswer.setVisibility(View.VISIBLE);
        iconCheckBoxAnswer.setImageResource(R.drawable.ic_check);
    }

    public void makeIncorrectAnswer() {
        iconCheckBoxAnswer.setVisibility(View.VISIBLE);
        iconCheckBoxAnswer.setImageResource(R.drawable.ic_check_cancel);
    }

}
