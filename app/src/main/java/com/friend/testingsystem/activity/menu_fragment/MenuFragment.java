package com.friend.testingsystem.activity.menu_fragment;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.activity.course.CourseActivity;
import com.friend.testingsystem.activity.friends.FriendsActivity;
import com.friend.testingsystem.activity.menu_fragment.dialog.AuthConfirmDialogWithMessage;
import com.friend.testingsystem.activity.profile.ProfileActivity;
import com.friend.testingsystem.activity.testing_history.TestingHistoryActivity;
import com.friend.testingsystem.activity.menu_fragment.contact.MenuContact;
import com.friend.testingsystem.activity.menu_fragment.presenter.MenuPresenter;
import com.friend.testingsystem.helper.ActivityEnum;
import com.friend.testingsystem.repository.UserDataRepository;

public class MenuFragment extends Fragment implements MenuContact.View {
    private MenuPresenter presenter;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_tab_menu_main, null);

        UserDataRepository repository = new UserDataRepository(getActivity());
        presenter = new MenuPresenter(this, repository);

        view.findViewById(R.id.history_tab).setOnClickListener(view15 -> presenter.navigateToTestingHistory());

        view.findViewById(R.id.profile_tab).setOnClickListener(view1 -> presenter.navigateToProfile());

        view.findViewById(R.id.home_tab).setOnClickListener(view12 -> startActivity(new Intent(getActivity(), CategoryActivity.class)));

        view.findViewById(R.id.friends_tab).setOnClickListener(view13 -> presenter.navigateToFriends());

        view.findViewById(R.id.courses_tab).setOnClickListener(view14 -> presenter.navigateToCourse());

        return view;
    }

    @Override
    public void showAuthDialog(String message) {
        AuthConfirmDialogWithMessage authConfirmDialog = new AuthConfirmDialogWithMessage(getActivity(), presenter);
        authConfirmDialog.showDialog(message);
    }

    @Override
    public void navigateTo(ActivityEnum activity) {
        switch (activity) {
            case LOGIN:
                startActivity(new Intent(getActivity(), LoginActivity.class));
                break;
            case TESTING_HISTORY:
                startActivity(new Intent(getActivity(), TestingHistoryActivity.class));
                break;
            case FRIENDS:
                startActivity(new Intent(getActivity(), FriendsActivity.class));
                break;
            case COURSE:
                startActivity(new Intent(getActivity(), CourseActivity.class));
                break;
            case PROFILE:
                startActivity(new Intent(getActivity(), ProfileActivity.class));
                break;
        }
    }
}
