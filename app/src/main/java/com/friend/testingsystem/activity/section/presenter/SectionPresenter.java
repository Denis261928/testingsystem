package com.friend.testingsystem.activity.section.presenter;

import android.util.Log;

import com.friend.testingsystem.activity.section.SectionActivity;
import com.friend.testingsystem.activity.section.contact.SectionContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.Course;
import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.model.Section;
import com.friend.testingsystem.repository.UserDataRepository;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SectionPresenter implements SectionContact.Presenter {
    private int pageNumber;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private SectionContact.View view;
    private UserDataRepository repository;

    private Course course;

    public SectionPresenter(SectionContact.View view, UserDataRepository repository, Course course) {
        this.view = view;
        this.repository = repository;
        this.course = course;
        pageNumber = 1;
        view.setCreateSectionButtonVisible(repository.getUserDetail().isTeacher());
    }

    @Override
    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public void setLoading(boolean loading) {
        this.isLoading = loading;
    }

    @Override
    public boolean isLastPage() {
        return isLastPage;
    }

    @Override
    public void loadInitial() {
        pageNumber = 1;
        view.showProgress();
        NetworkService.getInstance().getSectionApi().getAllSectionListByCourseIdAndPage(course.getId(), pageNumber)
                .enqueue(new Callback<ResponseListData<Section>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<Section>> call, Response<ResponseListData<Section>> response) {
                        if (response.isSuccessful()) {
                            view.hideProgress();
                            List<Section> sectionList = response.body().getResults();
                            view.showSections(sectionList);
                            pageNumber++;
                        } else {
                            view.hideProgress();
                            Log.d(SectionActivity.TAG, response.errorBody().toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<Section>> call, Throwable t) {
                        Log.d(SectionActivity.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }

    @Override
    public void loadMore() {
        NetworkService.getInstance().getSectionApi().getAllSectionListByCourseIdAndPage(course.getId(), pageNumber)
                .enqueue(new Callback<ResponseListData<Section>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<Section>> call, Response<ResponseListData<Section>> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getNext() == null) {
                                view.hideLoadingItem();
                                isLastPage = true;
                            } else {
                                view.addAndShowSections(response.body().getResults());
                            }
                        }
                        else {
                            Log.d(SectionActivity.TAG, response.errorBody().toString());
                        }
                        pageNumber++;
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<Section>> call, Throwable t) {
                        Log.d(SectionActivity.TAG, t.getMessage());
                    }
                });
    }

    @Override
    public void clearList() {
        view.clearSections();
    }

    @Override
    public void createSectionClick() {
        view.showCreateSectionDialog(course);
    }
}
