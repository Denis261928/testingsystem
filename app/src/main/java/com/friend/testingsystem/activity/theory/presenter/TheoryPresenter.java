package com.friend.testingsystem.activity.theory.presenter;

import android.util.Log;

import com.friend.testingsystem.activity.theory.TheoryActivity;
import com.friend.testingsystem.activity.theory.contact.TheoryContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.Theory;
import com.friend.testingsystem.model.Topic;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TheoryPresenter implements TheoryContact.Presenter {
    private TheoryContact.View view;

    public TheoryPresenter(TheoryContact.View view) {
        this.view = view;
    }

    @Override
    public void init(Topic topic) {
        view.showProgress();
        NetworkService.getInstance().getTestApi().getTheoryByTopicId(topic.getId())
                .enqueue(new Callback<List<Theory>>() {
                    @Override
                    public void onResponse(Call<List<Theory>> call, Response<List<Theory>> response) {
                        if (response.isSuccessful()) {
                            List<Theory> theoryList = response.body();
                            view.showTheory(theoryList);
                        } else {
                            Log.d(TheoryActivity.TAG, response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @Override
                    public void onFailure(Call<List<Theory>> call, Throwable t) {
                        Log.d(TheoryActivity.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }
}
