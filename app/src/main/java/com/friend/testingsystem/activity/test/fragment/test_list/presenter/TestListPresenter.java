package com.friend.testingsystem.activity.test.fragment.test_list.presenter;


import com.friend.testingsystem.activity.test.fragment.test_list.contact.TestListContact;
import com.friend.testingsystem.model.Test;
import com.friend.testingsystem.model.Topic;
import com.friend.testingsystem.repository.TestDataRepository;
import com.friend.testingsystem.repository.UserDataRepository;

import java.util.List;

public class TestListPresenter implements TestListContact.Presenter {
    private List<Test> testList;

    private TestListContact.View view;
    private UserDataRepository repository;

    public TestListPresenter(TestListContact.View view, UserDataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void init(Topic topic, List<Test> testList) {
        this.testList = testList;

        view.showTopic(topic);
    }

    @Override
    public void updateTestNumbers() {
        view.showTasksNumbers(testList);
    }

    @Override
    public void setTestFinished() {
        view.onTestFinished();
    }

    @Override
    public void goOut() {
        if (TestDataRepository.isTestFinished()) {
            TestDataRepository.clearUserTestList();
            if (repository.isUserAuthorized())
                view.goToTestingHistory();
            else
                view.goToCategory();
        }
        else {
            view.showCancelTestConfirmDialog();
        }
    }
}
