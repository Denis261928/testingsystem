package com.friend.testingsystem.activity.course;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.ImageView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.CommonActivity;
import com.friend.testingsystem.activity.MainActivity;
import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.activity.course.adapter.CourseAdapter;
import com.friend.testingsystem.activity.course.dialog.CreateCourseDialog;
import com.friend.testingsystem.activity.course.presenter.CoursePresenter;
import com.friend.testingsystem.activity.course.сontact.CourseContact;
import com.friend.testingsystem.activity.section.SectionActivity;
import com.friend.testingsystem.dialog.LogoutConfirmDialog;
import com.friend.testingsystem.helper.NetworkUtils;
import com.friend.testingsystem.model.Course;
import com.friend.testingsystem.repository.UserDataRepository;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;
import java.util.List;

public class CourseActivity extends AppCompatActivity implements CourseContact.View, CommonActivity {
    public static final String TAG = "CourseActivityLog";

    private Handler handler;
    private ProgressDialog progressDialog;
    private ImageView loginLogoutButton;
    private AutoCompleteTextView courseNameEditText;
    private ListView courseListView;
    private FloatingActionButton createCourseButton;
    private View loadingView;

    private CourseAdapter adapter;
    private UserDataRepository repository;
    private CoursePresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_course);

        ImageView goBack = findViewById(R.id.go_back);
        loginLogoutButton = findViewById(R.id.login_logout_icon);
        courseListView = findViewById(R.id.course_list);
        createCourseButton = findViewById(R.id.create_course_button);

        if (!NetworkUtils.getNetworkConnection(this)) {
            startActivity(new Intent(this, MainActivity.class));
        }

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.item_loading, null);

        View headerView = inflater.inflate(R.layout.item_course_header, null);
        courseNameEditText = headerView.findViewById(R.id.course_name_edit_text);
        courseListView.addHeaderView(headerView);
        // courseNameEditText.setThreshold(2);

        handler = new MyHandler();

        courseListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(CourseActivity.this, SectionActivity.class);
                intent.putExtra("course", adapter.getCourseList().get(i - 1));
                startActivity(intent);
            }
        });

        repository = new UserDataRepository(this);
        setLoginLogoutIcon();
        presenter = new CoursePresenter(this, repository);
        presenter.loadInitial();

        courseNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) { }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) { }

            @Override
            public void afterTextChanged(Editable s) {
                if (courseNameEditText.getText().toString().equals("")) {
                    presenter.loadInitial();
                } else {
                    presenter.loadBySearchText(courseNameEditText.getText().toString());
                }
            }
        });

        loginLogoutButton.setOnClickListener(view -> loginLogoutClick());

        goBack.setOnClickListener(view -> onBackPressed());

        createCourseButton.setOnClickListener(view -> presenter.createCourseClick());

        courseListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) { }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                // Check when scroll to last item in listView, in this tut, init data in list view
                if (absListView.getLastVisiblePosition() == i2-1 && courseListView.getCount() >= 10
                        && !presenter.isLoading() && !presenter.isLastPage()) {
                    presenter.setLoading(true);
                    Thread thread = new ThreadGetMoreData();
                    thread.start();
                }
            }
        });
    }

    @SuppressLint("HandlerLeak")
    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case 0:
                    courseListView.addFooterView(loadingView);
                    break;
                case 1:
                    adapter.addListItemsToAdapter((List<Course>) msg.obj);
                    courseListView.removeFooterView(loadingView);
                    presenter.setLoading(false);
                    break;
                case 2:
                    courseListView.removeFooterView(loadingView);
                    presenter.setLoading(false);
                    break;
                case 3:
                    adapter.clearCourseList();
                    presenter.loadInitial();
                    break;
                default:
                    break;
            }
        }
    }

    public class ThreadGetMoreData extends Thread {
        @Override
        public void run() {
            // Add loading view during search processing
            handler.sendEmptyMessage(0);
            // Search more data
            presenter.loadMore();
        }
    }

    @Override
    public void setCreateCourseButtonVisible(boolean visible) {
        if (visible)
            createCourseButton.setVisibility(View.VISIBLE);
        else
            createCourseButton.setVisibility(View.GONE);
    }

    @Override
    public void showCourses(List<Course> courseList, boolean isSearch) {
        if (courseList.size() == 0) {
            List<Course> emptyCourseList = new ArrayList<>();
            emptyCourseList.add(new Course());
            adapter = new CourseAdapter(this, emptyCourseList, true);
        } else {
            adapter = new CourseAdapter(this, courseList, false);
        }
        courseListView.setAdapter(adapter);
        if (isSearch)
            courseNameEditText.requestFocus();
    }

    @Override
    public void addAndShowCourses(List<Course> courseList) {
        // Update data adapter and UI
        Message message = handler.obtainMessage(1, courseList);
        handler.sendMessage(message);
    }

    @Override
    public void clearCourses() {
        handler.sendEmptyMessage(3);
    }

    @Override
    public void hideLoadingItem() {
        // Remove loading view when there is not any other data
        handler.sendEmptyMessage(2);
    }

    @Override
    public void showCreateCourseDialog() {
        CreateCourseDialog dialog = new CreateCourseDialog(this, presenter);
        dialog.showDialog();
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Завантаження курсів");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void loginLogoutClick() {
        if (repository.isUserAuthorized()) {
            LogoutConfirmDialog logoutConfirmDialog = new LogoutConfirmDialog(this);
            logoutConfirmDialog.showDialog("Ви впевнені що хочете вийти з акаунту?");
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    @Override
    public void setLoginLogoutIcon() {
        if (repository.isUserAuthorized())
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_logout));
        else
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_login));
    }

    @Override
    public void logout() {
        repository.logoutUser();
        setLoginLogoutIcon();
        startActivity(new Intent(this, CategoryActivity.class));
    }
}
