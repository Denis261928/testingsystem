package com.friend.testingsystem.activity.friends.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.model.Friend;

import java.util.List;

public class FriendsAdapter extends BaseAdapter {
    private Context context;
    private List<Friend> friendList;
    private OnFriendListener listener;

    public FriendsAdapter(Context context, List<Friend> friendList, OnFriendListener listener) {
        this.context = context;
        this.friendList = friendList;
        this.listener = listener;
    }

    public void addListItemsToAdapter(List<Friend> list) {
        this.friendList.addAll(list);
        this.notifyDataSetChanged();
    }

    public void clearFriendList() {
        this.friendList.clear();
        this.notifyDataSetChanged();
    }

    public List<Friend> getFriendList() {
        return friendList;
    }

    @Override
    public int getCount() {
        return friendList.size();
    }

    @Override
    public Object getItem(int i) {
        return friendList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(context, R.layout.item_friend, null);

        TextView friendNumberText = v.findViewById(R.id.friend_number_text);
        TextView friendNameText = v.findViewById(R.id.friend_name_text);
        ImageView removeFriendButton = v.findViewById(R.id.remove_friend_button);

        final int position = i;
        friendNumberText.setText(String.valueOf(position + 1));
        friendNameText.setText(friendList.get(position).getUserName());

        removeFriendButton.setOnClickListener(view1 -> listener.onRemoveFriendClick(friendList.get(position)));

        return v;
    }

    public interface OnFriendListener {
        void onRemoveFriendClick(Friend friend);
    }
}
