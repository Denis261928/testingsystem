package com.friend.testingsystem.activity.video_lesson.presenter;

import android.util.Log;

import com.friend.testingsystem.activity.video_lesson.VideoLessonActivity;
import com.friend.testingsystem.activity.video_lesson.contact.VideoLessonContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.helper.ActivityEnum;
import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.model.SubCategory;
import com.friend.testingsystem.model.VideoLesson;
import com.friend.testingsystem.repository.UserDataRepository;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class VideoLessonPresenter implements VideoLessonContact.Presenter {
    private List<String> videoList;
    private SubCategory subCategory;
    private int pageNumber;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private VideoLessonContact.View view;
    private UserDataRepository repository;

    public VideoLessonPresenter(VideoLessonContact.View view, UserDataRepository repository, SubCategory subCategory) {
        this.view = view;
        this.repository = repository;
        this.subCategory = subCategory;
        pageNumber = 1;
    }

    @Override
    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public void setLoading(boolean loading) {
        this.isLoading = loading;
    }

    @Override
    public boolean isLastPage() {
        return isLastPage;
    }

    @Override
    public void loadInitial() {
        pageNumber = 1;
        view.showProgress();
        NetworkService.getInstance().getVideoLessonApi().getVideoLessonListBySubCategoryIdAndByPage(subCategory.getId(), pageNumber)
                .enqueue(new Callback<ResponseListData<VideoLesson>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<VideoLesson>> call, Response<ResponseListData<VideoLesson>> response) {
                        if (response.isSuccessful()) {
                            view.hideProgress();
                            List<VideoLesson> videoLessonList = response.body().getResults();
                            view.showVideoLessons(videoLessonList);
                            Log.d(VideoLessonActivity.TAG, response.body().toString());
                            pageNumber++;
                        } else {
                            view.hideProgress();
                            Log.d(VideoLessonActivity.TAG, response.errorBody().toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<VideoLesson>> call, Throwable t) {
                        Log.d(VideoLessonActivity.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }

    @Override
    public void loadMore() {
        NetworkService.getInstance().getVideoLessonApi().getVideoLessonListBySubCategoryIdAndByPage(subCategory.getId(), pageNumber)
                .enqueue(new Callback<ResponseListData<VideoLesson>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<VideoLesson>> call, Response<ResponseListData<VideoLesson>> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getNext() == null) {
                                view.hideLoadingItem();
                                isLastPage = true;
                            } else {
                                view.addAndShowVideoLessons(response.body().getResults());
                            }
                        }
                        else {
                            Log.d(VideoLessonActivity.TAG, response.errorBody().toString());
                        }
                        pageNumber++;
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<VideoLesson>> call, Throwable t) {
                        Log.d(VideoLessonActivity.TAG, t.getMessage());
                    }
                });
    }

    @Override
    public void navigateToTestingHistory() {
        if (repository.isUserAuthorized())
            view.navigateTo(ActivityEnum.TESTING_HISTORY);
        else
            view.showAuthDialog("Щоб вести історію тестувань вам потрібно авторизуватись.\nПерейти на сторінку авторизації?");
    }

    @Override
    public void navigateToFriends() {
        if (repository.isUserAuthorized())
            view.navigateTo(ActivityEnum.FRIENDS);
        else
            view.showAuthDialog("Щоб перейти до списку друзів вам потрібно авторизуватись.\nПерейти на сторінку авторизації?");
    }

    @Override
    public void navigateToCourse() {
        if (repository.isUserAuthorized())
            view.navigateTo(ActivityEnum.COURSE);
        else
            view.showAuthDialog("Щоб перейти до курсів вам потрібно авторизуватись.\nПерейти на сторінку авторизації?");
    }

    @Override
    public void navigateToLogin() {
        view.navigateTo(ActivityEnum.LOGIN);
    }

    @Override
    public void navigateToProfile() {
        if (repository.isUserAuthorized())
            view.navigateTo(ActivityEnum.PROFILE);
        else
            view.showAuthDialog("Щоб перейти до профілю вам потрібно авторизуватись.\nПерейти на сторінку авторизації?");
    }
}
