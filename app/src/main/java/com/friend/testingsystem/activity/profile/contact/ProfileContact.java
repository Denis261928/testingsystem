package com.friend.testingsystem.activity.profile.contact;

import com.friend.testingsystem.model.Password;
import com.friend.testingsystem.model.UserDetail;

public interface ProfileContact {
    interface View {
        void showUser(String email, UserDetail userDetail);
        void becomeTeacherRequestSuccessful();
        void setBecomeTeacherButtonStatus();
        String getFirstName();
        String getLastName();
        String getPhoneNumber();
        void confirmFirstNameSavingDialog();
        void confirmLastNameSavingDialog();
        void confirmPhoneNumberNameSavingDialog();
        void phoneNumberIsIncompleteDialog();
        void confirmPasswordSavingDialog(Password password);
        void hideFirstNameKeyBoard();
        void hideLastNameKeyBoard();
        void hidePhoneNumberKeyBoard();
        void showProgress(String message);
        void hideProgress();
    }

    interface Presenter {
        void init();
        void getUserDetailApi();
        void becomeTeacher();
        UserDetail getUserDetail();
        void updatePassword(String oldPassword, String newPassword);
        void inputFirstNameChanged();
        void inputLastNameChanged();
        void inputPhoneNumberChanged();
    }
}
