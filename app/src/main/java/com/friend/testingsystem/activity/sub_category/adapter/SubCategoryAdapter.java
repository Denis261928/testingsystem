package com.friend.testingsystem.activity.sub_category.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.model.SubCategory;

public class SubCategoryAdapter extends PagedListAdapter<SubCategory, SubCategoryAdapter.SubCategoryViewHolder> {
    private Context context;
    private OnSubCategoryListener onSubCategoryListener;

    public SubCategoryAdapter(Context context, OnSubCategoryListener onSubCategoryListener) {
        super(DIFF_CALLBACK);
        this.context = context;
        this.onSubCategoryListener = onSubCategoryListener;
    }

    @NonNull
    @Override
    public SubCategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_sub_category, parent, false);

        return new SubCategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull SubCategoryViewHolder holder, int position) {
        SubCategory subCategory = getItem(position);
        if (subCategory != null) {
            holder.setItem(subCategory, onSubCategoryListener);
        } else {
            Log.d(CategoryActivity.TAG, "Category "+ position +" is null");
        }
    }

    private static DiffUtil.ItemCallback<SubCategory> DIFF_CALLBACK = new DiffUtil.ItemCallback<SubCategory>() {
        @Override
        public boolean areItemsTheSame(@NonNull SubCategory oldItem, @NonNull SubCategory newItem) {
             return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull SubCategory oldItem, @NonNull SubCategory newItem) {
            return oldItem.equals(newItem);
        }
    };

    static class SubCategoryViewHolder extends RecyclerView.ViewHolder {
        private FrameLayout goVideo;
        private TextView subCategoryName;

        public SubCategoryViewHolder(@NonNull View itemView) {
            super(itemView);

            goVideo = itemView.findViewById(R.id.go_to_video);
            subCategoryName = itemView.findViewById(R.id.sub_category_name);
        }

        public void setItem(final SubCategory subCategory, final OnSubCategoryListener listener) {
            subCategoryName.setText(subCategory.getName());

            itemView.setOnClickListener(view -> listener.onSubCategoryClick(subCategory));

            goVideo.setOnClickListener(view -> listener.onVideoClick(subCategory));
        }

    }

    public interface OnSubCategoryListener {
        void onSubCategoryClick(SubCategory subCategory);
        void onVideoClick(SubCategory subCategory);

    }
}
