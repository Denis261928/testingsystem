package com.friend.testingsystem.activity.auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.MainActivity;
import com.friend.testingsystem.activity.auth.contact.AuthContact;
import com.friend.testingsystem.activity.auth.presenter.AuthPresenter;
import com.friend.testingsystem.dialog.InfoDialog;
import com.friend.testingsystem.helper.NetworkUtils;
import com.friend.testingsystem.repository.UserDataRepository;

public class LoginActivity extends AppCompatActivity implements AuthContact.View {
    public static final String TAG = "LoginActivityLog";

    private ProgressDialog progressDialog;
    private EditText userNameText;
    private EditText passwordText;
    private Button loginButton;

    private AuthPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        userNameText = findViewById(R.id.user_name_login_text);
        passwordText = findViewById(R.id.password_login_text);
        loginButton = findViewById(R.id.login_button);
        // TextView forgotPasswordButton = findViewById(R.id.forgot_password_button);
        TextView goRegistrationButton = findViewById(R.id.go_to_registration);

        if (!NetworkUtils.getNetworkConnection(LoginActivity.this)) {
            startActivity(new Intent(LoginActivity.this, MainActivity.class));
        }

        UserDataRepository repository = new UserDataRepository(this);
        presenter = new AuthPresenter(this, repository);

        loginButton.setOnClickListener(view -> presenter.login(userNameText.getText().toString(), passwordText.getText().toString()));

        userNameText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                inputDataChanged();
            }
        });

        passwordText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                inputDataChanged();
            }
        });

        goRegistrationButton.setOnClickListener(view -> startActivity(new Intent(LoginActivity.this, RegistrationActivity.class)));
    }

    private void inputDataChanged() {
        if (userNameText.getText().toString().equals("") || passwordText.getText().toString().equals("")) {
            loginButton.setEnabled(false);
            loginButton.setBackgroundResource(R.drawable.background_button_disable);
            return;
        }
        loginButton.setEnabled(true);
        loginButton.setBackgroundResource(R.drawable.background_button);
    }

    @Override
    public void showErrorDialog(String title, String message) {
        InfoDialog infoDialog = new InfoDialog(this);
        infoDialog.showDialog(title, message);
    }

    @Override
    public void success() {
        startActivity(new Intent(LoginActivity.this, MainActivity.class));
    }

    @Override
    public void showProgress(String message) {
        progressDialog = new ProgressDialog(LoginActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }
}
