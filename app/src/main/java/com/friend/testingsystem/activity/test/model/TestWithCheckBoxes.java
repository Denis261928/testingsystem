package com.friend.testingsystem.activity.test.model;

import java.io.Serializable;
import java.util.List;

public class TestWithCheckBoxes implements Serializable {
    private List<TaskWithCheckBoxes> taskWithCheckBoxesList;
    private boolean isActive = true;
    private int scoreForTheTest = 0;

    public TestWithCheckBoxes(List<TaskWithCheckBoxes> taskWithCheckBoxesList) {
        this.taskWithCheckBoxesList = taskWithCheckBoxesList;
    }

    public List<TaskWithCheckBoxes> getTaskWithCheckBoxesList() {
        return taskWithCheckBoxesList;
    }

    public void setTaskWithCheckBoxesList(List<TaskWithCheckBoxes> taskWithCheckBoxesList) {
        this.taskWithCheckBoxesList = taskWithCheckBoxesList;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getScoreForTheTest() {
        return scoreForTheTest;
    }

    public void setScoreForTheTest(int scoreForTheTest) {
        this.scoreForTheTest = scoreForTheTest;
    }

}
