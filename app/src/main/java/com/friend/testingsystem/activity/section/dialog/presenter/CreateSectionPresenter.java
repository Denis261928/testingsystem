package com.friend.testingsystem.activity.section.dialog.presenter;

import android.util.Log;

import com.friend.testingsystem.activity.section.dialog.CreateSectionDialog;
import com.friend.testingsystem.activity.section.dialog.contact.CreateSectionContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.Course;
import com.friend.testingsystem.model.Section;
import com.friend.testingsystem.repository.UserDataRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateSectionPresenter implements CreateSectionContact.Presenter {
    private CreateSectionContact.View view;
    private UserDataRepository repository;

    private Course course;

    public CreateSectionPresenter(CreateSectionContact.View view, UserDataRepository repository, Course course) {
        this.view = view;
        this.repository = repository;
        this.course = course;
    }

    @Override
    public void createSection(String sectionName) {
        Section section = new Section();
        section.setName(sectionName);
        section.setCourseId(course.getId());
        NetworkService.getInstance().getSectionApi().createSection("Token "+repository.getAuthToken(), section)
                .enqueue(new Callback<Section>() {
                    @Override
                    public void onResponse(Call<Section> call, Response<Section> response) {
                        if (response.isSuccessful()) {
                            view.sectionCreated();
                        }
                        else {
                            Log.d(CreateSectionDialog.TAG, response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @Override
                    public void onFailure(Call<Section> call, Throwable t) {
                        Log.d(CreateSectionDialog.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }
}
