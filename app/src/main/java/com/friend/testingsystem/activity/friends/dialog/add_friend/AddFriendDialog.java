package com.friend.testingsystem.activity.friends.dialog.add_friend;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.friends.dialog.add_friend.contact.AddFriendContact;
import com.friend.testingsystem.activity.friends.dialog.add_friend.presenter.AddFriendPresenter;
import com.friend.testingsystem.activity.friends.presenter.FriendsPresenter;
import com.friend.testingsystem.dialog.interfaces.MyDialog;
import com.friend.testingsystem.repository.UserDataRepository;

public class AddFriendDialog implements MyDialog, AddFriendContact.View {
    public static final String TAG = "AddFriendDialogLog";

    private Dialog dialog;
    private ProgressDialog progressDialog;
    private EditText friendUserNameEditText;
    private Button addFriendButton;
    private TextView noUserWrongText;

    private Activity parentActivity;
    private AddFriendPresenter addFriendPresenter;
    private FriendsPresenter friendsPresenter;

    public AddFriendDialog(Activity parentActivity, FriendsPresenter friendsPresenter) {
        this.parentActivity = parentActivity;
        this.friendsPresenter = friendsPresenter;
        UserDataRepository repository = new UserDataRepository(parentActivity);
        addFriendPresenter = new AddFriendPresenter(this, repository);
    }

    @Override
    public void showDialog() {
        dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_add_friend);

        friendUserNameEditText = dialog.findViewById(R.id.friend_user_name_edit_text);
        addFriendButton = dialog.findViewById(R.id.add_friend_button);
        Button cancelButton = dialog.findViewById(R.id.cancel_button);
        noUserWrongText = dialog.findViewById(R.id.no_user_wrong_text);

        friendUserNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                if (friendUserNameEditText.getText().toString().equals("")) {
                    addFriendButton.setEnabled(false);
                    addFriendButton.setBackgroundResource(R.drawable.background_dialog_button_disable);
                }
                else {
                    addFriendButton.setEnabled(true);
                    addFriendButton.setBackgroundResource(R.drawable.background_dialog_positive_button);
                }

                if (noUserWrongText.getVisibility() == View.VISIBLE)
                    noUserWrongText.setVisibility(View.GONE);
            }
        });

        addFriendButton.setOnClickListener(v -> addFriendPresenter.addFriend(friendUserNameEditText.getText().toString()));

        cancelButton.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }

    @Override
    public void friendAdded() {
        dialog.dismiss();
        friendsPresenter.clearList();
    }

    @Override
    public void userNotFound() {
        noUserWrongText.setVisibility(View.VISIBLE);
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(parentActivity);
        progressDialog.setMessage("Добавлення друга");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }
}
