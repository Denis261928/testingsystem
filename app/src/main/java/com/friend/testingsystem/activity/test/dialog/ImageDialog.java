package com.friend.testingsystem.activity.test.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.Window;
import android.widget.ImageView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.dialog.interfaces.MyDialog;
import com.squareup.picasso.Picasso;

public class ImageDialog implements MyDialog {
    private Context context;
    private String image;

    public ImageDialog(Context context, String image) {
        this.context = context;
        this.image = image;
    }

    @Override
    public void showDialog(){
        final Dialog dialog = new Dialog(context);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(true);
        dialog.setContentView(R.layout.dialog_image_layout);

        ImageView imageView = dialog.findViewById(R.id.image_dialog);
        Picasso.get().load(image).into(imageView);

        imageView.setOnClickListener(v -> dialog.dismiss());

        dialog.show();
    }
}
