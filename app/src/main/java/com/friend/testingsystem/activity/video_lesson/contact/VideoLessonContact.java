package com.friend.testingsystem.activity.video_lesson.contact;

import com.friend.testingsystem.helper.ActivityEnum;
import com.friend.testingsystem.model.VideoLesson;

import java.util.List;

public interface VideoLessonContact {
    interface View {
        void showVideoLessons(List<VideoLesson> videoLessonList);
        void addAndShowVideoLessons(List<VideoLesson> videoLessonList);
        void hideLoadingItem();
        void showAuthDialog(String message);
        void navigateTo(ActivityEnum activity);
        void showProgress();
        void hideProgress();
    }

    interface Presenter {
        boolean isLoading();
        void setLoading(boolean loading);
        boolean isLastPage();
        void loadInitial();
        void loadMore();
        void navigateToTestingHistory();
        void navigateToFriends();
        void navigateToCourse();
        void navigateToLogin();
        void navigateToProfile();
    }
}
