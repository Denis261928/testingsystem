package com.friend.testingsystem.activity.topic.data_source;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.friend.testingsystem.activity.topic.TopicActivity;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.model.Topic;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TopicDataSource extends PageKeyedDataSource<Integer, Topic> {
    private long subCategoryId;

    public TopicDataSource(long subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, Topic> callback) {
        NetworkService.getInstance().getTopicApi().getTopicListBySubCategoryIdAndByPage(subCategoryId, 1)
                .enqueue(new Callback<ResponseListData<Topic>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<Topic>> call, Response<ResponseListData<Topic>> response) {
                        if (response.body() != null) {
                            callback.onResult(response.body().getResults(), null, 2);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<Topic>> call, Throwable t) {
                        Log.d(TopicActivity.TAG, t.getMessage());
                    }
                });
    }

    @Override
    public void loadBefore(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Topic> callback) {
        NetworkService.getInstance().getTopicApi().getTopicListBySubCategoryIdAndByPage(subCategoryId, params.key)
                .enqueue(new Callback<ResponseListData<Topic>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<Topic>> call, Response<ResponseListData<Topic>> response) {
                        // Integer key = (params.key > 1) ? params.key - 1 : null;
                        if (response.body() != null) {
                            // Integer key = response.body().getPrevious() != null ? params.key - 1 : null;
                            callback.onResult(response.body().getResults(), params.key-1);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<Topic>> call, Throwable t) {
                        Log.d(TopicActivity.TAG, t.getMessage());
                    }
                });
    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, Topic> callback) {
        NetworkService.getInstance().getTopicApi().getTopicListBySubCategoryIdAndByPage(subCategoryId, params.key)
                .enqueue(new Callback<ResponseListData<Topic>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<Topic>> call, Response<ResponseListData<Topic>> response) {
                        if (response.body() != null) {
                            // Integer key = response.body().getNext() != null ? params.key + 1 : null;
                            callback.onResult(response.body().getResults(), params.key+1);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<Topic>> call, Throwable t) {
                        Log.d(TopicActivity.TAG, t.getMessage());
                    }
                });
    }
}
