package com.friend.testingsystem.activity.category.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.model.Category;
import com.squareup.picasso.Picasso;

public class CategoryAdapter extends PagedListAdapter<Category, CategoryAdapter.CategoryViewHolder> {
    private Context context;
    private OnCategoryListener onCategoryListener;

    public CategoryAdapter(Context context, OnCategoryListener onCategoryListener) {
        super(DIFF_CALLBACK);
        this.context = context;
        this.onCategoryListener = onCategoryListener;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_category, parent, false);

        return new CategoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Category category = getItem(position);
        if (category != null) {
            holder.setItem(category, onCategoryListener);
        } else {
            Log.d(CategoryActivity.TAG, "Category "+ position +" is null");
        }
    }

    private static DiffUtil.ItemCallback<Category> DIFF_CALLBACK = new DiffUtil.ItemCallback<Category>() {
        @Override
        public boolean areItemsTheSame(@NonNull Category oldItem, @NonNull Category newItem) {
             return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Category oldItem, @NonNull Category newItem) {
            return oldItem.equals(newItem);
        }
    };

    static class CategoryViewHolder extends RecyclerView.ViewHolder {
        ImageView categoryImage;
        TextView categoryName;
        TextView subCategoriesNumber;

        public CategoryViewHolder(@NonNull View itemView) {
            super(itemView);
            categoryImage = itemView.findViewById(R.id.img_category);
            categoryName = itemView.findViewById(R.id.category_name);
            subCategoriesNumber = itemView.findViewById(R.id.sections_number);
        }

        public void setItem(final Category category, final OnCategoryListener listener) {
            Picasso.get().load(category.getLogo()).into(categoryImage);
            categoryName.setText(category.getName());
            subCategoriesNumber.setText(category.getSubCategoryCount()+" розділів");

            itemView.setOnClickListener(view -> listener.onCategoryClick(category));
        }
    }

    public interface OnCategoryListener {
        void onCategoryClick(Category category);
    }
}
