package com.friend.testingsystem.activity.course.dialog.contact;

public interface CreateCourseContact {
    interface View {
        void courseCreated();
        void showProgress();
        void hideProgress();
    }
    interface Presenter {
        void createCourse(String courseName);
    }
}
