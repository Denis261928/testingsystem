package com.friend.testingsystem.activity.section_element.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.friend.testingsystem.activity.section_element.SectionElementActivity;
import com.friend.testingsystem.activity.section_element.contact.SectionElementContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.model.Section;
import com.friend.testingsystem.model.SectionElement;
import com.friend.testingsystem.repository.UserDataRepository;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SectionElementPresenter implements SectionElementContact.Presenter {
    private int pageNumber;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private SectionElementContact.View view;

    private Section section;

    public SectionElementPresenter(SectionElementContact.View view, UserDataRepository repository, Section section) {
        this.view = view;
        this.section = section;
        pageNumber = 1;
        view.setCreateSectionElementButtonVisible(repository.getUserDetail().isTeacher());
    }

    @Override
    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public void setLoading(boolean loading) {
        this.isLoading = loading;
    }

    @Override
    public boolean isLastPage() {
        return isLastPage;
    }

    @SuppressLint("LongLogTag")
    @Override
    public void loadInitial() {
        Log.d(SectionElementActivity.TAG, "load");
        pageNumber = 1;
        view.showProgress();
        NetworkService.getInstance().getSectionElementApi().getAllSectionElementListBySectionIdAndPage(section.getId(), pageNumber)
                .enqueue(new Callback<ResponseListData<SectionElement>>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(Call<ResponseListData<SectionElement>> call, Response<ResponseListData<SectionElement>> response) {
                        if (response.isSuccessful()) {
                            view.hideProgress();
                            List<SectionElement> sectionElementList = response.body().getResults();
                            view.showSectionElements(sectionElementList);
                            pageNumber++;
                        } else {
                            view.hideProgress();
                            Log.d(SectionElementActivity.TAG, response.errorBody().toString());
                        }
                    }

                    @SuppressLint("LongLogTag")
                    @Override
                    public void onFailure(Call<ResponseListData<SectionElement>> call, Throwable t) {
                        Log.d(SectionElementActivity.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }

    @Override
    public void loadMore() {
        NetworkService.getInstance().getSectionElementApi().getAllSectionElementListBySectionIdAndPage(section.getId(), pageNumber)
                .enqueue(new Callback<ResponseListData<SectionElement>>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(Call<ResponseListData<SectionElement>> call, Response<ResponseListData<SectionElement>> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getNext() == null) {
                                isLastPage = true;
                                view.addAndShowSectionElements(response.body().getResults());
                                view.hideLoadingItem();
                            } else {
                                view.addAndShowSectionElements(response.body().getResults());
                            }
                        }
                        else {
                            Log.d(SectionElementActivity.TAG, response.errorBody().toString());
                        }
                        pageNumber++;
                    }

                    @SuppressLint("LongLogTag")
                    @Override
                    public void onFailure(Call<ResponseListData<SectionElement>> call, Throwable t) {
                        Log.d(SectionElementActivity.TAG, t.getMessage());
                        // view.hideProgress();
                    }
                });
    }

    @Override
    public void clearList() {
        view.clearSectionElements();
    }

    @Override
    public void downloadFile(SectionElement sectionElement) {
        view.downloadFile(sectionElement);
    }

    @Override
    public void createSectionElementClick() {
        view.showCreateSectionElementDialog(section);
    }

}
