package com.friend.testingsystem.activity.course.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.model.Course;

import java.util.List;

public class CourseAdapter extends BaseAdapter {
    private Context context;
    private List<Course> courseList;
    private boolean isEmpty;

    public CourseAdapter(Context context, List<Course> courseList, boolean isEmpty) {
        this.context = context;
        this.courseList = courseList;
        this.isEmpty = isEmpty;
    }

    public void addListItemsToAdapter(List<Course> list) {
        courseList.addAll(list);
        notifyDataSetChanged();
    }

    public void clearCourseList() {
        courseList.clear();
        notifyDataSetChanged();
    }

    public List<Course> getCourseList() {
        return courseList;
    }

    @Override
    public int getCount() {
        return courseList.size();
    }

    @Override
    public Object getItem(int i) {
        return courseList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v;

        if (isEmpty) {
            v = View.inflate(context, R.layout.item_course_empty, null);
        } else {
            v = View.inflate(context, R.layout.item_course, null);
            TextView courseNameText = v.findViewById(R.id.course_name_text);
            courseNameText.setText(courseList.get(i).getName());
        }

        return v;
    }
}
