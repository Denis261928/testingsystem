package com.friend.testingsystem.activity.test.fragment.matching_test.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.test.TestActivity;
import com.friend.testingsystem.activity.test.dialog.ImageDialog;
import com.friend.testingsystem.model.Answer;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MatchingTestAnswerAdapter extends RecyclerView.Adapter<MatchingTestAnswerAdapter.MatchingTestAnswerViewHolder> {
    private Context context;
    private List<Answer> answerList;

    public MatchingTestAnswerAdapter(Context context, List<Answer> answerList) {
        this.context = context;
        this.answerList = answerList;
    }

    @NonNull
    @Override
    public MatchingTestAnswerViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_matching_question_answer, parent, false);

        return new MatchingTestAnswerViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MatchingTestAnswerViewHolder holder, int position) {
        holder.bind(answerList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return answerList.size();
    }

    class MatchingTestAnswerViewHolder extends RecyclerView.ViewHolder {
        private TextView answerNumberText;
        private TextView answerText;
        private RelativeLayout answerImageContainer;
        private ImageView answerImage;

        private Answer answer;

        public MatchingTestAnswerViewHolder(@NonNull View itemView) {
            super(itemView);

            answerNumberText = itemView.findViewById(R.id.question_answer_number_text);
            answerText = itemView.findViewById(R.id.text_question_answer);
            answerImageContainer = itemView.findViewById(R.id.img_question_answer_container);
            answerImage = itemView.findViewById(R.id.img_question_answer);

            answerImage.setOnClickListener(view -> {
                ImageDialog dialog = new ImageDialog(context, answer.getImg());
                dialog.showDialog();
            });
        }

        public void bind(Answer answer, int position) {
            this.answer = answer;

            answerNumberText.setText(TestActivity.alphabetIndexes[position]);
            if (answer.getText() == null || answer.getText().equals("") || answer.getText().equals("null"))
                this.answerText.setVisibility(View.GONE);
            else
                this.answerText.setText(answer.getText());

            if (answer.getImg() != null) {
                answerImageContainer.setVisibility(View.VISIBLE);
                Picasso.get().load(answer.getImg()).into(answerImage);
            }
        }
    }
}
