package com.friend.testingsystem.activity.test.dialog.test_help;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.RelativeLayout;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.test.dialog.test_help.adapter.TestHelpAdapter;
import com.friend.testingsystem.activity.test.dialog.test_help.contact.TestHelpContact;
import com.friend.testingsystem.activity.test.dialog.test_help.presenter.TestHelpPresenter;
import com.friend.testingsystem.dialog.InfoDialog;
import com.friend.testingsystem.dialog.interfaces.MyDialog;
import com.friend.testingsystem.model.TestHelp;

import java.util.List;

public class TestHelpDialog implements MyDialog, TestHelpContact.View {
    public static final String TAG = TestHelpDialog.class.getName();

    private Activity parentActivity;
    private long testId;

    private Dialog dialog;

    private ProgressDialog progressDialog;
    private RecyclerView recyclerView;

    public TestHelpDialog(Activity parentActivity, long testId) {
        this.parentActivity = parentActivity;
        this.testId = testId;
    }

    @Override
    public void showDialog() {
        dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_test_help);

        RelativeLayout hideTestHelpButton = dialog.findViewById(R.id.hide_test_help_button);
        recyclerView = dialog.findViewById(R.id.test_help_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(dialog.getContext()));

        TestHelpPresenter presenter = new TestHelpPresenter(this);
        presenter.init(testId);

        hideTestHelpButton.setOnClickListener(view -> dialog.dismiss());
    }

    @Override
    public void showTestHelp(List<TestHelp> testHelpList) {
        TestHelpAdapter adapter = new TestHelpAdapter(testHelpList);
        recyclerView.setAdapter(adapter);
        dialog.show();
    }

    @Override
    public void showDialogInfo(String message) {
        InfoDialog infoDialog = new InfoDialog(parentActivity);
        infoDialog.showDialog(message);
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(parentActivity);
        progressDialog.setMessage("Завантаження...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

}
