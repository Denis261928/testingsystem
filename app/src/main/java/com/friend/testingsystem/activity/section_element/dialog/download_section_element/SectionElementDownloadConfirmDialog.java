package com.friend.testingsystem.activity.section_element.dialog.download_section_element;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.section_element.contact.SectionElementContact;
import com.friend.testingsystem.dialog.interfaces.MyDialog;
import com.friend.testingsystem.model.SectionElement;

public class SectionElementDownloadConfirmDialog implements MyDialog {
    private Activity parentActivity;
    private SectionElementContact.Presenter presenter;
    private SectionElement sectionElement;

    public SectionElementDownloadConfirmDialog(Activity parentActivity,
                                               SectionElementContact.Presenter presenter,
                                               SectionElement sectionElement) {
        this.parentActivity = parentActivity;
        this.presenter = presenter;
        this.sectionElement = sectionElement;
    }

    @Override
    public void showDialog() {
        final Dialog dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_confirm);

        TextView messageText = dialog.findViewById(R.id.dialog_message);
        Button positiveButton = dialog.findViewById(R.id.dialog_positive_button);
        Button negativeButton = dialog.findViewById(R.id.dialog_negative_button);

        messageText.setText("Ви впевнені що хочете завантажити " + sectionElement.getName() + "?");

        positiveButton.setOnClickListener(v -> {
            dialog.dismiss();
            presenter.downloadFile(sectionElement);
        });
        negativeButton.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }
}
