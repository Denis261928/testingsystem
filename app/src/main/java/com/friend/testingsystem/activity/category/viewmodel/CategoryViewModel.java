package com.friend.testingsystem.activity.category.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.friend.testingsystem.activity.category.data_source.CategoryDataSourceFactory;
import com.friend.testingsystem.model.Category;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class CategoryViewModel extends ViewModel {

    public LiveData<PagedList<Category>> categoryPageList;
    public LiveData<PageKeyedDataSource<Integer, Category>> liveDataSource;

    public CategoryViewModel() {
        initCategories();
    }

    public void initCategories() {
        Executor executor = Executors.newFixedThreadPool(5);

        CategoryDataSourceFactory categoryDataSourceFactory = new CategoryDataSourceFactory();
        liveDataSource = categoryDataSourceFactory.getCategoryLiveDataSource();

        PagedList.Config config = (new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(10)
                .setInitialLoadSizeHint(20))
                .build();

        categoryPageList = (new LivePagedListBuilder(categoryDataSourceFactory, config))
                .setFetchExecutor(executor)
                .build();
    }
}
