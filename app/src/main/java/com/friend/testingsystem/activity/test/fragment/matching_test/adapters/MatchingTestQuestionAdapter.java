package com.friend.testingsystem.activity.test.fragment.matching_test.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.test.dialog.ImageDialog;
import com.friend.testingsystem.model.Task;
import com.squareup.picasso.Picasso;

import java.util.List;

public class MatchingTestQuestionAdapter extends RecyclerView.Adapter<MatchingTestQuestionAdapter.MatchingTestQuestionViewHolder> {
    private Context context;
    private List<Task> taskList;

    public MatchingTestQuestionAdapter(Context context, List<Task> taskList) {
        this.context = context;
        this.taskList = taskList;
    }

    @NonNull
    @Override
    public MatchingTestQuestionViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.item_matching_question_answer, parent, false);

        return new MatchingTestQuestionViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MatchingTestQuestionViewHolder holder, int position) {
        holder.bind(taskList.get(position), position);
    }

    @Override
    public int getItemCount() {
        return taskList.size();
    }

    class MatchingTestQuestionViewHolder extends RecyclerView.ViewHolder {
        private TextView questionNumberText;
        private TextView questionText;
        private TextView correctAnswerCountText;
        private LinearLayout correctAnswerCountTextContainer;
        private RelativeLayout questionImageContainer;
        private ImageView questionImage;

        private Task task;

        public MatchingTestQuestionViewHolder(@NonNull View itemView) {
            super(itemView);

            questionNumberText = itemView.findViewById(R.id.question_answer_number_text);
            questionText = itemView.findViewById(R.id.text_question_answer);
            correctAnswerCountText = itemView.findViewById(R.id.correct_answer_count_text);
            correctAnswerCountTextContainer = itemView.findViewById(R.id.correct_answer_count_text_container);
            questionImageContainer = itemView.findViewById(R.id.img_question_answer_container);
            questionImage = itemView.findViewById(R.id.img_question_answer);

            questionImage.setOnClickListener(view -> {
                ImageDialog dialog = new ImageDialog(context, task.getImg());
                dialog.showDialog();
            });
        }

        public void bind(Task task, int position) {
            this.task = task;

            questionNumberText.setText(String.valueOf(position + 1));
            if (task.getQuestion() == null || task.getQuestion().equals("") || task.getQuestion().equals("null"))
                this.questionText.setVisibility(View.GONE);
            else
                this.questionText.setText(task.getQuestion());

            correctAnswerCountTextContainer.setVisibility(View.VISIBLE);
            if (task.getCorrectAnswers().size() == 1)
                correctAnswerCountText.setText("Виберіть 1 правильну відповідь");
            else if (task.getCorrectAnswers().size() >= 2 && task.getCorrectAnswers().size() <= 4)
                correctAnswerCountText.setText("Виберіть "+task.getCorrectAnswers().size()+" правильні відповіді");
            else
                correctAnswerCountText.setText("Виберіть "+task.getCorrectAnswers().size()+" правильних відповідей");

            if (task.getImg() != null) {
                questionImageContainer.setVisibility(View.VISIBLE);
                Picasso.get().load(task.getImg()).into(questionImage);
            }
        }
    }
}
