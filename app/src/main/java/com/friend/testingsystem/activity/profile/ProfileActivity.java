package com.friend.testingsystem.activity.profile;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.CommonActivity;
import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.profile.contact.ProfileContact;
import com.friend.testingsystem.activity.profile.dialog.update_info.UpdateUserConfirmDialog;
import com.friend.testingsystem.activity.profile.dialog.update_password.UpdatePasswordConfirmDialog;
import com.friend.testingsystem.activity.profile.presenter.ProfilePresenter;
import com.friend.testingsystem.dialog.InfoDialog;
import com.friend.testingsystem.dialog.LogoutConfirmDialog;
import com.friend.testingsystem.helper.UserFieldName;
import com.friend.testingsystem.model.Password;
import com.friend.testingsystem.model.UserDetail;
import com.friend.testingsystem.repository.UserDataRepository;

import br.com.sapereaude.maskedEditText.MaskedEditText;

public class ProfileActivity extends AppCompatActivity implements ProfileContact.View, CommonActivity {
    public static final String TAG = "ProfileActivityLog";

    private ProgressDialog progressDialog;
    private ImageView loginLogoutButton;
    private TextView emailText;
    private TextView userNameText;
    private EditText firstNameEditText;
    private ImageView saveFirstNameButton;
    private EditText lastNameEditText;
    private ImageView saveLastNameButton;
    private MaskedEditText phoneNumberEditText;
    private ImageView savePhoneNumberButton;
    private EditText oldPasswordEditText;
    private EditText newPasswordEditText;
    private ImageView updatePasswordButton;
    private Button becomeTeacherButton;

    private ProfilePresenter presenter;
    private UserDataRepository repository;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        loginLogoutButton = findViewById(R.id.login_logout_icon);
        userNameText = findViewById(R.id.user_name_text);
        emailText = findViewById(R.id.email_text);
        firstNameEditText = findViewById(R.id.first_name_edit_text);
        ImageView editFirstNameButton = findViewById(R.id.edit_first_name_button);
        saveFirstNameButton = findViewById(R.id.save_first_name_button);
        lastNameEditText = findViewById(R.id.last_name_edit_text);
        ImageView editLastNameButton = findViewById(R.id.edit_last_name_button);
        saveLastNameButton = findViewById(R.id.save_last_name_button);
        phoneNumberEditText = findViewById(R.id.phone_number_edit_text);
        ImageView editPhoneNumberButton = findViewById(R.id.edit_phone_number_button);
        savePhoneNumberButton = findViewById(R.id.save_phone_number_button);
        oldPasswordEditText = findViewById(R.id.old_password_edit_text);
        newPasswordEditText = findViewById(R.id.new_password_edit_text);
        updatePasswordButton = findViewById(R.id.update_password_button);
        becomeTeacherButton = findViewById(R.id.become_teacher_button);

        repository = new UserDataRepository(this);
        presenter = new ProfilePresenter(this, repository);
        presenter.init();

        Log.d(TAG, repository.getAuthToken());

        becomeTeacherButton.setOnClickListener(view -> presenter.becomeTeacher());

        editFirstNameButton.setOnClickListener(view -> {
            firstNameEditText.setEnabled(true);
            lastNameEditText.setEnabled(false);
            phoneNumberEditText.setEnabled(false);
            firstNameEditText.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(firstNameEditText, 1);
            if (saveFirstNameButton.getVisibility() == View.GONE)
                saveFirstNameButton.setVisibility(View.VISIBLE);
            if (saveLastNameButton.getVisibility() == View.VISIBLE)
                saveLastNameButton.setVisibility(View.GONE);
            if (savePhoneNumberButton.getVisibility() == View.VISIBLE)
                savePhoneNumberButton.setVisibility(View.GONE);
        });

        saveFirstNameButton.setOnClickListener(view -> presenter.inputFirstNameChanged());

        editLastNameButton.setOnClickListener(view -> {
            firstNameEditText.setEnabled(false);
            lastNameEditText.setEnabled(true);
            phoneNumberEditText.setEnabled(false);
            lastNameEditText.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(lastNameEditText, 1);
            if (saveFirstNameButton.getVisibility() == View.VISIBLE)
                saveFirstNameButton.setVisibility(View.GONE);
            if (saveLastNameButton.getVisibility() == View.GONE)
                saveLastNameButton.setVisibility(View.VISIBLE);
            if (savePhoneNumberButton.getVisibility() == View.VISIBLE)
                savePhoneNumberButton.setVisibility(View.GONE);
        });

        saveLastNameButton.setOnClickListener(view -> presenter.inputLastNameChanged());

        editPhoneNumberButton.setOnClickListener(view -> {
            firstNameEditText.setEnabled(false);
            lastNameEditText.setEnabled(false);
            phoneNumberEditText.setEnabled(true);
            phoneNumberEditText.requestFocus();
            InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.showSoftInput(phoneNumberEditText, 1);
            if (saveFirstNameButton.getVisibility() == View.VISIBLE)
                saveFirstNameButton.setVisibility(View.GONE);
            if (saveLastNameButton.getVisibility() == View.VISIBLE)
                saveLastNameButton.setVisibility(View.GONE);
            if (savePhoneNumberButton.getVisibility() == View.GONE)
                savePhoneNumberButton.setVisibility(View.VISIBLE);
        });

        savePhoneNumberButton.setOnClickListener(view -> presenter.inputPhoneNumberChanged());

        oldPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                passwordEditTextsChanged();
            }
        });

        newPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                passwordEditTextsChanged();
            }
        });

        updatePasswordButton.setOnClickListener(view -> presenter.updatePassword(oldPasswordEditText.getText().toString(), newPasswordEditText.getText().toString()));

        loginLogoutButton.setOnClickListener(view -> loginLogoutClick());

        findViewById(R.id.go_back).setOnClickListener(view -> onBackPressed());
    }

    private void passwordEditTextsChanged() {
        if (oldPasswordEditText.getText().toString().length() >= 8
                && newPasswordEditText.getText().toString().length() >= 8) {
            if (updatePasswordButton.getVisibility() == View.GONE)
                updatePasswordButton.setVisibility(View.VISIBLE);
        }
        else {
            if (updatePasswordButton.getVisibility() == View.VISIBLE)
                updatePasswordButton.setVisibility(View.GONE);
        }
    }

    @Override
    public void showUser(String email, UserDetail userDetail) {
        emailText.setText(email);
        userNameText.setText(userDetail.getUser().getUserName());
        firstNameEditText.setText(userDetail.getFirstName());
        lastNameEditText.setText(userDetail.getLastName());
        if (userDetail.getPhoneNumber() != null)
            phoneNumberEditText.setText(userDetail.getPhoneNumber().substring(3));
    }

    @Override
    public void becomeTeacherRequestSuccessful() {
        InfoDialog dialog = new InfoDialog(this);
        dialog.showDialog("Відправка запиту для статусу вчителя пройшла успішно");
        setBecomeTeacherButtonStatus();
    }

    @Override
    public void setBecomeTeacherButtonStatus() {
        if (presenter.getUserDetail().isTeacher()) {
            becomeTeacherButton.setVisibility(View.GONE);
        } else {
            if (presenter.getUserDetail().isRequestTeacher()) {
                becomeTeacherButton.setEnabled(false);
                becomeTeacherButton.setBackgroundResource(R.drawable.background_dialog_button_disable);
                becomeTeacherButton.setText("Запит для отримання статусу вчителя відправлений");
            }
        }
    }

    @Override
    public String getFirstName() {
        return firstNameEditText.getText().toString();
    }

    @Override
    public String getLastName() {
        return lastNameEditText.getText().toString();
    }

    @Override
    public String getPhoneNumber() {
        return phoneNumberEditText.getText().toString().replace("-", "");
    }

    @Override
    public void confirmFirstNameSavingDialog() {
        UpdateUserConfirmDialog dialog = new UpdateUserConfirmDialog(this, UserFieldName.FIRST_NAME, getFirstName());
        dialog.showDialog("Ви впевнені що хочити змінити своє ім'я?");
    }

    @Override
    public void confirmLastNameSavingDialog() {
        UpdateUserConfirmDialog dialog = new UpdateUserConfirmDialog(this, UserFieldName.LAST_NAME, getLastName());
        dialog.showDialog("Ви впевнені що хочити змінити свою фамілію?");
    }

    @Override
    public void confirmPhoneNumberNameSavingDialog() {
        UpdateUserConfirmDialog dialog = new UpdateUserConfirmDialog(this, UserFieldName.PHONE_NUMBER, getPhoneNumber());
        dialog.showDialog("Ви впевнені що хочити змінити свій номер телефону");
    }

    @Override
    public void phoneNumberIsIncompleteDialog() {
        InfoDialog infoDialog = new InfoDialog(this);
        infoDialog.showDialog("Введений номер телефону некоректний. Вам потрібно ввести 9 цифер.");
    }

    @Override
    public void confirmPasswordSavingDialog(Password password) {
        UpdatePasswordConfirmDialog dialog = new UpdatePasswordConfirmDialog(this, password);
        dialog.showDialog("Ви впевнені що хочете змінити свій пароль?");
    }

    @Override
    public void hideFirstNameKeyBoard() {
        firstNameEditText.clearFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(firstNameEditText.getWindowToken(), 0);
    }

    @Override
    public void hideLastNameKeyBoard() {
        lastNameEditText.clearFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(lastNameEditText.getWindowToken(), 0);
    }

    @Override
    public void hidePhoneNumberKeyBoard() {
        phoneNumberEditText.clearFocus();
        InputMethodManager imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(phoneNumberEditText.getWindowToken(), 0);
    }

    @Override
    public void showProgress(String message) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void loginLogoutClick() {
        if (repository.isUserAuthorized()) {
            LogoutConfirmDialog logoutConfirmDialog = new LogoutConfirmDialog(this);
            logoutConfirmDialog.showDialog("Ви впевнені що хочете вийти з акаунту?");
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    @Override
    public void setLoginLogoutIcon() {
        if (repository.isUserAuthorized())
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_logout));
        else
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_login));
    }

    @Override
    public void logout() {
        repository.logoutUser();
        setLoginLogoutIcon();
    }

    public void showUpdatedUser(UserDetail userDetail, UserFieldName userFieldName) {
        UserDetail newUserDetail;
        switch (userFieldName) {
            case FIRST_NAME:
                firstNameEditText.setText(userDetail.getFirstName());
                firstNameEditText.setEnabled(false);
                saveFirstNameButton.setVisibility(View.GONE);
                newUserDetail = repository.getUserDetail();
                newUserDetail.setFirstName(userDetail.getFirstName());
                repository.setUserDetail(newUserDetail);
                break;
            case LAST_NAME:
                lastNameEditText.setText(userDetail.getLastName());
                lastNameEditText.setEnabled(false);
                saveLastNameButton.setVisibility(View.GONE);
                newUserDetail = repository.getUserDetail();
                newUserDetail.setLastName(userDetail.getLastName());
                repository.setUserDetail(newUserDetail);
                break;
            case PHONE_NUMBER:
                phoneNumberEditText.setText(userDetail.getPhoneNumber().substring(3));
                phoneNumberEditText.setEnabled(false);
                savePhoneNumberButton.setVisibility(View.GONE);
                newUserDetail = repository.getUserDetail();
                newUserDetail.setPhoneNumber(userDetail.getPhoneNumber());
                repository.setUserDetail(newUserDetail);
                break;
            default:
                throw new IllegalStateException("Unexpected value: " + userFieldName);
        }
    }

    public void passwordUpdatedSuccessful() {
        InfoDialog infoDialog = new InfoDialog(this);
        infoDialog.showDialog("Оновлення паролю пройшло успішно");
        oldPasswordEditText.setText("");
        newPasswordEditText.setText("");
    }

    public void passwordUpdatedFailed() {
        InfoDialog infoDialog = new InfoDialog(this);
        infoDialog.showDialog("Не вдалося оновити пароль. Введений старий пароль не правельний");
        oldPasswordEditText.setText("");
        newPasswordEditText.setText("");
    }
}
