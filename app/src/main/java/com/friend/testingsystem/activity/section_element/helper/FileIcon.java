package com.friend.testingsystem.activity.section_element.helper;

import android.widget.ImageView;

import com.friend.testingsystem.R;

public class FileIcon {

    public static void setFileIconByFileExtension(ImageView fileIcon, String fileExtension) {
        switch (fileExtension) {
            case "png":
            case "jpg":
            case "jpeg":
            case "gif":
                fileIcon.setImageResource(R.drawable.ic_image_green);
                break;
            case "mp4":
            case "flv":
            case "avi":
            case "mov":
            case "mpg":
            case "wmv":
                fileIcon.setImageResource(R.drawable.ic_video_green);
                break;
            case "mp3":
            case "3gp":
            case "aac":
            case "ogg":
            case "wma":
            case "pcm":
            case "wav":
            case "aiff":
            case "flac":
            case "alac":
                fileIcon.setImageResource(R.drawable.ic_audio_green);
                break;
            case "doc":
            case "docx":
                fileIcon.setImageResource(R.drawable.ic_word_green);
                break;
            case "pdf":
                fileIcon.setImageResource(R.drawable.ic_pdf_green);
                break;
            case "xls":
            case "xlsx":
                fileIcon.setImageResource(R.drawable.ic_exel_green);
                break;
            case "ppt":
            case "pptx":
                fileIcon.setImageResource(R.drawable.ic_powerpoint_green);
                break;
            case "zip":
                fileIcon.setImageResource(R.drawable.ic_zip_green);
                break;
            case "rar":
                fileIcon.setImageResource(R.drawable.ic_rar_green);
                break;
            case "txt":
            case "html":
            case "xml":
            case "json":
            case "csv":
            case "css":
            default:
                fileIcon.setImageResource(R.drawable.ic_document_green);
                break;
        }
    }

}
