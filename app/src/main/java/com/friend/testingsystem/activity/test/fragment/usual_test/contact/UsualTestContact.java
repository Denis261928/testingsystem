package com.friend.testingsystem.activity.test.fragment.usual_test.contact;

import com.friend.testingsystem.activity.test.model.TestWithCheckBoxes;
import com.friend.testingsystem.model.Answer;
import com.friend.testingsystem.model.Test;

import java.util.List;

public interface UsualTestContact {
    interface View {
        void showGeneralQuestion(String generalQuestion);
        void showQuestion(String testNumberText, String questionText, String correctAnswerCountText, String questionImage);
        void showAnswers(List<Answer> answerList);
        void showAnswersTable(List<Answer> answerList, int correctAnswerCount);
        void setSaveButtonEnabled(boolean enabled);
        void setSaveButtonExam();
        void onTestSaved();
        void showTestHelpDialog(long testId);
    }

    interface Presenter {
        void init(int currentPosition, Test currentTest, List<Test> testList, boolean isTestExam);
        void setTestWithCheckBoxes(TestWithCheckBoxes testWithCheckBoxes);
        void answerClick(int index);
        void setButtonStatus();
        int saveTestAndGetScore();
        boolean isTestActive();
        void showHelp();
    }
}
