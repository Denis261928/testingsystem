package com.friend.testingsystem.activity.menu_fragment.contact;

import com.friend.testingsystem.helper.ActivityEnum;

public interface MenuContact {
    interface View {
        void showAuthDialog(String message);
        void navigateTo(ActivityEnum activity);
    }

    interface Presenter {
        void navigateToTestingHistory();
        void navigateToFriends();
        void navigateToCourse();
        void navigateToLogin();
        void navigateToProfile();
    }
}
