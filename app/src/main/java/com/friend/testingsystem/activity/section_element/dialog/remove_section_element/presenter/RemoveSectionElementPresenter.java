package com.friend.testingsystem.activity.section_element.dialog.remove_section_element.presenter;

import android.util.Log;

import com.friend.testingsystem.activity.section_element.dialog.remove_section_element.RemoveSectionElementConfirmDialog;
import com.friend.testingsystem.activity.section_element.dialog.remove_section_element.contact.RemoveSectionElementContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.SectionElement;
import com.friend.testingsystem.model.SectionElementId;
import com.friend.testingsystem.repository.UserDataRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RemoveSectionElementPresenter implements RemoveSectionElementContact.Presenter {
    private RemoveSectionElementContact.View view;
    private UserDataRepository repository;

    public RemoveSectionElementPresenter(RemoveSectionElementContact.View view, UserDataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void removeSectionElement(SectionElement sectionElement) {
        view.showProgress();
        SectionElementId sectionElementId = new SectionElementId(sectionElement.getId());
        NetworkService.getInstance().getSectionElementApi().removeSectionElement(sectionElementId)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            if (response.body().equals("Valid delete"))
                                view.sectionElementRemoved();
                        } else {
                            Log.d(RemoveSectionElementConfirmDialog.TAG, response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.d(RemoveSectionElementConfirmDialog.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }
}
