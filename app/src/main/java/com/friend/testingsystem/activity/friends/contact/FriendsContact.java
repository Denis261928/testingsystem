package com.friend.testingsystem.activity.friends.contact;

import com.friend.testingsystem.model.Friend;

import java.util.List;

public interface FriendsContact {
    interface View {
        void showUserFriends(List<Friend> friendList);
        void addAndShowUserFriends(List<Friend> friendList);
        void clearUserFriends();
        void hideLoadingItem();
        void showAddFriendDialog();
        void showRemoveFriendDialog(Friend friend);
        void showProgress();
        void hideProgress();
    }

    interface Presenter {
        boolean isLoading();
        void setLoading(boolean loading);
        boolean isLastPage();
        void loadInitial();
        void loadMore();
        void clearList();
        void addFriendClick();
        void removeFriendClick(Friend friend);
    }
}
