package com.friend.testingsystem.activity.profile.dialog.update_password;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.profile.ProfileActivity;
import com.friend.testingsystem.activity.profile.dialog.update_password.contact.UpdatePasswordContact;
import com.friend.testingsystem.activity.profile.dialog.update_password.presenter.UpdatePasswordPresenter;
import com.friend.testingsystem.dialog.interfaces.MyDialogWithMessage;
import com.friend.testingsystem.model.Password;
import com.friend.testingsystem.repository.UserDataRepository;

public class UpdatePasswordConfirmDialog implements MyDialogWithMessage, UpdatePasswordContact.View {
    public static final String TAG = "UpdatePasswordConfirmDialogLog";

    private Dialog dialog;
    private ProgressDialog progressDialog;

    private Activity parentActivity;
    private UpdatePasswordPresenter updatePasswordPresenter;

    public UpdatePasswordConfirmDialog(Activity parentActivity, Password password) {
        this.parentActivity = parentActivity;
        UserDataRepository repository = new UserDataRepository(parentActivity);
        updatePasswordPresenter = new UpdatePasswordPresenter(this, repository, password);
    }

    @Override
    public void showDialog(String message) {
        dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_confirm);

        TextView messageText = dialog.findViewById(R.id.dialog_message);
        Button positiveButton = dialog.findViewById(R.id.dialog_positive_button);
        Button negativeButton = dialog.findViewById(R.id.dialog_negative_button);

        messageText.setText(message);

        positiveButton.setOnClickListener(view -> updatePasswordPresenter.updatePassword());

        negativeButton.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }

    @Override
    public void showDialog(String title, String message) {
        dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_confirm);

        TextView messageText = dialog.findViewById(R.id.dialog_message);
        TextView titleText = dialog.findViewById(R.id.dialog_title);
        Button positiveButton = dialog.findViewById(R.id.dialog_positive_button);
        Button negativeButton = dialog.findViewById(R.id.dialog_negative_button);

        titleText.setText(title);
        messageText.setText(message);

        positiveButton.setOnClickListener(view -> updatePasswordPresenter.updatePassword());

        negativeButton.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }


    @Override
    public void passwordUpdatedSuccessful() {
        dialog.dismiss();
        ProfileActivity profileActivity = (ProfileActivity) parentActivity;
        profileActivity.passwordUpdatedSuccessful();
    }

    @Override
    public void passwordUpdatedFailed() {
        dialog.dismiss();
        ProfileActivity profileActivity = (ProfileActivity) parentActivity;
        profileActivity.passwordUpdatedFailed();
    }

    @Override
    public void showProgress(String message) {
        progressDialog = new ProgressDialog(parentActivity);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }
}
