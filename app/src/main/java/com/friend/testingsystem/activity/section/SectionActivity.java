package com.friend.testingsystem.activity.section;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.CommonActivity;
import com.friend.testingsystem.activity.MainActivity;
import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.activity.section.adapter.SectionAdapter;
import com.friend.testingsystem.activity.section.contact.SectionContact;
import com.friend.testingsystem.activity.section.dialog.CreateSectionDialog;
import com.friend.testingsystem.activity.section.presenter.SectionPresenter;
import com.friend.testingsystem.activity.section_element.SectionElementActivity;
import com.friend.testingsystem.dialog.LogoutConfirmDialog;
import com.friend.testingsystem.helper.NetworkUtils;
import com.friend.testingsystem.model.Course;
import com.friend.testingsystem.model.Section;
import com.friend.testingsystem.repository.UserDataRepository;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class SectionActivity extends AppCompatActivity implements SectionContact.View, CommonActivity {
    public static final String TAG = "SectionActivityLog";

    private Handler handler;
    private ProgressDialog progressDialog;
    private ImageView loginLogoutButton;
    private ListView sectionListView;
    private FloatingActionButton createSectionButton;
    private View loadingView;

    private SectionAdapter adapter;
    private UserDataRepository repository;
    private SectionPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_section);

        ImageView goBack = findViewById(R.id.go_back);
        loginLogoutButton = findViewById(R.id.login_logout_icon);
        sectionListView = findViewById(R.id.section_list);
        createSectionButton = findViewById(R.id.create_section_button);

        if (!NetworkUtils.getNetworkConnection(this)) {
            startActivity(new Intent(this, MainActivity.class));
        }

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.item_loading, null);

        View headerView = inflater.inflate(R.layout.item_section_header, null);
        TextView courseNameText = headerView.findViewById(R.id.course_name_text);
        Course course = (Course) getIntent().getSerializableExtra("course");
        if (course != null) {
            courseNameText.setText(course.getName());
        }
        sectionListView.addHeaderView(headerView);

        handler = new MyHandler();

        sectionListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Intent intent = new Intent(SectionActivity.this, SectionElementActivity.class);
                intent.putExtra("section", adapter.getSectionList().get(i - 1));
                startActivity(intent);
            }
        });

        repository = new UserDataRepository(this);
        setLoginLogoutIcon();
        presenter = new SectionPresenter(this, repository, course);
        presenter.loadInitial();

        loginLogoutButton.setOnClickListener(view -> loginLogoutClick());

        goBack.setOnClickListener(view -> onBackPressed());

        createSectionButton.setOnClickListener(view -> presenter.createSectionClick());

        sectionListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) { }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                // Check when scroll to last item in listView, in this tut, init data in list view
                if (absListView.getLastVisiblePosition() == i2-1 && sectionListView.getCount() >= 10
                        && !presenter.isLoading() && !presenter.isLastPage()) {
                    presenter.setLoading(true);
                    Thread thread = new ThreadGetMoreData();
                    thread.start();
                }
            }
        });

    }

    @SuppressLint("HandlerLeak")
    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case 0:
                    sectionListView.addFooterView(loadingView);
                    break;
                case 1:
                    adapter.addListItemsToAdapter((List<Section>) msg.obj);
                    sectionListView.removeFooterView(loadingView);
                    presenter.setLoading(false);
                    break;
                case 2:
                    sectionListView.removeFooterView(loadingView);
                    presenter.setLoading(false);
                    break;
                case 3:
                    adapter.clearSectionList();
                    presenter.loadInitial();
                    break;
                default:
                    break;
            }
        }
    }

    public class ThreadGetMoreData extends Thread {
        @Override
        public void run() {
            // Add loading view during search processing
            handler.sendEmptyMessage(0);
            // Search more data
            presenter.loadMore();
        }
    }

    @Override
    public void setCreateSectionButtonVisible(boolean visible) {
        if (visible)
            createSectionButton.setVisibility(View.VISIBLE);
        else
            createSectionButton.setVisibility(View.GONE);
    }

    @Override
    public void showSections(List<Section> sectionList) {
        adapter = new SectionAdapter(this, sectionList);
        sectionListView.setAdapter(adapter);
    }

    @Override
    public void addAndShowSections(List<Section> sectionList) {
        // Update data adapter and UI
        Message message = handler.obtainMessage(1, sectionList);
        handler.sendMessage(message);
    }

    @Override
    public void clearSections() {
        handler.sendEmptyMessage(3);
    }

    @Override
    public void hideLoadingItem() {
        // Remove loading view when there is not any other data
        handler.sendEmptyMessage(2);
    }

    @Override
    public void showCreateSectionDialog(Course course) {
        CreateSectionDialog dialog = new CreateSectionDialog(this, presenter, course);
        dialog.showDialog();
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Завантаження розділів");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void loginLogoutClick() {
        if (repository.isUserAuthorized()) {
            LogoutConfirmDialog logoutConfirmDialog = new LogoutConfirmDialog(this);
            logoutConfirmDialog.showDialog("Ви впевнені що хочете вийти з акаунту?");
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    @Override
    public void setLoginLogoutIcon() {
        if (repository.isUserAuthorized())
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_logout));
        else
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_login));
    }

    @Override
    public void logout() {
        repository.logoutUser();
        setLoginLogoutIcon();
        startActivity(new Intent(this, CategoryActivity.class));
    }
}
