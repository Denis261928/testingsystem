package com.friend.testingsystem.activity;

public interface CommonActivity {
    void loginLogoutClick();
    void setLoginLogoutIcon();
    void logout();
}
