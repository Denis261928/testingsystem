package com.friend.testingsystem.activity.category;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.CommonActivity;
import com.friend.testingsystem.activity.MainActivity;
import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.category.adapter.CategoryAdapter;
import com.friend.testingsystem.activity.category.viewmodel.CategoryViewModel;
import com.friend.testingsystem.activity.sub_category.SubCategoryActivity;
import com.friend.testingsystem.dialog.LogoutConfirmDialog;
import com.friend.testingsystem.helper.NetworkUtils;
import com.friend.testingsystem.model.Category;
import com.friend.testingsystem.repository.UserDataRepository;

import java.io.Serializable;

public class CategoryActivity extends AppCompatActivity implements CommonActivity, CategoryAdapter.OnCategoryListener {
    public static final String TAG = "CategoryActivityLog";

    private ImageView loginLogoutButton;

    private UserDataRepository repository;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        loginLogoutButton = findViewById(R.id.login_logout_icon);
        RecyclerView recyclerView = findViewById(R.id.categories_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        // recyclerView.setHasFixedSize(true);

        if (!NetworkUtils.getNetworkConnection(this)) {
            startActivity(new Intent(this, MainActivity.class));
        }

        repository = new UserDataRepository(this);
        setLoginLogoutIcon();
        Log.d(TAG, repository.getAuthToken());

        loginLogoutButton.setOnClickListener(view -> loginLogoutClick());

        CategoryViewModel categoryViewModel = ViewModelProviders.of(this, new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new CategoryViewModel();
            }
        }).get(CategoryViewModel.class);

        final CategoryAdapter adapter = new CategoryAdapter(this, this);
        categoryViewModel.categoryPageList.observe(this, new Observer<PagedList<Category>>() {
            @Override
            public void onChanged(PagedList<Category> categories) {
                adapter.submitList(categories);
            }
        });
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void loginLogoutClick() {
        if (repository.isUserAuthorized()) {
            LogoutConfirmDialog logoutConfirmDialog = new LogoutConfirmDialog(this);
            logoutConfirmDialog.showDialog("Ви впевнені що хочете вийти з акаунту?");
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    @Override
    public void setLoginLogoutIcon() {
        if (repository.isUserAuthorized())
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_logout));
        else
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_login));
    }

    @Override
    public void logout() {
        repository.logoutUser();
        setLoginLogoutIcon();
    }

    @Override
    public void onCategoryClick(Category category) {
        Intent intent = new Intent(CategoryActivity.this, SubCategoryActivity.class);
        intent.putExtra("category", (Serializable) category);
        startActivity(intent);
    }
}
