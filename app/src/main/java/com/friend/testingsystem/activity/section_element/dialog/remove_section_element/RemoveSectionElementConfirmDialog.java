package com.friend.testingsystem.activity.section_element.dialog.remove_section_element;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.section_element.dialog.remove_section_element.contact.RemoveSectionElementContact;
import com.friend.testingsystem.activity.section_element.dialog.remove_section_element.presenter.RemoveSectionElementPresenter;
import com.friend.testingsystem.activity.section_element.presenter.SectionElementPresenter;
import com.friend.testingsystem.dialog.interfaces.MyDialogWithMessage;
import com.friend.testingsystem.model.SectionElement;
import com.friend.testingsystem.repository.UserDataRepository;

public class RemoveSectionElementConfirmDialog implements MyDialogWithMessage, RemoveSectionElementContact.View {
    public static final String TAG = "AddFriendDialogLog";

    private Dialog dialog;
    private ProgressDialog progressDialog;

    private SectionElement sectionElement;
    private Activity parentActivity;
    private RemoveSectionElementPresenter removeSectionElementPresenter;
    private SectionElementPresenter sectionElementPresenter;

    public RemoveSectionElementConfirmDialog(Activity parentActivity, SectionElementPresenter sectionElementPresenter,
                                             SectionElement sectionElement) {
        this.parentActivity = parentActivity;
        this.sectionElementPresenter = sectionElementPresenter;
        this.sectionElement = sectionElement;
        UserDataRepository repository = new UserDataRepository(parentActivity);
        removeSectionElementPresenter = new RemoveSectionElementPresenter(this, repository);
    }

    @Override
    public void showDialog(String message) {
        dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_confirm);

        TextView messageText = dialog.findViewById(R.id.dialog_message);
        Button positiveButton = dialog.findViewById(R.id.dialog_positive_button);
        Button negativeButton = dialog.findViewById(R.id.dialog_negative_button);

        messageText.setText(message);

        positiveButton.setOnClickListener(v -> removeSectionElementPresenter.removeSectionElement(sectionElement));

        negativeButton.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }

    @Override
    public void showDialog(String title, String message) {
        dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_confirm);

        TextView messageText = dialog.findViewById(R.id.dialog_message);
        TextView titleText = dialog.findViewById(R.id.dialog_title);
        Button positiveButton = dialog.findViewById(R.id.dialog_positive_button);
        Button negativeButton = dialog.findViewById(R.id.dialog_negative_button);

        titleText.setText(title);
        messageText.setText(message);

        positiveButton.setOnClickListener(v -> removeSectionElementPresenter.removeSectionElement(sectionElement));

        negativeButton.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }

    @Override
    public void sectionElementRemoved() {
        dialog.dismiss();
        sectionElementPresenter.clearList();
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(parentActivity);
        progressDialog.setMessage("Видалення матеріалу");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }
}
