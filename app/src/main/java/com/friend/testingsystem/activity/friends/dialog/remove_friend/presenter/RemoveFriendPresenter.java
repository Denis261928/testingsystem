package com.friend.testingsystem.activity.friends.dialog.remove_friend.presenter;

import android.util.Log;

import com.friend.testingsystem.activity.friends.dialog.remove_friend.RemoveFriendConfirmDialog;
import com.friend.testingsystem.activity.friends.dialog.remove_friend.contact.RemoveFriendContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.Friend;
import com.friend.testingsystem.model.User;
import com.friend.testingsystem.repository.UserDataRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RemoveFriendPresenter implements RemoveFriendContact.Presenter {
    private RemoveFriendContact.View view;
    private UserDataRepository repository;

    public RemoveFriendPresenter(RemoveFriendContact.View view, UserDataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void removeFriend(Friend friend) {
        view.showProgress();
        User user = new User();
        user.setUserName(friend.getUserName());
        NetworkService.getInstance().getFriendsApi().removeFriend("Token "+repository.getAuthToken(), user)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            if (response.body().equals("ok"))
                                view.friendRemoved();
                        } else {
                            Log.d(RemoveFriendConfirmDialog.TAG, response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.d(RemoveFriendConfirmDialog.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }
}
