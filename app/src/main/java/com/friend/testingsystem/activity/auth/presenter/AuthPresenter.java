package com.friend.testingsystem.activity.auth.presenter;

import android.util.Base64;
import android.util.Log;

import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.auth.RegistrationActivity;
import com.friend.testingsystem.activity.auth.contact.AuthContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.AuthToken;
import com.friend.testingsystem.model.DecodedToken;
import com.friend.testingsystem.model.User;
import com.friend.testingsystem.model.UserDetail;
import com.friend.testingsystem.model.UserInfo;
import com.friend.testingsystem.repository.UserDataRepository;
import com.google.gson.Gson;

import java.io.UnsupportedEncodingException;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AuthPresenter implements AuthContact.Presenter {
    private UserDataRepository repository;
    private AuthContact.View view;

    public AuthPresenter(AuthContact.View view, UserDataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void login(String userName, String password) {
        User user = new User(userName, password);
        view.showProgress("Авторизація");
        NetworkService.getInstance().getAuthApi().getAuthToken(user).enqueue(new Callback<AuthToken>() {
            @Override
            public void onResponse(Call<AuthToken> call, Response<AuthToken> response) {
                if (response.isSuccessful()) {
                    AuthToken token = response.body();
                    Log.d(LoginActivity.TAG, token.toString());
                    repository.setAuthToken(token.getAuthToken());
                    view.hideProgress();
                    view.success();
                    getUserInfo();
                }
                else {
                    view.hideProgress();
                    view.showErrorDialog("Помилка", "Ви ввели не корректні дані.");
                }
            }

            @Override
            public void onFailure(Call<AuthToken> call, Throwable t) {
                view.hideProgress();
                Log.d(LoginActivity.TAG, t.getMessage());
            }
        });
    }

    @Override
    public void getUserInfo() {
        view.showProgress("Завантаження інформації про користувача");

        NetworkService.getInstance().getAuthApi().getUserInfo("Token "+repository.getAuthToken())
                .enqueue(new Callback<UserInfo>() {
                    @Override
                    public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                        if (response.isSuccessful()) {
                            UserInfo userInfo = response.body();
                            repository.setUserId(userInfo.getId());
                            repository.setEmail(userInfo.getEmail());
                            getUserDetail();
                        } else {
                            Log.d(LoginActivity.TAG, response.errorBody().toString());
                            view.hideProgress();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserInfo> call, Throwable t) {
                        Log.d(LoginActivity.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }

    @Override
    public void getUserDetail() {
        NetworkService.getInstance().getProfileApi().getUserDetailById(repository.getUserId())
                .enqueue(new Callback<UserDetail>() {
                    @Override
                    public void onResponse(Call<UserDetail> call, Response<UserDetail> response) {
                        if (response.isSuccessful()) {
                            UserDetail userDetail = response.body();
                            repository.setUserDetail(userDetail);
                        } else {
                            Log.d(LoginActivity.TAG, response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @Override
                    public void onFailure(Call<UserDetail> call, Throwable t) {
                        Log.d(LoginActivity.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }

    @Override
    public void registration(String email, String userName, String password) {
        User user = new User(email, userName, password);
        view.showProgress("Реєстрація");
        NetworkService.getInstance().getAuthApi().registerUser(user).enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if (response.isSuccessful()) {
                    Log.d(RegistrationActivity.TAG, response.body().toString());
                    view.hideProgress();
                    view.success();
                }
                else {
                    Log.d(RegistrationActivity.TAG, response.errorBody().toString());
                    view.hideProgress();
                    view.showErrorDialog("Помилка", "Ви ввели не корректні дані.");
                }
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                view.hideProgress();
                Log.d(RegistrationActivity.TAG, t.getMessage());
            }
        });
    }

    public void decoded(String JWTEncoded) throws Exception {
        try {
            String[] split = JWTEncoded.split("\\.");
            Log.d(LoginActivity.TAG, "JWT_DECODED: Header: " + getJson(split[0]));
            Log.d(LoginActivity.TAG, "JWT_DECODED: Body: " + getJson(split[1]));

            Gson g = new Gson();
            DecodedToken decodedToken = g.fromJson(getJson(split[1]), DecodedToken.class);

            repository.setUserId(decodedToken.getUserId());
        } catch (UnsupportedEncodingException e) {
            Log.d(LoginActivity.TAG, "Decode token error!");
        }
    }

    private static String getJson(String strEncoded) throws UnsupportedEncodingException {
        byte[] decodedBytes = Base64.decode(strEncoded, Base64.URL_SAFE);
        return new String(decodedBytes, "UTF-8");
    }
}
