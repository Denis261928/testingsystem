package com.friend.testingsystem.activity.section.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.model.Section;

import java.util.List;

public class SectionAdapter extends BaseAdapter {
    private Context context;
    private List<Section> sectionList;

    public SectionAdapter(Context context, List<Section> sectionList) {
        this.context = context;
        this.sectionList = sectionList;
    }

    public void addListItemsToAdapter(List<Section> list) {
        sectionList.addAll(list);
        notifyDataSetChanged();
    }

    public void clearSectionList() {
        sectionList.clear();
        notifyDataSetChanged();
    }

    public List<Section> getSectionList() {
        return sectionList;
    }

    @Override
    public int getCount() {
        return sectionList.size();
    }

    @Override
    public Object getItem(int i) {
        return sectionList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(context, R.layout.item_section, null);

        TextView sectionNameText = v.findViewById(R.id.section_name_text);
        sectionNameText.setText(sectionList.get(i).getName());

        return v;
    }
}
