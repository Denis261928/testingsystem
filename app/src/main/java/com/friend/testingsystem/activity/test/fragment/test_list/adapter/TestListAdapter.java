package com.friend.testingsystem.activity.test.fragment.test_list.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.test.model.UserAnswer;
import com.friend.testingsystem.activity.test.model.UserTask;
import com.friend.testingsystem.activity.test.model.UserTest;
import com.friend.testingsystem.model.Test;
import com.friend.testingsystem.repository.TestDataRepository;

import java.util.List;

public class TestListAdapter extends RecyclerView.Adapter<TestListAdapter.TestViewHolder> {
    private Context context;
    private List<Test> testList;
    private OnTestListener onTestListener;

    public TestListAdapter(Context context, List<Test> testList, OnTestListener onTestListener) {
        this.context = context;
        this.testList = testList;
        this.onTestListener = onTestListener;
    }

    public void setDataSet(List<Test> testList) {
        this.testList = testList;
    }

    @NonNull
    @Override
    public TestViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_test_number, parent, false);

        return new TestViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TestViewHolder holder, int position) {
        holder.bind(testList.get(position), position, onTestListener);
    }

    @Override
    public int getItemCount() {
        return testList.size();
    }

    class TestViewHolder extends RecyclerView.ViewHolder {
        private LinearLayout testNumberContainer;
        private TextView testNumberText;

        private Test test;
        private int position;

        public TestViewHolder(@NonNull View itemView) {
            super(itemView);

            testNumberContainer = itemView.findViewById(R.id.test_number_container);
            testNumberText = itemView.findViewById(R.id.test_number_text);
        }

        public void bind(final Test test, final int position, final OnTestListener listener) {
            this.test = test;
            this.position = position;

            testNumberText.setText(String.valueOf(position+1));

            setTestNumberBackground();

            itemView.setOnClickListener(view -> listener.onTestClick(position + 1));
        }

        private void setTestNumberBackground() {
            UserTest userTest = TestDataRepository.getUserTestList().get(position);
            if (userTest.isActive()) {
                testNumberText.setTextColor(context.getResources().getColor(R.color.colorText));
                int minCheckedElementsForEnabled = userTest.getUserTaskList().size();
                int countOfCheckedElements = 0;
                if (userTest.isWatched()) {
                    for (UserTask userTask : userTest.getUserTaskList()) {
                        for (UserAnswer userAnswer : userTask.getUserAnswerList()) {
                            if (userAnswer.isChecked()) {
                                countOfCheckedElements++;
                            }
                        }
                    }
                }
                if (countOfCheckedElements >= minCheckedElementsForEnabled)
                    testNumberContainer.setBackgroundResource(R.drawable.background_test_number_checked);
                else
                    testNumberContainer.setBackgroundResource(R.drawable.background_test_number);

            }
            else {
                testNumberText.setTextColor(context.getResources().getColor(R.color.colorTextWhite));
                if (userTest.getScoreForTheTest() == 0)
                    testNumberContainer.setBackgroundResource(R.drawable.background_test_number_wrong);
                else if (userTest.getScoreForTheTest() == userTest.getMaxScoreForTheTest())
                    testNumberContainer.setBackgroundResource(R.drawable.background_test_number_correct);
                else
                    testNumberContainer.setBackgroundResource(R.drawable.background_test_number_half_correct);
            }
        }
    }

    public interface OnTestListener {
        void onTestClick(int testNumber);
    }
}
