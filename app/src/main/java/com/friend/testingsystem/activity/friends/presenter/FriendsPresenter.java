package com.friend.testingsystem.activity.friends.presenter;

import android.util.Log;

import com.friend.testingsystem.activity.friends.FriendsActivity;
import com.friend.testingsystem.activity.friends.contact.FriendsContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.Friend;
import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.model.Friends;
import com.friend.testingsystem.repository.UserDataRepository;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class FriendsPresenter implements FriendsContact.Presenter {
    private int pageNumber;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private FriendsContact.View view;
    private UserDataRepository repository;

    public FriendsPresenter(FriendsContact.View view, UserDataRepository repository) {
        this.view = view;
        this.repository = repository;
        pageNumber = 1;
    }

    @Override
    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public void setLoading(boolean loading) {
        this.isLoading = loading;
    }

    @Override
    public boolean isLastPage() {
        return isLastPage;
    }

    @Override
    public void loadInitial() {
        pageNumber = 1;
        view.showProgress();
        NetworkService.getInstance().getFriendsApi().getFriendsByUserId(repository.getUserId(), pageNumber)
                .enqueue(new Callback<ResponseListData<Friends>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<Friends>> call, Response<ResponseListData<Friends>> response) {
                        if (response.isSuccessful()) {
                            view.hideProgress();
                            List<Friend> friendList = response.body().getResults().get(0).getFriendList();
                            view.showUserFriends(friendList);
                            pageNumber++;
                        } else {
                            view.hideProgress();
                            Log.d(FriendsActivity.TAG, response.errorBody().toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<Friends>> call, Throwable t) {
                        Log.d(FriendsActivity.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }

    @Override
    public void loadMore() {
        NetworkService.getInstance().getFriendsApi().getFriendsByUserId(repository.getUserId(), pageNumber)
                .enqueue(new Callback<ResponseListData<Friends>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<Friends>> call, Response<ResponseListData<Friends>> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getNext() == null) {
                                view.hideLoadingItem();
                                isLastPage = true;
                            } else {
                                view.addAndShowUserFriends(response.body().getResults().get(0).getFriendList());
                            }
                        } else {
                            Log.d(FriendsActivity.TAG, response.errorBody().toString());
                        }
                        pageNumber++;
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<Friends>> call, Throwable t) {
                        Log.d(FriendsActivity.TAG, t.getMessage());
                    }
                });
    }

    @Override
    public void clearList() {
        view.clearUserFriends();
    }

    @Override
    public void addFriendClick() {
        view.showAddFriendDialog();
    }

    @Override
    public void removeFriendClick(Friend friend) {
        view.showRemoveFriendDialog(friend);
    }
}
