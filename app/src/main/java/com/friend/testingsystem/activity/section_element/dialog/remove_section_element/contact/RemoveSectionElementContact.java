package com.friend.testingsystem.activity.section_element.dialog.remove_section_element.contact;

import com.friend.testingsystem.model.SectionElement;

public interface RemoveSectionElementContact {
    interface View {
        void sectionElementRemoved();
        void showProgress();
        void hideProgress();
    }
    interface Presenter {
        void removeSectionElement(SectionElement sectionElement);
    }
}
