package com.friend.testingsystem.activity.testing_history.contact;

import com.friend.testingsystem.model.Friend;
import com.friend.testingsystem.model.TestingHistoryItem;

import java.util.List;

public interface TestingHistoryContact {
    interface View {
        void showTestingHistory(List<TestingHistoryItem> testingHistoryList);
        void addAndShowTestingHistory(List<TestingHistoryItem> testingHistoryList);
        void hideLoadingItem();
        void showProgress();
        void hideProgress();
    }

    interface Presenter {
        boolean isLoading();
        void setLoading(boolean loading);
        boolean isLastPage();
        Friend getFriend();
        void setFriend(Friend friend);
        boolean isFriend();
        void loadInitial();
        void loadMore();
    }
}
