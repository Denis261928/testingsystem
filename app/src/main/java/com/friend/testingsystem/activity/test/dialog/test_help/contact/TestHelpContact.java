package com.friend.testingsystem.activity.test.dialog.test_help.contact;

import com.friend.testingsystem.model.TestHelp;

import java.util.List;

public interface TestHelpContact {
    interface View {
        void showTestHelp(List<TestHelp> testHelpList);
        void showDialogInfo(String message);
        void showProgress();
        void hideProgress();
    }

    interface Presenter {
        void init(long testId);
    }
}
