package com.friend.testingsystem.activity.auth.contact;

public interface AuthContact {
    interface View {
        void showErrorDialog(String title, String message);
        void success();
        void showProgress(String message);
        void hideProgress();
    }

    interface Presenter {
        void login(String userName, String password);
        void getUserInfo();
        void getUserDetail();
        void registration(String email, String userName, String password);
    }
}
