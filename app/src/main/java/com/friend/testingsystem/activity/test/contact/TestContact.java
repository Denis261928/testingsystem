package com.friend.testingsystem.activity.test.contact;

import com.friend.testingsystem.model.Test;

import java.util.List;

public interface TestContact {
    interface View {
        void showTests(List<Test> testList);
        void showScoreForTheTest(int score, int maxScore, int percentage);
        void startProgress(String message);
        void hideProgress();
        void showCancelTestConfirmDialog();
        void goToCategory();
        void goToTestingHistory();
    }

    interface Presenter {
        void loadTest(long topicId);
        void finishTest(List<TestContact.TestFragment> testFragmentList);
        int getMaxScoreForTheTests();
        void goOut();
        void goOutOfTheTestWithoutSaving();
    }

    interface TestFragment {
        int saveTestAndGetScore();
    }
}
