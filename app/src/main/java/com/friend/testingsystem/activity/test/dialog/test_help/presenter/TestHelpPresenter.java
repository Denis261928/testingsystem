package com.friend.testingsystem.activity.test.dialog.test_help.presenter;

import android.util.Log;

import com.friend.testingsystem.activity.test.dialog.test_help.TestHelpDialog;
import com.friend.testingsystem.activity.test.dialog.test_help.contact.TestHelpContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.TestHelp;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestHelpPresenter implements TestHelpContact.Presenter {

    private TestHelpContact.View view;

    public TestHelpPresenter(TestHelpContact.View view) {
        this.view = view;
    }

    @Override
    public void init(long testId) {
        Log.d(TestHelpDialog.TAG, testId+"");
        view.showProgress();
        NetworkService.getInstance().getTestApi().getTestHelpByTestId(testId)
                .enqueue(new Callback<List<TestHelp>>() {
                    @Override
                    public void onResponse(Call<List<TestHelp>> call, Response<List<TestHelp>> response) {
                        if (response.isSuccessful()) {
                            List<TestHelp> testHelpList = response.body();
                            if (testHelpList.size() > 0)
                                view.showTestHelp(response.body());
                            else
                                view.showDialogInfo("Підказки немає");
                        }
                        else {
                            Log.d(TestHelpDialog.TAG, response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @Override
                    public void onFailure(Call<List<TestHelp>> call, Throwable t) {
                        view.hideProgress();
                        Log.d(TestHelpDialog.TAG, t.getMessage());
                    }
                });
    }
}
