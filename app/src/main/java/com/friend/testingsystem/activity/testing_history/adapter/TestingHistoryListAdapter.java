package com.friend.testingsystem.activity.testing_history.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.model.TestingHistoryItem;

import java.util.List;

public class TestingHistoryListAdapter extends BaseAdapter {
    private Context context;
    private List<TestingHistoryItem> testingHistoryList;

    public TestingHistoryListAdapter(Context context, List<TestingHistoryItem> testingHistoryList) {
        this.context = context;
        this.testingHistoryList = testingHistoryList;
    }

    public void addListItemsToAdapter(List<TestingHistoryItem> list) {
        this.testingHistoryList.addAll(list);
        this.notifyDataSetChanged();
    }

    @Override
    public int getCount() {
        return testingHistoryList.size();
    }

    @Override
    public Object getItem(int i) {
        return testingHistoryList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(context, R.layout.item_testing_history, null);

        TextView topicNameText = v.findViewById(R.id.topic_name_text);
        TextView testScoreText = v.findViewById(R.id.test_score_text);
        TextView dateText = v.findViewById(R.id.date_text);
        RelativeLayout testPercentageScoreContainer = v.findViewById(R.id.test_percentage_score_container);
        TextView testPercentageScoreText = v.findViewById(R.id.test_percentage_score_text);

        topicNameText.setText(testingHistoryList.get(i).getTopic().getName());
        testScoreText.setText(testingHistoryList.get(i).getCorrectAnswerCount() +" з "
                + testingHistoryList.get(i).getAllAnswerCount());

        String dateString = testingHistoryList.get(i).getDateString();
        dateString = dateString.replaceAll("-", ".");
        dateText.setText(dateString);

        testPercentageScoreText.setText(testingHistoryList.get(i).getPercentageScore() + "%");

        if (testingHistoryList.get(i).getPercentageScore() < 11)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score1);
        else if (testingHistoryList.get(i).getPercentageScore() < 21)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score2);
        else if (testingHistoryList.get(i).getPercentageScore() < 31)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score3);
        else if (testingHistoryList.get(i).getPercentageScore() < 41)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score4);
        else if (testingHistoryList.get(i).getPercentageScore() < 51)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score5);
        else if (testingHistoryList.get(i).getPercentageScore() < 61)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score6);
        else if (testingHistoryList.get(i).getPercentageScore() < 71)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score7);
        else if (testingHistoryList.get(i).getPercentageScore() < 81)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score8);
        else if (testingHistoryList.get(i).getPercentageScore() < 91)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score9);

        return v;
    }

}
