package com.friend.testingsystem.activity.topic.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.friend.testingsystem.activity.topic.data_source.TopicDataSourceFactory;
import com.friend.testingsystem.model.Topic;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class TopicViewModel extends ViewModel {
    public LiveData<PagedList<Topic>> topicPageList;
    public LiveData<PageKeyedDataSource<Integer, Topic>> liveDataSource;

    public TopicViewModel(long subCategoryId) {
        Executor executor = Executors.newFixedThreadPool(5);

        TopicDataSourceFactory topicDataSourceFactory = new TopicDataSourceFactory(subCategoryId);
        liveDataSource = topicDataSourceFactory.getTopicLiveDataSource();

        PagedList.Config config = (new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(10)
                .setInitialLoadSizeHint(20))
                .build();

        topicPageList = (new LivePagedListBuilder(topicDataSourceFactory, config))
                .setFetchExecutor(executor)
                .build();
    }
}
