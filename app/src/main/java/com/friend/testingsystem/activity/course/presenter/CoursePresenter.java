package com.friend.testingsystem.activity.course.presenter;

import android.util.Log;

import com.friend.testingsystem.activity.course.CourseActivity;
import com.friend.testingsystem.activity.course.сontact.CourseContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.Course;
import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.repository.UserDataRepository;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CoursePresenter implements CourseContact.Presenter {
    private int pageNumber;
    private boolean isLoading = false;
    private boolean isLastPage = false;

    private CourseContact.View view;
    private UserDataRepository repository;

    public CoursePresenter(CourseContact.View view, UserDataRepository repository) {
        this.view = view;
        this.repository = repository;
        pageNumber = 1;
        view.setCreateCourseButtonVisible(repository.getUserDetail().isTeacher());
    }

    @Override
    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public void setLoading(boolean loading) {
        this.isLoading = loading;
    }

    @Override
    public boolean isLastPage() {
        return isLastPage;
    }

    @Override
    public void loadInitial() {
        pageNumber = 1;
        view.showProgress();
        NetworkService.getInstance().getCourseApi().getAllCourseListByPage(pageNumber)
                .enqueue(new Callback<ResponseListData<Course>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<Course>> call, Response<ResponseListData<Course>> response) {
                        if (response.isSuccessful()) {
                            view.hideProgress();
                            List<Course> courseList = response.body().getResults();
                            view.showCourses(courseList, false);
                            pageNumber++;
                        }
                        else {
                            view.hideProgress();
                            Log.d(CourseActivity.TAG, response.errorBody().toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<Course>> call, Throwable t) {
                        Log.d(CourseActivity.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }

    @Override
    public void loadMore() {
        NetworkService.getInstance().getCourseApi().getAllCourseListByPage(pageNumber)
                .enqueue(new Callback<ResponseListData<Course>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<Course>> call, Response<ResponseListData<Course>> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getNext() == null) {
                                view.hideLoadingItem();
                                isLastPage = true;
                            } else {
                                view.addAndShowCourses(response.body().getResults());
                            }
                        }
                        else {
                            Log.d(CourseActivity.TAG, response.errorBody().toString());
                        }
                        pageNumber++;
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<Course>> call, Throwable t) {
                        Log.d(CourseActivity.TAG, t.getMessage());
                    }
                });
    }

    @Override
    public void loadBySearchText(String searchText) {
        view.showProgress();
        NetworkService.getInstance().getCourseApi().getCourseListBySearchText(searchText)
                .enqueue(new Callback<ResponseListData<Course>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<Course>> call, Response<ResponseListData<Course>> response) {
                        if (response.isSuccessful()) {
                            List<Course> courseList = response.body().getResults();
                            view.showCourses(courseList, true);
                        }
                        else {
                            Log.d(CourseActivity.TAG, response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<Course>> call, Throwable t) {
                        Log.d(CourseActivity.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }

    @Override
    public void clearList() {
        view.clearCourses();
    }

    @Override
    public void createCourseClick() {
        view.showCreateCourseDialog();
    }
}
