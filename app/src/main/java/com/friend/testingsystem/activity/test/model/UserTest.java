package com.friend.testingsystem.activity.test.model;

import java.util.List;

public class UserTest {
    private List<UserTask> userTaskList;
    private boolean isActive = true;
    private int scoreForTheTest = 0;
    private int maxScoreForTheTest = 0;
    private boolean isWatched = false;

    public UserTest(List<UserTask> userTaskList) {
        this.userTaskList = userTaskList;
    }

    public List<UserTask> getUserTaskList() {
        return userTaskList;
    }

    public void setUserTaskList(List<UserTask> userTaskList) {
        this.userTaskList = userTaskList;
    }

    public boolean isActive() {
        return isActive;
    }

    public void setActive(boolean active) {
        isActive = active;
    }

    public int getScoreForTheTest() {
        return scoreForTheTest;
    }

    public void setScoreForTheTest(int scoreForTheTest) {
        this.scoreForTheTest = scoreForTheTest;
    }

    public int getMaxScoreForTheTest() {
        return maxScoreForTheTest;
    }

    public void setMaxScoreForTheTest(int maxScoreForTheTest) {
        this.maxScoreForTheTest = maxScoreForTheTest;
    }

    public boolean isWatched() {
        return isWatched;
    }

    public void setWatched(boolean watched) {
        isWatched = watched;
    }

    @Override
    public String toString() {
        return "UserTest{" +
                "userTaskList=" + userTaskList +
                ", isActive=" + isActive +
                ", scoreForTheTest=" + scoreForTheTest +
                ", maxScoreForTheTest=" + maxScoreForTheTest +
                ", isWatched=" + isWatched +
                '}';
    }
}
