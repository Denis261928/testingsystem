package com.friend.testingsystem.activity.auth;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.MainActivity;
import com.friend.testingsystem.activity.auth.contact.AuthContact;
import com.friend.testingsystem.activity.auth.dialog.RegistrationSuccessInfoDialog;
import com.friend.testingsystem.activity.auth.presenter.AuthPresenter;
import com.friend.testingsystem.dialog.InfoDialog;
import com.friend.testingsystem.helper.NetworkUtils;
import com.friend.testingsystem.repository.UserDataRepository;

public class RegistrationActivity extends AppCompatActivity implements AuthContact.View {
    public static final String TAG = "RegistrationActivityLog";

    private ProgressDialog progressDialog;
    private EditText emailText;
    private EditText userNameText;
    private EditText passwordText;
    private Button registrationButton;

    private AuthPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        emailText = findViewById(R.id.email_registration_text);
        userNameText = findViewById(R.id.user_name_registration_text);
        passwordText = findViewById(R.id.password_registration_text);
        registrationButton = findViewById(R.id.registration_button);
        TextView goLoginButton = findViewById(R.id.go_to_login);

        if (!NetworkUtils.getNetworkConnection(RegistrationActivity.this)) {
            startActivity(new Intent(RegistrationActivity.this, MainActivity.class));
        }

        UserDataRepository repository = new UserDataRepository(this);
        presenter = new AuthPresenter(this, repository);

        registrationButton.setOnClickListener(view -> presenter.registration(emailText.getText().toString(),
                                userNameText.getText().toString(),
                                passwordText.getText().toString()));

        emailText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                inputDataChanged();
            }
        });

        userNameText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                inputDataChanged();
            }
        });

        passwordText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                inputDataChanged();
            }
        });

        goLoginButton.setOnClickListener(view -> startActivity(new Intent(RegistrationActivity.this, LoginActivity.class)));
    }

    private void inputDataChanged() {
        if (emailText.getText().toString().equals("")
                || userNameText.getText().toString().equals("")
                || passwordText.getText().toString().equals("")) {
            registrationButton.setEnabled(false);
            registrationButton.setBackgroundResource(R.drawable.background_button_disable);
            return;
        }
        registrationButton.setEnabled(true);
        registrationButton.setBackgroundResource(R.drawable.background_button);
    }


    @Override
    public void showErrorDialog(String title, String message) {
        InfoDialog infoDialog = new InfoDialog(this);
        infoDialog.showDialog(title, message);
    }

    @Override
    public void success() {
        RegistrationSuccessInfoDialog dialog = new RegistrationSuccessInfoDialog(this);
        dialog.showDialog("Ви успішно зареєструвались.");
    }

    public void navigateToLoginActivity() {
        startActivity(new Intent(RegistrationActivity.this, LoginActivity.class));
    }

    @Override
    public void showProgress(String message) {
        progressDialog = new ProgressDialog(RegistrationActivity.this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

}
