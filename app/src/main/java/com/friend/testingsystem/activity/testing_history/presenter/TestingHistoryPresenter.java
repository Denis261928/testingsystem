package com.friend.testingsystem.activity.testing_history.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.friend.testingsystem.activity.testing_history.TestingHistoryActivity;
import com.friend.testingsystem.activity.testing_history.contact.TestingHistoryContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.Friend;
import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.model.TestingHistoryItem;
import com.friend.testingsystem.repository.UserDataRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestingHistoryPresenter implements TestingHistoryContact.Presenter {
    private int pageNumber;
    private boolean isLoading = false;
    private boolean isLastPage = false;
    private Friend friend;
    private long userId;
    private boolean isFriend = false;
    private TestingHistoryContact.View view;

    public TestingHistoryPresenter(TestingHistoryContact.View view, UserDataRepository repository) {
        this.view = view;
        this.userId = repository.getUserId();
        this.pageNumber = 1;
    }

    @Override
    public Friend getFriend() {
        return friend;
    }

    @Override
    public void setFriend(Friend friend) {
        this.friend = friend;
        this.userId = friend.getId();
        this.isFriend = true;
    }

    @Override
    public boolean isFriend() {
        return isFriend;
    }

    @Override
    public boolean isLoading() {
        return isLoading;
    }

    @Override
    public void setLoading(boolean loading) {
        this.isLoading = loading;
    }

    @Override
    public boolean isLastPage() {
        return isLastPage;
    }

    @Override
    public void loadInitial() {
        view.showProgress();
        NetworkService.getInstance().getTestApi().getTestingHistoryByUserId(userId, pageNumber)
                .enqueue(new Callback<ResponseListData<TestingHistoryItem>>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(Call<ResponseListData<TestingHistoryItem>> call, Response<ResponseListData<TestingHistoryItem>> response) {
                        if (response.isSuccessful()) {
                            view.showTestingHistory(response.body().getResults());
                            pageNumber++;
                        } else {
                            Log.d(TestingHistoryActivity.TAG, response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @SuppressLint("LongLogTag")
                    @Override
                    public void onFailure(Call<ResponseListData<TestingHistoryItem>> call, Throwable t) {
                        Log.d(TestingHistoryActivity.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }

    @Override
    public void loadMore() {
        NetworkService.getInstance().getTestApi().getTestingHistoryByUserId(userId, pageNumber)
                .enqueue(new Callback<ResponseListData<TestingHistoryItem>>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(Call<ResponseListData<TestingHistoryItem>> call, Response<ResponseListData<TestingHistoryItem>> response) {
                        if (response.isSuccessful()) {
                            if (response.body().getNext() == null) {
                                view.hideLoadingItem();
                                isLastPage = true;
                            } else {
                                view.addAndShowTestingHistory(response.body().getResults());
                            }
                        }
                        else {
                            Log.d(TestingHistoryActivity.TAG, response.errorBody().toString());
                        }
                        pageNumber++;
                    }

                    @SuppressLint("LongLogTag")
                    @Override
                    public void onFailure(Call<ResponseListData<TestingHistoryItem>> call, Throwable t) {
                        Log.d(TestingHistoryActivity.TAG, t.getMessage());
                    }
                });
    }
}
