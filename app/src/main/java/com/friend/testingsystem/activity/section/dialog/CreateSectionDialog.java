package com.friend.testingsystem.activity.section.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.section.dialog.contact.CreateSectionContact;
import com.friend.testingsystem.activity.section.dialog.presenter.CreateSectionPresenter;
import com.friend.testingsystem.activity.section.presenter.SectionPresenter;
import com.friend.testingsystem.dialog.interfaces.MyDialog;
import com.friend.testingsystem.model.Course;
import com.friend.testingsystem.repository.UserDataRepository;

public class CreateSectionDialog implements MyDialog, CreateSectionContact.View {
    public static final String TAG = "CreateSectionDialogLog";

    private Dialog dialog;
    private ProgressDialog progressDialog;
    private EditText sectionNameEditText;
    private Button createSectionButton;

    private Activity parentActivity;
    private CreateSectionPresenter createSectionPresenter;
    private SectionPresenter sectionPresenter;

    public CreateSectionDialog(Activity parentActivity, SectionPresenter sectionPresenter, Course course) {
        this.parentActivity = parentActivity;
        this.sectionPresenter = sectionPresenter;
        UserDataRepository repository = new UserDataRepository(parentActivity);
        createSectionPresenter = new CreateSectionPresenter(this, repository, course);
    }

    @Override
    public void showDialog() {
        dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_create_section);

        sectionNameEditText = dialog.findViewById(R.id.section_name_edit_text);
        createSectionButton = dialog.findViewById(R.id.create_section_button);
        Button cancelButton = dialog.findViewById(R.id.cancel_button);

        sectionNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                if (sectionNameEditText.getText().toString().equals("")) {
                    createSectionButton.setEnabled(false);
                    createSectionButton.setBackgroundResource(R.drawable.background_dialog_button_disable);
                }
                else {
                    createSectionButton.setEnabled(true);
                    createSectionButton.setBackgroundResource(R.drawable.background_dialog_positive_button);
                }
            }
        });

        createSectionButton.setOnClickListener(v -> createSectionPresenter.createSection(sectionNameEditText.getText().toString()));

        cancelButton.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }

    @Override
    public void sectionCreated() {
        dialog.dismiss();
        sectionPresenter.clearList();
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(parentActivity);
        progressDialog.setMessage("Створення розділу");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }
}
