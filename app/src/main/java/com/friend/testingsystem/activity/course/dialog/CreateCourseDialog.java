package com.friend.testingsystem.activity.course.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.course.dialog.contact.CreateCourseContact;
import com.friend.testingsystem.activity.course.dialog.presenter.CreateCoursePresenter;
import com.friend.testingsystem.activity.course.presenter.CoursePresenter;
import com.friend.testingsystem.dialog.interfaces.MyDialog;
import com.friend.testingsystem.repository.UserDataRepository;

public class CreateCourseDialog implements MyDialog, CreateCourseContact.View {
    public static final String TAG = "CreateCourseDialogLog";

    private Dialog dialog;
    private ProgressDialog progressDialog;
    private EditText courseNameEditText;
    private Button createCourseButton;

    private Activity parentActivity;
    private CreateCoursePresenter createCoursePresenter;
    private CoursePresenter coursePresenter;

    public CreateCourseDialog(Activity parentActivity, CoursePresenter coursePresenter) {
        this.parentActivity = parentActivity;
        this.coursePresenter = coursePresenter;
        UserDataRepository repository = new UserDataRepository(parentActivity);
        createCoursePresenter = new CreateCoursePresenter(this, repository);
    }

    @Override
    public void showDialog() {
        dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_create_course);

        courseNameEditText = dialog.findViewById(R.id.course_name_edit_text);
        createCourseButton = dialog.findViewById(R.id.create_course_button);
        Button cancelButton = dialog.findViewById(R.id.cancel_button);

        courseNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                if (courseNameEditText.getText().toString().equals("")) {
                    createCourseButton.setEnabled(false);
                    createCourseButton.setBackgroundResource(R.drawable.background_dialog_button_disable);
                }
                else {
                    createCourseButton.setEnabled(true);
                    createCourseButton.setBackgroundResource(R.drawable.background_dialog_positive_button);
                }
            }
        });

        createCourseButton.setOnClickListener(v -> createCoursePresenter.createCourse(courseNameEditText.getText().toString()));

        cancelButton.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }

    @Override
    public void courseCreated() {
        dialog.dismiss();
        coursePresenter.clearList();
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(parentActivity);
        progressDialog.setMessage("Створення курса");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }
}
