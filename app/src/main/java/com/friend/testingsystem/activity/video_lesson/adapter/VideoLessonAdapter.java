package com.friend.testingsystem.activity.video_lesson.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.model.VideoLesson;

import java.util.List;

public class VideoLessonAdapter extends BaseAdapter {
    private Context context;
    private List<VideoLesson> videoLessonList;

    public VideoLessonAdapter(Context context, List<VideoLesson> videoLessonList) {
        this.context = context;
        this.videoLessonList = videoLessonList;
    }

    public void addListItemsToAdapter(List<VideoLesson> list) {
        this.videoLessonList.addAll(list);
        this.notifyDataSetChanged();
    }

    public List<VideoLesson> getVideoLessonList() {
        return videoLessonList;
    }

    @Override
    public int getCount() {
        return videoLessonList.size();
    }

    @Override
    public Object getItem(int i) {
        return videoLessonList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        View v = View.inflate(context, R.layout.item_video_lesson, null);

        TextView videoNumberText = v.findViewById(R.id.video_number_text);
        TextView videoNameText = v.findViewById(R.id.video_name_text);

        videoNumberText.setText(String.valueOf(i + 1));
        videoNameText.setText(videoLessonList.get(i).getName());

        return v;
    }
}
