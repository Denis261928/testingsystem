package com.friend.testingsystem.activity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.helper.NetworkUtils;
import com.friend.testingsystem.repository.UserDataRepository;

public class MainActivity extends AppCompatActivity {
    public static final String TAG = MainActivity.class.getName();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        UserDataRepository repository = new UserDataRepository(this);
        Log.d(TAG, "User id: "+ repository.getUserId());

        if (NetworkUtils.getNetworkConnection(this)) {
            startActivity(new Intent(MainActivity.this, CategoryActivity.class));
        }

        findViewById(R.id.try_again_button).setOnClickListener(view -> {
            if (NetworkUtils.getNetworkConnection(MainActivity.this)) {
                startActivity(new Intent(MainActivity.this, CategoryActivity.class));
            }
        });
    }
}
