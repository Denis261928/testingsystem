package com.friend.testingsystem.activity.friends.dialog.remove_friend;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.friends.dialog.remove_friend.contact.RemoveFriendContact;
import com.friend.testingsystem.activity.friends.dialog.remove_friend.presenter.RemoveFriendPresenter;
import com.friend.testingsystem.activity.friends.presenter.FriendsPresenter;
import com.friend.testingsystem.dialog.interfaces.MyDialogWithMessage;
import com.friend.testingsystem.model.Friend;
import com.friend.testingsystem.repository.UserDataRepository;

public class RemoveFriendConfirmDialog implements MyDialogWithMessage, RemoveFriendContact.View {
    public static final String TAG = "AddFriendDialogLog";

    private Dialog dialog;
    private ProgressDialog progressDialog;

    private Friend friend;
    private Activity parentActivity;
    private RemoveFriendPresenter removeFriendPresenter;
    private FriendsPresenter friendsPresenter;

    public RemoveFriendConfirmDialog(Activity parentActivity, FriendsPresenter friendsPresenter, Friend friend) {
        this.parentActivity = parentActivity;
        this.friendsPresenter = friendsPresenter;
        this.friend = friend;
        UserDataRepository repository = new UserDataRepository(parentActivity);
        removeFriendPresenter = new RemoveFriendPresenter(this, repository);
    }

    @Override
    public void showDialog(String message) {
        dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_confirm);

        TextView messageText = dialog.findViewById(R.id.dialog_message);
        Button positiveButton = dialog.findViewById(R.id.dialog_positive_button);
        Button negativeButton = dialog.findViewById(R.id.dialog_negative_button);

        messageText.setText(message);

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeFriendPresenter.removeFriend(friend);
            }
        });
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void showDialog(String title, String message) {
        dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_confirm);

        TextView messageText = dialog.findViewById(R.id.dialog_message);
        TextView titleText = dialog.findViewById(R.id.dialog_title);
        Button positiveButton = dialog.findViewById(R.id.dialog_positive_button);
        Button negativeButton = dialog.findViewById(R.id.dialog_negative_button);

        titleText.setText(title);
        messageText.setText(message);

        positiveButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeFriendPresenter.removeFriend(friend);
            }
        });
        negativeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void friendRemoved() {
        dialog.dismiss();
        friendsPresenter.clearList();
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(parentActivity);
        progressDialog.setMessage("Видалення друга");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }
}
