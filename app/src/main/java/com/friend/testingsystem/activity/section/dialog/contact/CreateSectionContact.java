package com.friend.testingsystem.activity.section.dialog.contact;

public interface CreateSectionContact {
    interface View {
        void sectionCreated();
        void showProgress();
        void hideProgress();
    }
    interface Presenter {
        void createSection(String sectionName);
    }
}
