package com.friend.testingsystem.activity.theory.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.model.Theory;
import com.github.chrisbanes.photoview.PhotoView;
import com.squareup.picasso.Picasso;

import java.util.List;

public class TheoryAdapter extends RecyclerView.Adapter<TheoryAdapter.TheoryViewHolder> {
    private List<Theory> theoryList;

    public TheoryAdapter(List<Theory> theoryList) {
        this.theoryList = theoryList;
    }

    @NonNull
    @Override
    public TheoryViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_theory, parent, false);

        return new TheoryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TheoryViewHolder holder, int position) {
        holder.bind(theoryList.get(position));
    }

    @Override
    public int getItemCount() {
        return theoryList.size();
    }

    static class TheoryViewHolder extends RecyclerView.ViewHolder {
        private PhotoView theoryItemImage;
        private TextView theoryItemText;

        public TheoryViewHolder(@NonNull View itemView) {
            super(itemView);

            theoryItemImage = itemView.findViewById(R.id.theory_item_image);
            theoryItemText = itemView.findViewById(R.id.theory_item_text);
        }

        public void bind(Theory theory) {
            if (theory.getImage() != null) {
                theoryItemImage.setVisibility(View.VISIBLE);
                Picasso.get().load(theory.getImage()).into(theoryItemImage);
            }
            if (theory.getText() != null && !theory.getText().equals("")) {
                theoryItemText.setVisibility(View.VISIBLE);
                theoryItemText.setText(theory.getText());
            }
        }
    }
}
