package com.friend.testingsystem.activity.friends.dialog.add_friend.presenter;

import android.util.Log;

import com.friend.testingsystem.activity.friends.dialog.add_friend.AddFriendDialog;
import com.friend.testingsystem.activity.friends.dialog.add_friend.contact.AddFriendContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.User;
import com.friend.testingsystem.repository.UserDataRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AddFriendPresenter implements AddFriendContact.Presenter {

    private AddFriendContact.View view;
    private UserDataRepository repository;

    public AddFriendPresenter(AddFriendContact.View view, UserDataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void addFriend(String userName) {
        view.showProgress();
        User user = new User();
        user.setUserName(userName);
        NetworkService.getInstance().getFriendsApi().addFriend("Token "+repository.getAuthToken(), user)
                .enqueue(new Callback<String>() {
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        if (response.isSuccessful()) {
                            if (response.body().equals("ok"))
                                view.friendAdded();
                            else
                                view.userNotFound();
                        } else {
                            view.userNotFound();
                            Log.d(AddFriendDialog.TAG, response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.d(AddFriendDialog.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }
}
