package com.friend.testingsystem.activity.test.fragment.test_list;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.activity.test.TestActivity;
import com.friend.testingsystem.activity.test.dialog.TestCancelConfirmDialog;
import com.friend.testingsystem.activity.test.fragment.test_list.adapter.TestListAdapter;
import com.friend.testingsystem.activity.test.fragment.test_list.contact.TestListContact;
import com.friend.testingsystem.activity.test.fragment.test_list.presenter.TestListPresenter;
import com.friend.testingsystem.activity.test.dialog.TestFinishConfirmDialog;
import com.friend.testingsystem.activity.testing_history.TestingHistoryActivity;
import com.friend.testingsystem.model.Test;
import com.friend.testingsystem.model.Topic;
import com.friend.testingsystem.repository.UserDataRepository;

import java.util.ArrayList;
import java.util.List;

public class TestListFragment extends Fragment implements TestListAdapter.OnTestListener,
                                                          TestListContact.View,
                                                          TestListContact.ScoreForTheTest {
    public static final String TAG = "TestListFragmentLog";

    private static final String POSITION = "position_key";
    private static final String TOPIC = "topic";
    private static final String TEST_LIST = "test_list";

    private ImageView goBackButton;
    private TextView topicNameText;
    private LinearLayout testScoreContainer;
    private TextView testScoreText;
    private RelativeLayout testPercentageScoreContainer;
    private TextView testPercentageScoreText;
    private RecyclerView recyclerView;
    private Button finishTestButton;

    private boolean isTestFinished = false;
    private int scoreForTheTest = 0;
    private int maxScoreForTheTest = 0;
    private int percentageScoreForTheTest = 0;

    private int numberOfColumn = 4;
    private TestListAdapter testListAdapter;
    private TestListPresenter presenter;

    public static TestListFragment getNewInstance(int position, Topic topic, List<Test> testList) {
        TestListFragment testListFragment = new TestListFragment();
        Bundle args = new Bundle();
        args.putInt(POSITION, position);
        args.putSerializable(TOPIC, topic);
        args.putParcelableArrayList(TEST_LIST, (ArrayList<? extends Parcelable>) testList);
        testListFragment.setArguments(args);

        return testListFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_test_list, container, false);

        goBackButton = view.findViewById(R.id.go_back);
        testScoreContainer = view.findViewById(R.id.test_score_container);
        testScoreText = view.findViewById(R.id.test_score_text);
        testPercentageScoreContainer = view.findViewById(R.id.test_percentage_score_container);
        testPercentageScoreText = view.findViewById(R.id.test_percentage_score_text);
        topicNameText = view.findViewById(R.id.topic_name_text);
        recyclerView = view.findViewById(R.id.test_numbers_list);
        finishTestButton = view.findViewById(R.id.finish_test);

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        UserDataRepository repository = new UserDataRepository(getActivity());
        presenter = new TestListPresenter(this, repository);
        presenter.init((Topic) getArguments().getSerializable(TOPIC), getArguments().<Test>getParcelableArrayList(TEST_LIST));

        if (isTestFinished) {
            showScore(scoreForTheTest, maxScoreForTheTest, percentageScoreForTheTest);
            presenter.setTestFinished();
        }
    }

    @Override
    public void onStart() {
        super.onStart();

        ViewTreeObserver vto = recyclerView.getViewTreeObserver();
        vto.addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                if (Build.VERSION.SDK_INT < Build.VERSION_CODES.JELLY_BEAN) {
                    recyclerView.getViewTreeObserver().removeGlobalOnLayoutListener(this);
                } else {
                    recyclerView.getViewTreeObserver().removeOnGlobalLayoutListener(this);
                }
                int width  = recyclerView.getMeasuredWidth();

                if (width < 750)
                    numberOfColumn = 4;
                else if (width < 1250)
                    numberOfColumn = 5;
                else if (width < 1750)
                    numberOfColumn = 6;
                else if (width < 2250)
                    numberOfColumn = 7;
                else
                    numberOfColumn = 8;

                recyclerView.setLayoutManager(new GridLayoutManager(getActivity(), numberOfColumn));

                Log.d(TAG, "Recycler view width: "+ width);
                Log.d(TAG, "Number of column: "+ numberOfColumn);
            }
        });

        finishTestButton.setOnClickListener(view -> {
            TestFinishConfirmDialog testFinishConfirmDialog =
                    new TestFinishConfirmDialog(getActivity(), presenter);
            testFinishConfirmDialog.showDialog("Ви впервнені що хочите завершити тест?");
        });

        goBackButton.setOnClickListener(view -> presenter.goOut());

    }

    @Override
    public void onResume() {
        super.onResume();

        presenter.updateTestNumbers();
    }

    @Override
    public void onTestClick(int testNumber) {
        TestActivity testActivity = (TestActivity) getActivity();
        testActivity.setCurrentTest(testNumber);
    }

    @Override
    public void showTopic(Topic topic) {
        topicNameText.setText(topic.getName());
    }

    @Override
    public void showTasksNumbers(List<Test> testList) {
        testListAdapter = new TestListAdapter(getActivity(), testList,this);
        recyclerView.setAdapter(testListAdapter);
    }

    @Override
    public void onTestFinished() {
        finishTestButton.setText("Тест завершений");
        finishTestButton.setEnabled(false);
        finishTestButton.setBackgroundResource(R.drawable.background_button_disable);
    }

    @Override
    public void showCancelTestConfirmDialog() {
        TestCancelConfirmDialog testCancelConfirmDialog = new TestCancelConfirmDialog(getActivity());
        testCancelConfirmDialog.showDialog("Ви впервнені що хочите вийти з тесту?\nВаші відповіді при цьому не збережуться");
    }

    @Override
    public void goToCategory() {
        startActivity(new Intent(getActivity(), CategoryActivity.class));
    }

    @Override
    public void goToTestingHistory() {
        startActivity(new Intent(getActivity(), TestingHistoryActivity.class));
    }

    @Override
    public void showScore(int score, int maxScore, int percentage) {
        this.scoreForTheTest = score;
        this.maxScoreForTheTest = maxScore;
        this.percentageScoreForTheTest = percentage;

        testScoreContainer.setVisibility(View.VISIBLE);
        testScoreText.setText(scoreForTheTest +" з "+ maxScoreForTheTest);

        showPercentageScore(percentage);

        isTestFinished = true;
    }

    private void showPercentageScore(int percentage) {
        testPercentageScoreText.setText(percentage +"%");

        if (percentage < 11)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score1);
        else if (percentage < 21)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score2);
        else if (percentage < 31)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score3);
        else if (percentage < 41)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score4);
        else if (percentage < 51)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score5);
        else if (percentage < 61)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score6);
        else if (percentage < 71)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score7);
        else if (percentage < 81)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score8);
        else if (percentage < 91)
            testPercentageScoreContainer.setBackgroundResource(R.drawable.background_percentage_score9);
    }
}
