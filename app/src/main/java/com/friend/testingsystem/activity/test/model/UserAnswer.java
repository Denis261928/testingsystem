package com.friend.testingsystem.activity.test.model;


import com.friend.testingsystem.model.Answer;

public class UserAnswer {
    private Answer answer;
    private boolean isOpen;
    private boolean checked = false;
    private String userAnswerText = "";

    // 3 - користувач клікнув і це правильна відповідь (користувач відповів правильно)
    // 2 - користувач не клікнув але це була првильна відповідь (користувачть відповів не павильно)
    // 1 - користувач клікнув але це не була павильна відповідь (користувач відповів не правильно)
    // 0 - відповідь ще не перевірена
    private int statusCode = 0;

    public UserAnswer(Answer answer, boolean isOpen) {
        this.answer = answer;
        this.isOpen = isOpen;
    }

    public Answer getAnswer() {
        return answer;
    }

    public void setAnswer(Answer answer) {
        this.answer = answer;
    }

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public String getUserAnswerText() {
        return userAnswerText;
    }

    public void setUserAnswerText(String userAnswerText) {
        this.userAnswerText = userAnswerText;
    }

    public int getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(int statusCode) {
        this.statusCode = statusCode;
    }

    public void click() {
        if (checked) {
            this.checked = false;
        }
        else {
            this.checked = true;
        }
    }

    public void makeCorrectAnswer() {
        statusCode = 3;
    }

    public void makeAnswerChecked() {
        statusCode = 2;
    }

    public void makeIncorrectAnswer() {
        statusCode = 1;
    }

    @Override
    public String toString() {
        return "UserAnswer{" +
                "answer=" + answer +
                ", isOpen=" + isOpen +
                ", checked=" + checked +
                ", userAnswerText='" + userAnswerText + '\'' +
                ", statusCode=" + statusCode +
                '}';
    }
}
