package com.friend.testingsystem.activity.profile.dialog.update_password.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.friend.testingsystem.activity.profile.dialog.update_password.UpdatePasswordConfirmDialog;
import com.friend.testingsystem.activity.profile.dialog.update_password.contact.UpdatePasswordContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.Password;
import com.friend.testingsystem.repository.UserDataRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdatePasswordPresenter implements UpdatePasswordContact.Presenter {
    private Password password;

    private UpdatePasswordContact.View view;
    private UserDataRepository repository;

    public UpdatePasswordPresenter(UpdatePasswordContact.View view, UserDataRepository repository, Password password) {
        this.view = view;
        this.repository = repository;
        this.password = password;
    }

    @Override
    public void updatePassword() {
        view.showProgress("Оновлення паролю");
        NetworkService.getInstance().getProfileApi().updatePassword("Token "+repository.getAuthToken(), password)
                .enqueue(new Callback<String>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(Call<String> call, Response<String> response) {
                        view.hideProgress();
                        if (response.isSuccessful()) {
                            if (response.body().equals("yes")) {
                                view.passwordUpdatedSuccessful();
                                Log.d(UpdatePasswordConfirmDialog.TAG, "yes");
                            } else {
                                view.passwordUpdatedFailed();
                                Log.d(UpdatePasswordConfirmDialog.TAG, "no");
                            }
                        } else {
                            view.passwordUpdatedFailed();
                            Log.d(UpdatePasswordConfirmDialog.TAG, response.errorBody().toString());
                        }
                    }

                    @SuppressLint("LongLogTag")
                    @Override
                    public void onFailure(Call<String> call, Throwable t) {
                        Log.d(UpdatePasswordConfirmDialog.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }
}
