package com.friend.testingsystem.activity.profile.dialog.update_password.contact;

public interface UpdatePasswordContact {
    interface View {
        void passwordUpdatedSuccessful();
        void passwordUpdatedFailed();
        void showProgress(String message);
        void hideProgress();
    }

    interface Presenter {
        void updatePassword();
    }
}
