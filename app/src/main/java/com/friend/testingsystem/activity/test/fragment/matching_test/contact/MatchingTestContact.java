package com.friend.testingsystem.activity.test.fragment.matching_test.contact;

import com.friend.testingsystem.activity.test.model.TestWithCheckBoxes;
import com.friend.testingsystem.model.Answer;
import com.friend.testingsystem.model.Task;
import com.friend.testingsystem.model.Test;

import java.util.List;

public interface MatchingTestContact {
    interface View {
        void showGeneralQuestion(String generalQuestion);
        void showQuestions(String testNumberText, List<Task> taskList);
        void showAnswers(List<Answer> answerList);
        void showMatchingTable(List<Task> taskList, List<Answer> answerList);
        void setSaveButtonEnabled(boolean enabled);
        void setSaveButtonExam();
        void onTestSaved();
        void showTestHelpDialog(long testId);
    }

    interface Presenter {
        void init(int currentPosition, Test currentTest, List<Test> testList, boolean isTestExam);
        void setTestWithCheckBoxes(TestWithCheckBoxes testWithCheckBoxes);
        void answerClick(int questionIndex, int answerIndex);
        void setButtonStatus();
        int saveTestAndGetScore();
        boolean isTestActive();
        void showHelp();
    }
}
