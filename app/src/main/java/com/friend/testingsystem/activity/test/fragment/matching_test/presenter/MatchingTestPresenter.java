package com.friend.testingsystem.activity.test.fragment.matching_test.presenter;

import android.util.Log;

import com.friend.testingsystem.activity.test.fragment.matching_test.contact.MatchingTestContact;
import com.friend.testingsystem.activity.test.fragment.usual_test.UsualTestFragment;
import com.friend.testingsystem.activity.test.model.AnswerWithCheckBox;
import com.friend.testingsystem.activity.test.model.TestWithCheckBoxes;
import com.friend.testingsystem.activity.test.model.UserAnswer;
import com.friend.testingsystem.activity.test.model.UserTask;
import com.friend.testingsystem.model.Answer;
import com.friend.testingsystem.model.Task;
import com.friend.testingsystem.model.Test;
import com.friend.testingsystem.repository.TestDataRepository;

import java.util.ArrayList;
import java.util.List;

public class MatchingTestPresenter implements MatchingTestContact.Presenter {
    private Test currentTest;
    private int currentPosition;
    private boolean isTestExam;
    private TestWithCheckBoxes testWithCheckBoxes;

    private MatchingTestContact.View view;

    public MatchingTestPresenter(MatchingTestContact.View view) {
        this.view = view;
    }

    @Override
    public void init(int currentPosition, Test currentTest, List<Test> testList, boolean isTestExam) {
        TestDataRepository.getUserTestList().get(currentPosition).setWatched(true);

        this.currentTest = currentTest;
        this.currentPosition = currentPosition;
        this.isTestExam = isTestExam;

        view.showGeneralQuestion(currentTest.getGeneralQuestion());
        view.showQuestions("Тест "+ (currentPosition+1), currentTest.getTasks());

        List<Answer> answerList = new ArrayList<>();

        if (TestDataRepository.getUserTestList().get(currentPosition).getUserTaskList().size() > 0) {
            for (UserAnswer userAnswer : TestDataRepository
                    .getUserTestList().get(currentPosition)
                    .getUserTaskList().get(0)
                    .getUserAnswerList()) {

                answerList.add(userAnswer.getAnswer());
            }
        }
        view.showAnswers(answerList);
        view.showMatchingTable(currentTest.getTasks(), answerList);
    }

    @Override
    public void setTestWithCheckBoxes(TestWithCheckBoxes testWithCheckBoxes) {
        this.testWithCheckBoxes = testWithCheckBoxes;
    }

    @Override
    public void answerClick(int questionIndex, int answerIndex) {

        if (TestDataRepository.getUserTestList().get(currentPosition)
                .getUserTaskList().get(questionIndex).getUserAnswerList().get(answerIndex).isChecked()) {

            testWithCheckBoxes.getTaskWithCheckBoxesList().get(questionIndex).getAnswerWithCheckBoxList().get(answerIndex).click();
            TestDataRepository.getUserTestList().get(currentPosition)
                    .getUserTaskList().get(questionIndex).getUserAnswerList().get(answerIndex).click();
        }
        else {
            if (TestDataRepository.getUserTestList().get(currentPosition)
                    .getUserTaskList().get(questionIndex).getCountOfCheckedAnswer()
                    <
                    TestDataRepository.getUserTestList().get(currentPosition)
                    .getUserTaskList().get(questionIndex).getCorrectAnswerCount()) {

                testWithCheckBoxes.getTaskWithCheckBoxesList().get(questionIndex).getAnswerWithCheckBoxList().get(answerIndex).click();
                TestDataRepository.getUserTestList().get(currentPosition)
                        .getUserTaskList().get(questionIndex).getUserAnswerList().get(answerIndex).click();
            }
        }
        setButtonStatus();
    }

    @Override
    public void setButtonStatus() {
        if (isTestExam) {
            view.setSaveButtonExam();
        }
        else {
            if (isTestActive()) {
                int minCheckedElementsForEnabled = TestDataRepository.getUserTestList().get(currentPosition).getUserTaskList().size();
                int countOfCheckedElements = 0;
                for (UserTask userTask : TestDataRepository.getUserTestList().get(currentPosition).getUserTaskList()) {
                    for (UserAnswer userAnswer : userTask.getUserAnswerList()) {
                        if (userAnswer.isChecked()) {
                            countOfCheckedElements++;
                        }
                    }
                }
                if (countOfCheckedElements >= minCheckedElementsForEnabled) {
                    view.setSaveButtonEnabled(true);
                } else {
                    view.setSaveButtonEnabled(false);
                }
            } else {
                view.onTestSaved();
            }
        }
    }

    private int getMaxScoreForTheTest() {
        int maxScoreForTheTests = 0;
        for (Task task: currentTest.getTasks()) {
            maxScoreForTheTests += task.getCorrectAnswers().size();
        }
        return maxScoreForTheTests;
    }

    @Override
    public int saveTestAndGetScore() {
        int scoreForTheTest = 0;

        for (int i = 0; i < currentTest.getTasks().size(); i++) {
            Task task = currentTest.getTasks().get(i);

            for (Answer correctAnswer : task.getCorrectAnswers()) {
                for (int j = 0; j < TestDataRepository
                        .getUserTestList().get(currentPosition)
                        .getUserTaskList().get(i)
                        .getUserAnswerList().size(); j++) {

                    UserAnswer userAnswer = TestDataRepository
                            .getUserTestList().get(currentPosition)
                            .getUserTaskList().get(i)
                            .getUserAnswerList().get(j);

                    if (correctAnswer.getId() == userAnswer.getAnswer().getId()) {
                        if (userAnswer.isChecked()) {
                            scoreForTheTest++;

                            for (AnswerWithCheckBox answerWithCheckBox : testWithCheckBoxes.getTaskWithCheckBoxesList().get(i)
                                    .getAnswerWithCheckBoxList()) {
                                if (answerWithCheckBox.getAnswer().getId() == userAnswer.getAnswer().getId()) {
                                    userAnswer.makeCorrectAnswer();
                                    answerWithCheckBox.makeCorrectAnswer();
                                }
                            }

                        } else {

                            for (AnswerWithCheckBox answerWithCheckBox : testWithCheckBoxes.getTaskWithCheckBoxesList().get(i)
                                    .getAnswerWithCheckBoxList()) {
                                if (answerWithCheckBox.getAnswer().getId() == userAnswer.getAnswer().getId()) {
                                    userAnswer.makeAnswerChecked();
                                    answerWithCheckBox.makeAnswerChecked();
                                }
                            }
                        }
                    }
                    else {
                        if (userAnswer.isChecked()) {
                            boolean flag = false;
                            for (Answer correctAnswer1: currentTest.getTasks().get(i).getCorrectAnswers()) {
                                if (userAnswer.getAnswer().getId() == correctAnswer1.getId()) {
                                    flag = true;
                                    break;
                                }
                            }
                            if (!flag) {
                                for (AnswerWithCheckBox answerWithCheckBox : testWithCheckBoxes.getTaskWithCheckBoxesList().get(i)
                                        .getAnswerWithCheckBoxList()) {
                                    if (answerWithCheckBox.getAnswer().getId() == userAnswer.getAnswer().getId()) {
                                        answerWithCheckBox.makeIncorrectAnswer();
                                        userAnswer.makeIncorrectAnswer();
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        TestDataRepository.getUserTestList().get(currentPosition).setActive(false);
        TestDataRepository.getUserTestList().get(currentPosition).setScoreForTheTest(scoreForTheTest);
        TestDataRepository.getUserTestList().get(currentPosition).setMaxScoreForTheTest(getMaxScoreForTheTest());
        view.onTestSaved();

        Log.d(UsualTestFragment.TAG, "Test "+currentPosition
                        +": Max score = " + getMaxScoreForTheTest() +", score = "+scoreForTheTest);

        return scoreForTheTest;
    }

    @Override
    public boolean isTestActive() {
        return TestDataRepository.getUserTestList().get(currentPosition).isActive();
    }

    @Override
    public void showHelp() {
        view.showTestHelpDialog(currentTest.getId());
    }

}
