package com.friend.testingsystem.activity.test.fragment.usual_test.presenter;

import android.util.Log;

import com.friend.testingsystem.activity.test.fragment.usual_test.UsualTestFragment;
import com.friend.testingsystem.activity.test.fragment.usual_test.contact.UsualTestContact;
import com.friend.testingsystem.activity.test.model.AnswerWithCheckBox;
import com.friend.testingsystem.activity.test.model.TestWithCheckBoxes;
import com.friend.testingsystem.activity.test.model.UserAnswer;
import com.friend.testingsystem.model.Answer;
import com.friend.testingsystem.model.Test;
import com.friend.testingsystem.repository.TestDataRepository;

import java.util.ArrayList;
import java.util.List;

public class UsualTestPresenter implements UsualTestContact.Presenter {
    private Test currentTest;
    private int currentPosition;
    private boolean isTestExam;
    private TestWithCheckBoxes testWithCheckBoxes;

    private UsualTestContact.View view;

    public UsualTestPresenter(UsualTestContact.View view) {
        this.view = view;
    }

    @Override
    public void init(int currentPosition, Test currentTest, List<Test> testList, boolean isTestExam) {
        TestDataRepository.getUserTestList().get(currentPosition).setWatched(true);

        this.currentTest = currentTest;
        this.currentPosition = currentPosition;
        this.isTestExam = isTestExam;

        int correctAnswerCount = currentTest.getTasks().get(0).getCorrectAnswers().size();

        String correctAnswerCountText = "";
        if (correctAnswerCount == 1)
            correctAnswerCountText = "Виберіть 1 правильну відповідь";
        else if (correctAnswerCount >= 2 && correctAnswerCount <= 4)
            correctAnswerCountText = "Виберіть "+correctAnswerCount+" правильні відповіді";
        else
            correctAnswerCountText = "Виберіть "+correctAnswerCount+" правильних відповідей";

        view.showGeneralQuestion(currentTest.getGeneralQuestion());
        view.showQuestion("Тест "+ (currentPosition+1),
                currentTest.getTasks().get(0).getQuestion(),
                correctAnswerCountText,
                currentTest.getTasks().get(0).getImg());

        List<Answer> answerList = new ArrayList<>();
        for (UserAnswer userAnswer : TestDataRepository
                        .getUserTestList().get(currentPosition)
                        .getUserTaskList().get(0)
                        .getUserAnswerList()) {
            answerList.add(userAnswer.getAnswer());
        }

        view.showAnswers(answerList);
        view.showAnswersTable(answerList, correctAnswerCount);
    }

    @Override
    public void setTestWithCheckBoxes(TestWithCheckBoxes testWithCheckBoxes) {
        this.testWithCheckBoxes = testWithCheckBoxes;
    }

    @Override
    public void answerClick(int index) {
        if (TestDataRepository.getUserTestList().get(currentPosition)
                .getUserTaskList().get(0).getUserAnswerList().get(index).isChecked()) {

            testWithCheckBoxes.getTaskWithCheckBoxesList().get(0).getAnswerWithCheckBoxList().get(index).click();
            TestDataRepository.getUserTestList().get(currentPosition)
                    .getUserTaskList().get(0).getUserAnswerList().get(index).click();
        }
        else {
            if (TestDataRepository.getUserTestList().get(currentPosition).getUserTaskList().get(0).getCountOfCheckedAnswer()
                    < TestDataRepository.getUserTestList().get(currentPosition).getUserTaskList().get(0).getCorrectAnswerCount()) {

                testWithCheckBoxes.getTaskWithCheckBoxesList().get(0).getAnswerWithCheckBoxList().get(index).click();
                TestDataRepository.getUserTestList().get(currentPosition)
                        .getUserTaskList().get(0).getUserAnswerList().get(index).click();
            }
        }
        setButtonStatus();
    }

    @Override
    public void setButtonStatus() {
        if (isTestExam) {
            view.setSaveButtonExam();
        }
        else {
            if (isTestActive()) {
                boolean enabled = false;
                for (UserAnswer userAnswer : TestDataRepository
                        .getUserTestList().get(currentPosition)
                        .getUserTaskList().get(0)
                        .getUserAnswerList()) {
                    if (userAnswer.isChecked()) {
                        enabled = true;
                    }
                }
                view.setSaveButtonEnabled(enabled);
            } else {
                view.onTestSaved();
            }
        }
    }


    @Override
    public int saveTestAndGetScore() {
        int maxScoreForTheTest = TestDataRepository.getUserTestList().get(currentPosition)
                .getUserTaskList().get(0).getCorrectAnswerCount();
        int scoreForTheTest = 0;

        for (Answer correctAnswer: currentTest.getTasks().get(0).getCorrectAnswers()) {
            for (int i = 0; i < TestDataRepository
                            .getUserTestList().get(currentPosition)
                            .getUserTaskList().get(0)
                            .getUserAnswerList().size(); i++) {

                UserAnswer userAnswer = TestDataRepository
                        .getUserTestList().get(currentPosition)
                        .getUserTaskList().get(0)
                        .getUserAnswerList().get(i);

                if (correctAnswer.getId() == userAnswer.getAnswer().getId()) {
                    if (userAnswer.isChecked()) {
                        scoreForTheTest++;

                        for (AnswerWithCheckBox answerWithCheckBox : testWithCheckBoxes.getTaskWithCheckBoxesList().get(0)
                                .getAnswerWithCheckBoxList()) {
                            if (answerWithCheckBox.getAnswer().getId() == userAnswer.getAnswer().getId()) {
                                userAnswer.makeCorrectAnswer();
                                answerWithCheckBox.makeCorrectAnswer();
                            }
                        }

                    }
                    else {

                        for (AnswerWithCheckBox answerWithCheckBox : testWithCheckBoxes.getTaskWithCheckBoxesList().get(0)
                                .getAnswerWithCheckBoxList()) {
                            if (answerWithCheckBox.getAnswer().getId() == userAnswer.getAnswer().getId()) {
                                userAnswer.makeAnswerChecked();
                                answerWithCheckBox.makeAnswerChecked();
                            }
                        }
                    }
                }
                else {
                    if (userAnswer.isChecked()) {
                        boolean flag = false;
                        for (Answer correctAnswer1: currentTest.getTasks().get(0).getCorrectAnswers()) {
                            if (userAnswer.getAnswer().getId() == correctAnswer1.getId()) {
                                flag = true;
                            }
                        }
                        if (!flag) {
                            for (AnswerWithCheckBox answerWithCheckBox : testWithCheckBoxes.getTaskWithCheckBoxesList().get(0)
                                    .getAnswerWithCheckBoxList()) {
                                if (answerWithCheckBox.getAnswer().getId() == userAnswer.getAnswer().getId()) {
                                    userAnswer.makeIncorrectAnswer();
                                    answerWithCheckBox.makeIncorrectAnswer();
                                }
                            }
                        }
                    }
                }
            }
        }

        TestDataRepository.getUserTestList().get(currentPosition).setActive(false);
        TestDataRepository.getUserTestList().get(currentPosition).setScoreForTheTest(scoreForTheTest);
        TestDataRepository.getUserTestList().get(currentPosition).setMaxScoreForTheTest(maxScoreForTheTest);
        view.onTestSaved();

        Log.d(UsualTestFragment.TAG, "Test "+currentPosition+": Max score = "+ maxScoreForTheTest+", score = "+scoreForTheTest);

        return scoreForTheTest;
    }


    @Override
    public boolean isTestActive() {
        return TestDataRepository.getUserTestList().get(currentPosition).isActive();
    }

    @Override
    public void showHelp() {
        view.showTestHelpDialog(currentTest.getId());
    }
}
