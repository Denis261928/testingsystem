package com.friend.testingsystem.activity.section_element;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.DownloadManager;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.content.FileProvider;

import com.friend.testingsystem.BuildConfig;
import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.CommonActivity;
import com.friend.testingsystem.activity.MainActivity;
import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.activity.create_section_element.CreateSectionElementActivity;
import com.friend.testingsystem.activity.section_element.adapter.SectionElementAdapter;
import com.friend.testingsystem.activity.section_element.contact.SectionElementContact;
import com.friend.testingsystem.activity.section_element.dialog.download_section_element.SectionElementDownloadConfirmDialog;
import com.friend.testingsystem.activity.section_element.dialog.remove_section_element.RemoveSectionElementConfirmDialog;
import com.friend.testingsystem.activity.section_element.presenter.SectionElementPresenter;
import com.friend.testingsystem.dialog.LogoutConfirmDialog;
import com.friend.testingsystem.helper.NetworkUtils;
import com.friend.testingsystem.model.Section;
import com.friend.testingsystem.model.SectionElement;
import com.friend.testingsystem.repository.UserDataRepository;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.io.File;
import java.util.List;

public class SectionElementActivity extends AppCompatActivity implements SectionElementContact.View,
                                                                         CommonActivity,
                                                                         SectionElementAdapter.OnSectionElementListener {
    public static final String TAG = "SectionElementActivityLog";
    public static final String MESSAGE_PROGRESS = "message_progress";
    public static final int DOWNLOAD_REQUEST_CODE = 1;

    private Handler handler;
    private ProgressDialog progressDialog;
    private ImageView loginLogoutButton;
    private ListView sectionElementListView;
    private FloatingActionButton createSectionElementButton;
    private View loadingView;

    private SectionElementAdapter adapter;
    private UserDataRepository repository;
    private SectionElementPresenter presenter;

    private SectionElement currentSectionElement;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_section_element);

        ImageView goBack = findViewById(R.id.go_back);
        loginLogoutButton = findViewById(R.id.login_logout_icon);
        sectionElementListView = findViewById(R.id.section_element_list);
        createSectionElementButton = findViewById(R.id.create_section_element_button);

        if (!NetworkUtils.getNetworkConnection(this)) {
            startActivity(new Intent(this, MainActivity.class));
        }

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.item_loading, null);

        View headerView = inflater.inflate(R.layout.item_section_element_header, null);
        TextView sectionNameText = headerView.findViewById(R.id.section_name_text);
        Section section = (Section) getIntent().getSerializableExtra("section");
        if (section != null) {
            sectionNameText.setText(section.getName());
        }
        sectionElementListView.addHeaderView(headerView);

        handler = new MyHandler();

        sectionElementListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Toast.makeText(SectionElementActivity.this,
                        adapter.getSectionElementList().get(i - 1).getName(), Toast.LENGTH_SHORT).show();
            }
        });

        repository = new UserDataRepository(this);
        setLoginLogoutIcon();
        presenter = new SectionElementPresenter(this, repository, section);
        presenter.loadInitial();

        loginLogoutButton.setOnClickListener(view -> loginLogoutClick());

        goBack.setOnClickListener(view -> onBackPressed());

        createSectionElementButton.setOnClickListener(view -> presenter.createSectionElementClick());

        sectionElementListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) { }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                // Check when scroll to last item in listView, in this tut, init data in list view
                if (absListView.getLastVisiblePosition() == i2-1 && sectionElementListView.getCount() >= 10
                        && !presenter.isLoading() && !presenter.isLastPage()) {
                    presenter.setLoading(true);
                    Thread thread = new ThreadGetMoreData();
                    thread.start();
                }
            }
        });
    }

    @Override
    public void onRemoveClick(SectionElement sectionElement) {
        RemoveSectionElementConfirmDialog dialog = new RemoveSectionElementConfirmDialog(this,
                presenter, sectionElement);
        dialog.showDialog("Ви впевнені що хочете видалити навчальний матеріал " + sectionElement.getName() + "?");
    }

    @Override
    public void onDownloadClick(SectionElement sectionElement) {
        currentSectionElement = sectionElement;
        if(checkPermission())
            startDownload(sectionElement);
        else
            requestPermission();
    }

    private void startDownload(SectionElement sectionElement) {
        SectionElementDownloadConfirmDialog downloadConfirmDialog =
                new SectionElementDownloadConfirmDialog(this, presenter, sectionElement);
        downloadConfirmDialog.showDialog();
    }

    private boolean checkPermission(){
        int result = ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE);
        return result == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission(){
        ActivityCompat.requestPermissions(this,
                new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, DOWNLOAD_REQUEST_CODE);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == DOWNLOAD_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                startDownload(currentSectionElement);
            else
                Toast.makeText(this, "Permission Denied, Please allow to proceed!", Toast.LENGTH_SHORT).show();
        }
    }

    @SuppressLint("HandlerLeak")
    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case 0:
                    sectionElementListView.addFooterView(loadingView);
                    break;
                case 1:
                    adapter.addListItemsToAdapter((List<SectionElement>) msg.obj);
                    sectionElementListView.removeFooterView(loadingView);
                    presenter.setLoading(false);
                    break;
                case 2:
                    sectionElementListView.removeFooterView(loadingView);
                    presenter.setLoading(false);
                    break;
                case 3:
                    adapter.clearSectionElementList();
                    presenter.loadInitial();
                    break;
                default:
                    break;
            }
        }
    }

    public class ThreadGetMoreData extends Thread {
        @Override
        public void run() {
            // Add loading view during search processing
            handler.sendEmptyMessage(0);
            // Search more data
            presenter.loadMore();
        }
    }

    private void openFile() {
        File file = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS),
                "Test24/Pithon Core 1.mp4");
        Intent intent = new Intent(Intent.ACTION_VIEW);
        intent.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
        Uri uri = FileProvider.getUriForFile(SectionElementActivity.this,
                BuildConfig.APPLICATION_ID + ".provider", file);
        intent.setDataAndType(uri, "video/*");
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }

    @Override
    public void setCreateSectionElementButtonVisible(boolean visible) {
        if (visible)
            createSectionElementButton.setVisibility(View.VISIBLE);
        else
            createSectionElementButton.setVisibility(View.GONE);
    }

    @Override
    public void showSectionElements(List<SectionElement> sectionElementList) {
        if (sectionElementList.size() > 0) {
            adapter = new SectionElementAdapter(this, repository, sectionElementList, this);
            sectionElementListView.setAdapter(adapter);
        }
    }

    @Override
    public void addAndShowSectionElements(List<SectionElement> sectionElementList) {
        // Update data adapter and UI
        Message message = handler.obtainMessage(1, sectionElementList);
        handler.sendMessage(message);
    }

    @Override
    public void clearSectionElements() {
        handler.sendEmptyMessage(3);
    }

    @Override
    public void hideLoadingItem() {
        // Remove loading view when there is not any other data
        handler.sendEmptyMessage(2);
    }

    @Override
    public void downloadFile(SectionElement sectionElement) {
        DownloadManager.Request request = new DownloadManager.Request(Uri.parse(sectionElement.getFileUrl()));
        request.setTitle(sectionElement.getName());
        request.setDescription("Файл " + sectionElement.getName() + " завантажується...");

        request.allowScanningByMediaScanner();
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED);

        String fileExtension = sectionElement.getFileUrl().substring(sectionElement.getFileUrl().lastIndexOf("."));
        String fileName = sectionElement.getName() + fileExtension;

        request.setDestinationInExternalPublicDir(Environment.DIRECTORY_DOWNLOADS, "Test24/" + fileName);

        DownloadManager manager = (DownloadManager) getSystemService(Context.DOWNLOAD_SERVICE);
        manager.enqueue(request);
    }

    @Override
    public void showCreateSectionElementDialog(Section section) {
        Intent intent = new Intent(SectionElementActivity.this, CreateSectionElementActivity.class);
        intent.putExtra("section", section);
        startActivity(intent);
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Завантаження розділів");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void loginLogoutClick() {
        if (repository.isUserAuthorized()) {
            LogoutConfirmDialog logoutConfirmDialog = new LogoutConfirmDialog(this);
            logoutConfirmDialog.showDialog("Ви впевнені що хочете вийти з акаунту?");
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    @Override
    public void setLoginLogoutIcon() {
        if (repository.isUserAuthorized())
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_logout));
        else
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_login));
    }

    @Override
    public void logout() {
        repository.logoutUser();
        setLoginLogoutIcon();
        startActivity(new Intent(this, CategoryActivity.class));
    }
}
