package com.friend.testingsystem.activity.profile.presenter;

import android.util.Log;

import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.profile.ProfileActivity;
import com.friend.testingsystem.activity.profile.contact.ProfileContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.BecomeTeacherResponse;
import com.friend.testingsystem.model.Password;
import com.friend.testingsystem.model.UserDetail;
import com.friend.testingsystem.model.UserInfo;
import com.friend.testingsystem.repository.UserDataRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ProfilePresenter implements ProfileContact.Presenter {
    private UserDetail userDetail;

    private ProfileContact.View view;
    private UserDataRepository repository;

    public ProfilePresenter(ProfileContact.View view, UserDataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void init() {
        view.showProgress("Завантаження інформації про користувача");
        NetworkService.getInstance().getAuthApi().getUserInfo("Token "+repository.getAuthToken())
                .enqueue(new Callback<UserInfo>() {
                    @Override
                    public void onResponse(Call<UserInfo> call, Response<UserInfo> response) {
                        if (response.isSuccessful()) {
                            UserInfo userInfo = response.body();
                            repository.setEmail(userInfo.getEmail());
                            getUserDetailApi();
                        } else {
                            Log.d(LoginActivity.TAG, response.errorBody().toString());
                            view.hideProgress();
                        }
                    }

                    @Override
                    public void onFailure(Call<UserInfo> call, Throwable t) {
                        Log.d(LoginActivity.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }

    @Override
    public void getUserDetailApi() {
        NetworkService.getInstance().getProfileApi().getUserDetailById(repository.getUserId())
                .enqueue(new Callback<UserDetail>() {
                    @Override
                    public void onResponse(Call<UserDetail> call, Response<UserDetail> response) {
                        if (response.isSuccessful()) {
                            userDetail = response.body();
                            repository.setUserDetail(userDetail);
                            Log.d("111111", repository.getEmail());
                            view.showUser(repository.getEmail(), userDetail);
                            view.setBecomeTeacherButtonStatus();
                        } else {
                            Log.d(ProfileActivity.TAG, response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @Override
                    public void onFailure(Call<UserDetail> call, Throwable t) {
                        Log.d(ProfileActivity.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }


    @Override
    public void becomeTeacher() {
        view.showProgress("Відправка запиту для статусу вчителя");
        NetworkService.getInstance().getProfileApi().becomeTeacher("Token " + repository.getAuthToken())
                .enqueue(new Callback<BecomeTeacherResponse>() {
                    @Override
                    public void onResponse(Call<BecomeTeacherResponse> call, Response<BecomeTeacherResponse> response) {
                        view.hideProgress();
                        if (response.isSuccessful()) {
                            init();
                            view.becomeTeacherRequestSuccessful();
                        } else {
                            Log.d(ProfileActivity.TAG, response.errorBody().toString());
                        }
                    }

                    @Override
                    public void onFailure(Call<BecomeTeacherResponse> call, Throwable t) {
                        Log.d(ProfileActivity.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }

    @Override
    public UserDetail getUserDetail() {
        return userDetail;
    }

    @Override
    public void updatePassword(String oldPassword, String newPassword) {
        Password password = new Password(oldPassword, newPassword);
        view.confirmPasswordSavingDialog(password);
    }

    @Override
    public void inputFirstNameChanged() {
        if (view.getFirstName().equals(userDetail.getFirstName()))
            view.hideFirstNameKeyBoard();
        else
            view.confirmFirstNameSavingDialog();
    }

    @Override
    public void inputLastNameChanged() {
        if (view.getLastName().equals(userDetail.getLastName()))
            view.hideLastNameKeyBoard();
        else
            view.confirmLastNameSavingDialog();
    }

    private boolean isPhoneNumberFull() {
        return view.getPhoneNumber().length() == 9;
    }

    @Override
    public void inputPhoneNumberChanged() {
        if (view.getPhoneNumber().equals(userDetail.getPhoneNumber())) {
            view.hidePhoneNumberKeyBoard();
        } else {
            if (isPhoneNumberFull()) {
                view.confirmPhoneNumberNameSavingDialog();
            } else {
                view.phoneNumberIsIncompleteDialog();
            }
        }
    }

}
