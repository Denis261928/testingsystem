package com.friend.testingsystem.activity.test.fragment.test_list.contact;

import com.friend.testingsystem.model.Test;
import com.friend.testingsystem.model.Topic;

import java.util.List;

public interface TestListContact {
    interface View {
        void showTopic(Topic topic);
        void showTasksNumbers(List<Test> testList);
        void onTestFinished();
        void showCancelTestConfirmDialog();
        void goToCategory();
        void goToTestingHistory();
    }

    interface Presenter {
        void init(Topic topic, List<Test> testList);
        void updateTestNumbers();
        void setTestFinished();
        void goOut();
    }

    interface ScoreForTheTest {
        void showScore(int score, int maxScore, int percentage);
    }
}
