package com.friend.testingsystem.activity.category.data_source;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.friend.testingsystem.model.Category;

public class CategoryDataSourceFactory extends DataSource.Factory {

    private MutableLiveData<PageKeyedDataSource<Integer, Category>> categoryLiveDataSource = new MutableLiveData<>();

    @Override
    public DataSource create() {
        CategoryDataSource categoryDataSource = new CategoryDataSource();
        categoryLiveDataSource.postValue(categoryDataSource);
        return categoryDataSource;
    }

    public MutableLiveData<PageKeyedDataSource<Integer, Category>> getCategoryLiveDataSource() {
        return categoryLiveDataSource;
    }
}
