package com.friend.testingsystem.activity.test.presenter;

import android.util.Log;

import com.friend.testingsystem.activity.test.TestActivity;
import com.friend.testingsystem.activity.test.model.UserAnswer;
import com.friend.testingsystem.activity.test.model.UserTask;
import com.friend.testingsystem.activity.test.model.UserTest;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.Task;
import com.friend.testingsystem.model.Test;
import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.activity.test.fragment.test_list.TestListFragment;
import com.friend.testingsystem.activity.test.contact.TestContact;
import com.friend.testingsystem.model.TestingHistory;
import com.friend.testingsystem.model.TestingHistoryItem;
import com.friend.testingsystem.repository.TestDataRepository;
import com.friend.testingsystem.repository.UserDataRepository;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TestPresenter implements TestContact.Presenter {
    private long topicId;
    private List<Test> testList;

    private TestContact.View view;
    private UserDataRepository repository;

    public TestPresenter(TestContact.View view, UserDataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void loadTest(long topicId) {
        this.topicId = topicId;
        view.startProgress("Завантаження тестів");
        NetworkService.getInstance().getTestApi().getTestByTopicId(topicId).enqueue(new Callback<ResponseListData<Test>>() {
            @Override
            public void onResponse(Call<ResponseListData<Test>> call, Response<ResponseListData<Test>> response) {
                if (response.isSuccessful()) {
                    testList = response.body().getResults();

                    List<UserTest> userTestList = new ArrayList<>();
                    for (int i = 0; i < testList.size(); i++) {
                        List<UserTask> userTaskList = new ArrayList<>();
                        for (int j = 0; j < testList.get(i).getTasks().size(); j++) {
                            int correctAnswerCount = 0;
                            int answerCount = 0;
                            List<UserAnswer> userAnswerList = new ArrayList<>();
                            for (int k = 0; k < testList.get(i).getTasks().get(j).getCorrectAnswers().size(); k++) {
                                UserAnswer userAnswer = new UserAnswer(testList.get(i).getTasks().get(j).getCorrectAnswers().get(k),
                                                                       testList.get(i).isTestWithOpenAnswers());
                                correctAnswerCount++;
                                answerCount++;
                                userAnswerList.add(userAnswer);
                            }
                            for (int k = 0; k < testList.get(i).getTasks().get(j).getOtherAnswers().size(); k++) {
                                UserAnswer userAnswer = new UserAnswer(testList.get(i).getTasks().get(j).getOtherAnswers().get(k),
                                                                       testList.get(i).isTestWithOpenAnswers());
                                answerCount++;
                                userAnswerList.add(userAnswer);
                            }

                            for (int k = 0; k < userAnswerList.size(); k++) {
                                Collections.shuffle(userAnswerList);
                            }
                            UserTask userTask = new UserTask(userAnswerList, correctAnswerCount);
                            userTaskList.add(userTask);
                        }

                        if (userTaskList.size() > 1) {

                            List<UserAnswer> userAnswerRandomList = new ArrayList<>();
                            for (int k = 0; k < userTaskList.size(); k++) {
                                userAnswerRandomList.addAll(userTaskList.get(k).getUserAnswerList());
                            }
                            Collections.shuffle(userAnswerRandomList);

                            for (int j = 0; j < userTaskList.size(); j++) {

                                List<UserAnswer> userAnswerRandomListForEveryUserTask = new ArrayList<>();
                                for (int k = 0; k < userAnswerRandomList.size(); k++) {
                                    UserAnswer userAnswer = new UserAnswer(userAnswerRandomList.get(k).getAnswer(),
                                                                           userAnswerRandomList.get(k).isOpen());
                                    userAnswerRandomListForEveryUserTask.add(userAnswer);
                                }

                                UserTask userTask = userTaskList.get(j);
                                UserTask newUserTask = new UserTask(userAnswerRandomListForEveryUserTask,
                                            userTask.getCorrectAnswerCount());
                                newUserTask.setQuestionIndex(j);
                                userTaskList.set(j, newUserTask);
                            }
                        }

                        UserTest userTest = new UserTest(userTaskList);
                        userTestList.add(userTest);
                    }
                    TestDataRepository.clearUserTestList();
                    TestDataRepository.setUserTestList(userTestList);

                    view.showTests(testList);
                }
                else {
                    Log.d(TestListFragment.TAG, response.errorBody().toString());
                }

                view.hideProgress();
            }

            @Override
            public void onFailure(Call<ResponseListData<Test>> call, Throwable t) {
                view.hideProgress();
                Log.d(TestListFragment.TAG, t.getMessage());
            }
        });
    }

    @Override
    public void finishTest(List<TestContact.TestFragment> testFragmentList) {
        int sumScoreForTheTests = 0;
        for (int i = 0; i < testFragmentList.size(); i++) {
            if (TestDataRepository.getUserTestList().get(i).isWatched()) {
                sumScoreForTheTests += testFragmentList.get(i).saveTestAndGetScore();
            }
        }
        double percentageOfTests = sumScoreForTheTests * 100 / getMaxScoreForTheTests();

        TestDataRepository.finishTest();

        int sumScoreForTheTests1 = sumScoreForTheTests;
        int maxScoreForTheTests = getMaxScoreForTheTests();
        int percentageScoreForTheTests = (int) Math.round(percentageOfTests);

        view.showScoreForTheTest(sumScoreForTheTests1, maxScoreForTheTests, percentageScoreForTheTests);

        if (repository.isUserAuthorized()) {
            TestingHistory testingHistory = new TestingHistory(topicId,
                    sumScoreForTheTests, maxScoreForTheTests, percentageScoreForTheTests);
            saveTestToTestingHistory(repository.getAuthToken(), testingHistory);
        }
    }

    private void saveTestToTestingHistory(String accessToken, TestingHistory testingHistory) {
        view.startProgress("Сторення запису для історії тестувань...");
        NetworkService.getInstance().getTestApi().createTestingHistory("Token "+ accessToken, testingHistory)
                .enqueue(new Callback<TestingHistoryItem>() {
                    @Override
                    public void onResponse(Call<TestingHistoryItem> call, Response<TestingHistoryItem> response) {
                        if (response.isSuccessful()) {
                            Log.d(TestActivity.TAG, response.body().toString());
                        } else {
                            Log.d(TestActivity.TAG, response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @Override
                    public void onFailure(Call<TestingHistoryItem> call, Throwable t) {
                        view.hideProgress();
                        Log.d(TestActivity.TAG, t.getMessage());
                    }
                });
    }

    @Override
    public int getMaxScoreForTheTests() {
        int maxScoreForTheTests = 0;
        for (int i = 0; i < testList.size(); i++) {
            int maxScoreForTheTest = 0;
            for (Task task: testList.get(i).getTasks()) {
                maxScoreForTheTest += task.getCorrectAnswers().size();
                maxScoreForTheTests += task.getCorrectAnswers().size();
            }
            TestDataRepository.getUserTestList().get(i).setMaxScoreForTheTest(maxScoreForTheTest);
        }
        return maxScoreForTheTests;
    }

    @Override
    public void goOut() {
        if (TestDataRepository.isTestFinished())
            goOutOfTheTestWithoutSaving();
        else
            view.showCancelTestConfirmDialog();
    }

    @Override
    public void goOutOfTheTestWithoutSaving() {
        TestDataRepository.clearUserTestList();
        if (repository.isUserAuthorized())
            view.goToTestingHistory();
        else
            view.goToCategory();
    }


}
