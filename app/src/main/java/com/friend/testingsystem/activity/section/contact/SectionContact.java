package com.friend.testingsystem.activity.section.contact;

import com.friend.testingsystem.model.Course;
import com.friend.testingsystem.model.Section;

import java.util.List;

public interface SectionContact {
    interface View {
        void setCreateSectionButtonVisible(boolean visible);
        void showSections(List<Section> sectionList);
        void addAndShowSections(List<Section> sectionList);
        void clearSections();
        void hideLoadingItem();
        void showCreateSectionDialog(Course course);
        void showProgress();
        void hideProgress();
    }

    interface Presenter {
        boolean isLoading();
        void setLoading(boolean loading);
        boolean isLastPage();
        void loadInitial();
        void loadMore();
        void clearList();
        void createSectionClick();
    }
}
