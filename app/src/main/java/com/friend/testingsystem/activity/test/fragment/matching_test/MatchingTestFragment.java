package com.friend.testingsystem.activity.test.fragment.matching_test;

import android.os.Bundle;
import android.os.Parcelable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.test.TestActivity;
import com.friend.testingsystem.activity.test.dialog.test_help.TestHelpDialog;
import com.friend.testingsystem.activity.test.fragment.matching_test.adapters.MatchingTestAnswerAdapter;
import com.friend.testingsystem.activity.test.fragment.matching_test.adapters.MatchingTestQuestionAdapter;
import com.friend.testingsystem.activity.test.fragment.matching_test.contact.MatchingTestContact;
import com.friend.testingsystem.activity.test.fragment.matching_test.presenter.MatchingTestPresenter;
import com.friend.testingsystem.activity.test.contact.TestContact;
import com.friend.testingsystem.activity.test.model.AnswerWithCheckBox;
import com.friend.testingsystem.activity.test.model.TaskWithCheckBoxes;
import com.friend.testingsystem.activity.test.model.TestWithCheckBoxes;
import com.friend.testingsystem.activity.test.model.UserAnswer;
import com.friend.testingsystem.activity.test.model.UserTask;
import com.friend.testingsystem.model.Answer;
import com.friend.testingsystem.model.Task;
import com.friend.testingsystem.model.Test;
import com.friend.testingsystem.repository.TestDataRepository;

import java.util.ArrayList;
import java.util.List;

public class MatchingTestFragment extends Fragment implements MatchingTestContact.View, TestContact.TestFragment {
    public static final String TAG = "MatchingTestFragmentLog";

    private static final String POSITION = "position_key";
    private static final String TEST = "test_key";
    private static final String TEST_LIST = "test_list_key";
    private static final String IS_EXAM = "is_test_exam_key";

    private ImageView showHelpButton;
    private TextView testNumberText;
    private LinearLayout generalQuestionContainer;
    private TextView generalQuestionText;
    private RecyclerView questionsRecyclerView;
    private RecyclerView answersRecyclerView;
    private TableLayout questionsAnswersTableLayout;
    private Button saveTestButton;

    private int currentPosition;
    private boolean isTestExam;

    private MatchingTestPresenter presenter;

    public static MatchingTestFragment getNewInstance(int position, Test test, List<Test> testList, boolean isTestExam) {
        MatchingTestFragment matchingTestFragment = new MatchingTestFragment();
        Bundle args = new Bundle();
        args.putInt(POSITION, position);
        args.putParcelable(TEST, test);
        args.putParcelableArrayList(TEST_LIST, (ArrayList<? extends Parcelable>) testList);
        args.putBoolean(IS_EXAM, isTestExam);
        matchingTestFragment.setArguments(args);

        return matchingTestFragment;
    }

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_matching_test, container, false);

        showHelpButton = view.findViewById(R.id.show_help_icon);
        testNumberText = view.findViewById(R.id.test_number_text);
        generalQuestionContainer = view.findViewById(R.id.general_question_container);
        generalQuestionText = view.findViewById(R.id.general_question_text);
        questionsRecyclerView = view.findViewById(R.id.questions_list);
        answersRecyclerView = view.findViewById(R.id.answers_list);
        questionsAnswersTableLayout = view.findViewById(R.id.questions_answers_table_layout);
        saveTestButton = view.findViewById(R.id.save_test);

        questionsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        answersRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        return view;
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        presenter = new MatchingTestPresenter(this);
        currentPosition = getArguments().getInt(POSITION);
        isTestExam = getArguments().getBoolean(IS_EXAM);
        presenter.init(currentPosition, (Test) getArguments().getParcelable(TEST),
                getArguments().<Test>getParcelableArrayList(TEST_LIST), isTestExam);

        presenter.setButtonStatus();
        if (TestDataRepository.isTestFinished()) {
            presenter.saveTestAndGetScore();
        }

        for (int i = 0; i < questionsAnswersTableLayout.getChildCount(); i++) {
            TableRow raw = (TableRow) questionsAnswersTableLayout.getChildAt(i);
            final int questionIndex = i;
            for (int j = 0; j < raw.getChildCount(); j++) {
                View checkBoxView = raw.getChildAt(j);
                final int answerIndex = j;
                checkBoxView.setOnClickListener(view1 -> {
                    if (presenter.isTestActive() && questionIndex != 0 && answerIndex != 0) {
                        presenter.answerClick(questionIndex - 1, answerIndex - 1);
                    }
                });
            }
        }

        showHelpButton.setOnClickListener(view12 -> presenter.showHelp());

        saveTestButton.setOnClickListener(view13 -> {
            if (isTestExam) {
                TestActivity activity = (TestActivity) getActivity();
                activity.goNextTest();
            } else {
                if (presenter.isTestActive()) {
                    presenter.saveTestAndGetScore();
                }
                else {
                    TestActivity activity = (TestActivity) getActivity();
                    activity.goNextTest();
                }
            }
        });
    }

    @Override
    public void showGeneralQuestion(String generalQuestion) {
        if (generalQuestion.equals("") || generalQuestion == null)
            generalQuestionContainer.setVisibility(View.GONE);
        else
            generalQuestionText.setText(generalQuestion);
    }

    @Override
    public void showQuestions(String testNumberText, List<Task> taskList) {
        this.testNumberText.setText(testNumberText);
        MatchingTestQuestionAdapter adapter = new MatchingTestQuestionAdapter(getActivity(), taskList);
        questionsRecyclerView.setAdapter(adapter);
    }

    @Override
    public void showAnswers(List<Answer> answerList) {
        MatchingTestAnswerAdapter adapter = new MatchingTestAnswerAdapter(getActivity(), answerList);
        answersRecyclerView.setAdapter(adapter);
    }

    @Override
    public void showMatchingTable(List<Task> taskList, List<Answer> answerList) {
        questionsAnswersTableLayout.setStretchAllColumns(true);
        List<TaskWithCheckBoxes> taskWithCheckBoxesList = new ArrayList<>();

        TableRow headRow = new TableRow(getActivity());
        RelativeLayout startEmptyItemView = (RelativeLayout) LayoutInflater.from(getActivity())
                .inflate(R.layout.item_check_empty_place, null,false);
        headRow.addView(startEmptyItemView);

        for (int j = 0; j < answerList.size(); j++) {
            RelativeLayout headItemView = (RelativeLayout) LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_check_answer_number, null,false);
            TextView taskNumberText = headItemView.findViewById(R.id.answer_number);
            taskNumberText.setText(TestActivity.alphabetIndexes[j]);
            headRow.addView(headItemView);
        }
        questionsAnswersTableLayout.addView(headRow);

        for (int i = 0; i < taskList.size(); i++) {

            TableRow tableRow = new TableRow(getActivity());

            RelativeLayout startItemView = (RelativeLayout) LayoutInflater.from(getActivity())
                    .inflate(R.layout.item_check_task_number, null,false);
            TextView taskNumberText = startItemView.findViewById(R.id.task_number);
            taskNumberText.setText(String.valueOf(i + 1));
            tableRow.addView(startItemView);

            TaskWithCheckBoxes taskWithCheckBoxes = new TaskWithCheckBoxes(taskList.get(i).getCorrectAnswers().size());
            for (int j = 0; j < answerList.size(); j++) {
                RelativeLayout itemView = (RelativeLayout) LayoutInflater.from(getActivity())
                        .inflate(R.layout.item_check_test, null,false);
                RelativeLayout iconCheckBoxAnswerContainer = itemView.findViewById(R.id.icon_check_answer_container);
                ImageView iconCheckBoxAnswer = itemView.findViewById(R.id.icon_check_answer);
                tableRow.addView(itemView);

                AnswerWithCheckBox answerWithCheckBox =
                        new AnswerWithCheckBox(iconCheckBoxAnswerContainer, iconCheckBoxAnswer, answerList.get(j));

                for (int k = 0; k < TestDataRepository.getUserTestList().get(currentPosition)
                        .getUserTaskList().get(i)
                        .getUserAnswerList().size(); k++) {

                    Log.d("11111111", i+" - "+k);

                    UserAnswer userAnswer = TestDataRepository.getUserTestList().get(currentPosition)
                            .getUserTaskList().get(i)
                            .getUserAnswerList().get(k);

                    UserTask userTask = TestDataRepository.getUserTestList().get(currentPosition)
                            .getUserTaskList().get(i);

                    if (userAnswer.getAnswer().getId() == answerList.get(j).getId()) {
                        if (userAnswer.getStatusCode() == 0) {
                            if (userAnswer.isChecked()) {
                                answerWithCheckBox.makeAnswerChecked();
                                answerWithCheckBox.setChecked(true);
                            }
                        }
                        else {
                            if (userAnswer.getStatusCode() == 3) {
                                answerWithCheckBox.makeCorrectAnswer();
                                answerWithCheckBox.setChecked(true);
                            }
                            else if (userAnswer.getStatusCode() == 2) {
                                answerWithCheckBox.makeAnswerChecked();
                            }
                            else if (userAnswer.getStatusCode() == 1) {
                                answerWithCheckBox.makeIncorrectAnswer();
                                answerWithCheckBox.setChecked(true);
                            }
                        }
                        presenter.setButtonStatus();
                    }
                }

                taskWithCheckBoxes.addAnswerWithCheckBox(answerWithCheckBox);
            }
            questionsAnswersTableLayout.addView(tableRow);
            taskWithCheckBoxesList.add(taskWithCheckBoxes);
        }
        TestWithCheckBoxes testWithCheckBoxes = new TestWithCheckBoxes(taskWithCheckBoxesList);
        presenter.setTestWithCheckBoxes(testWithCheckBoxes);
    }

    @Override
    public void setSaveButtonEnabled(boolean enabled) {
        if (saveTestButton.isEnabled() && !enabled) {
            saveTestButton.setEnabled(false);
            saveTestButton.setBackgroundResource(R.drawable.background_button_disable);
        }
        else if (!saveTestButton.isEnabled() && enabled){
            saveTestButton.setEnabled(true);
            saveTestButton.setBackgroundResource(R.drawable.background_button);
        }
    }

    @Override
    public void setSaveButtonExam() {
        saveTestButton.setText("Наступне");
        saveTestButton.setBackgroundResource(R.drawable.background_button);
        saveTestButton.setEnabled(true);
        if (TestDataRepository.isTestFinished())
            showHelpButton.setVisibility(View.VISIBLE);
        else
            showHelpButton.setVisibility(View.GONE);
    }

    @Override
    public void onTestSaved() {
        saveTestButton.setEnabled(true);
        saveTestButton.setBackgroundResource(R.drawable.background_button);
        saveTestButton.setText("Наступне");
        if (showHelpButton.getVisibility() == View.GONE)
            showHelpButton.setVisibility(View.VISIBLE);
    }

    @Override
    public void showTestHelpDialog(long testId) {
        TestHelpDialog testHelpDialog = new TestHelpDialog(getActivity(), testId);
        testHelpDialog.showDialog();
    }

    @Override
    public int saveTestAndGetScore() {
        return presenter.saveTestAndGetScore();
    }
}
