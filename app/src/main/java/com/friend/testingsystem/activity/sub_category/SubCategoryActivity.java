package com.friend.testingsystem.activity.sub_category;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.CommonActivity;
import com.friend.testingsystem.activity.MainActivity;
import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.sub_category.adapter.SubCategoryAdapter;
import com.friend.testingsystem.activity.sub_category.viewmodel.SubCategoryViewModel;
import com.friend.testingsystem.activity.topic.TopicActivity;
import com.friend.testingsystem.activity.video_lesson.VideoLessonActivity;
import com.friend.testingsystem.dialog.LogoutConfirmDialog;
import com.friend.testingsystem.helper.NetworkUtils;
import com.friend.testingsystem.model.Category;
import com.friend.testingsystem.model.SubCategory;
import com.friend.testingsystem.repository.UserDataRepository;

import java.io.Serializable;

public class SubCategoryActivity extends AppCompatActivity implements CommonActivity, SubCategoryAdapter.OnSubCategoryListener {
    public static final String TAG = "SubCategoryActivityLog";

    private ImageView loginLogoutButton;

    private UserDataRepository repository;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sub_category);

        ImageView goBack = findViewById(R.id.go_back);
        loginLogoutButton = findViewById(R.id.login_logout_icon);
        TextView categoryNameText = findViewById(R.id.category_name_text);
        RecyclerView recyclerView = findViewById(R.id.sub_categories_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        if (!NetworkUtils.getNetworkConnection(this)) {
            startActivity(new Intent(this, MainActivity.class));
        }

        repository = new UserDataRepository(this);
        setLoginLogoutIcon();

        final Category category = (Category) getIntent().getSerializableExtra("category");
        if (category != null) {
            categoryNameText.setText(category.getName()+":");
        }

        loginLogoutButton.setOnClickListener(view -> loginLogoutClick());

        goBack.setOnClickListener(view -> onBackPressed());

        SubCategoryViewModel subCategoryViewModel = ViewModelProviders.of(this, new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new SubCategoryViewModel(category.getId());
            }
        }).get(SubCategoryViewModel.class);

        final SubCategoryAdapter adapter = new SubCategoryAdapter(this, this);

        subCategoryViewModel.subCategoryPageList.observe(this, new Observer<PagedList<SubCategory>>() {
            @Override
            public void onChanged(PagedList<SubCategory> subCategories) {
                adapter.submitList(subCategories);
            }
        });
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void loginLogoutClick() {
        if (repository.isUserAuthorized()) {
            LogoutConfirmDialog logoutConfirmDialog = new LogoutConfirmDialog(this);
            logoutConfirmDialog.showDialog("Ви впевнені що хочете вийти з акаунту?");
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    @Override
    public void setLoginLogoutIcon() {
        if (repository.isUserAuthorized())
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_logout));
        else
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_login));
    }

    @Override
    public void logout() {
        repository.logoutUser();
        setLoginLogoutIcon();
    }

    @Override
    public void onSubCategoryClick(SubCategory subCategory) {
        Intent intent = new Intent(SubCategoryActivity.this, TopicActivity.class);
        intent.putExtra("sub_category", (Serializable) subCategory);
        startActivity(intent);
    }

    @Override
    public void onVideoClick(SubCategory subCategory) {
        Intent intent = new Intent(SubCategoryActivity.this, VideoLessonActivity.class);
        intent.putExtra("sub_category", (Serializable) subCategory);
        startActivity(intent);
    }
}
