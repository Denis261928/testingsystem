package com.friend.testingsystem.activity.topic.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.model.Topic;

public class TopicAdapter extends PagedListAdapter<Topic, TopicAdapter.TopicViewHolder> {
    private Context context;
    private OnTopicListener onTopicListener;

    public TopicAdapter(Context context, OnTopicListener onTopicListener) {
        super(DIFF_CALLBACK);
        this.context = context;
        this.onTopicListener = onTopicListener;
    }

    @NonNull
    @Override
    public TopicViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.item_topic, parent, false);

        return new TopicViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull TopicViewHolder holder, int position) {
        Topic topic = getItem(position);
        if (topic != null) {
            holder.setItem(topic, onTopicListener);
        } else {
            Log.d(CategoryActivity.TAG, "Category "+ position +" is null");
        }
    }

    private static DiffUtil.ItemCallback<Topic> DIFF_CALLBACK = new DiffUtil.ItemCallback<Topic>() {
        @Override
        public boolean areItemsTheSame(@NonNull Topic oldItem, @NonNull Topic newItem) {
             return oldItem.getId() == newItem.getId();
        }

        @Override
        public boolean areContentsTheSame(@NonNull Topic oldItem, @NonNull Topic newItem) {
            return oldItem.equals(newItem);
        }
    };

    class TopicViewHolder extends RecyclerView.ViewHolder {
        private OnTopicListener listener;
        private FrameLayout goTheory;
        private TextView topicName;

        private Topic topic;

        public TopicViewHolder(@NonNull View itemView) {
            super(itemView);

            goTheory = itemView.findViewById(R.id.go_to_theory);
            topicName = itemView.findViewById(R.id.topic_name);

            itemView.setOnClickListener(view -> listener.onTopicClick(topic));

            goTheory.setOnClickListener(view -> listener.onTheoryClick(topic));
        }

        public void setItem(final Topic topic, final OnTopicListener listener) {
            this.topic = topic;
            this.listener = listener;

            topicName.setText(topic.getName());

            if (topic.isExam())
                goTheory.setVisibility(View.GONE);
        }

    }

    public interface OnTopicListener {
        void onTopicClick(Topic topic);
        void onTheoryClick(Topic topic);
    }
}
