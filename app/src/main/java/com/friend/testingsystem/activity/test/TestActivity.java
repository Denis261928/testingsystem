package com.friend.testingsystem.activity.test;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.viewpager.widget.ViewPager;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.MainActivity;
import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.activity.test.adapter.TestPagerAdapter;
import com.friend.testingsystem.activity.test.contact.TestContact;
import com.friend.testingsystem.activity.test.dialog.TestCancelConfirmDialog;
import com.friend.testingsystem.activity.test.fragment.matching_test.MatchingTestFragment;
import com.friend.testingsystem.activity.test.fragment.open_test.OpenTestFragment;
import com.friend.testingsystem.activity.test.fragment.test_list.TestListFragment;
import com.friend.testingsystem.activity.test.fragment.test_list.contact.TestListContact;
import com.friend.testingsystem.activity.test.fragment.usual_test.UsualTestFragment;
import com.friend.testingsystem.activity.test.presenter.TestPresenter;
import com.friend.testingsystem.activity.testing_history.TestingHistoryActivity;
import com.friend.testingsystem.helper.NetworkUtils;
import com.friend.testingsystem.model.Test;
import com.friend.testingsystem.model.Topic;
import com.friend.testingsystem.repository.UserDataRepository;

import java.util.ArrayList;
import java.util.List;

public class TestActivity extends AppCompatActivity implements TestContact.View {
    public static final String TAG = "TestActivityLog";
    public static final String[] alphabetIndexes = {
            "A","B","C","D","E","F","G","H","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z"
    };

    private TestPagerAdapter testPagerAdapter;
    private ViewPager viewPager;
    private ProgressDialog progressDialog;
    private List<Fragment> pages = new ArrayList<>();

    private Topic topic;
    private List<TestContact.TestFragment> testFragmentList;
    private TestListContact.ScoreForTheTest scoreForTheTest;

    private TestPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_test);

        viewPager = findViewById(R.id.test_pager);

        if (!NetworkUtils.getNetworkConnection(this)) {
            startActivity(new Intent(this, MainActivity.class));
        }

        topic = (Topic) getIntent().getSerializableExtra("topic");

        UserDataRepository repository = new UserDataRepository(this);
        presenter = new TestPresenter(this, repository);
        if (topic != null) {
            presenter.loadTest(topic.getId());
        }

        findViewById(R.id.left_direction_tab).setOnClickListener(view -> {
            int currentPosition = viewPager.getCurrentItem();
            viewPager.setCurrentItem(currentPosition - 1);
        });

        findViewById(R.id.menu_tests_tab).setOnClickListener(view -> viewPager.setCurrentItem(0));

        findViewById(R.id.right_direction_tab).setOnClickListener(view -> {
            int currentPosition = viewPager.getCurrentItem();
            viewPager.setCurrentItem(currentPosition + 1);
        });
    }

    @Override
    public void onBackPressed() {
        presenter.goOut();
    }

    public void setCurrentTest(int position) {
        viewPager.setCurrentItem(position);
    }

    public void finishTest() {
        presenter.finishTest(testFragmentList);
    }

    public void finishTestWithoutSaving() {
        presenter.goOutOfTheTestWithoutSaving();
    }

    public void goNextTest() {
        int currentPosition = viewPager.getCurrentItem();
        viewPager.setCurrentItem(currentPosition + 1);
    }

    @Override
    public void showTests(List<Test> testList) {
        testFragmentList = new ArrayList<>();

        TestListFragment testListFragment = TestListFragment.getNewInstance(0, topic, testList);
        pages.add(testListFragment);
        scoreForTheTest = testListFragment;
        for (int i = 0; i < testList.size(); i++) {
            if (!testList.get(i).isTestWithOpenAnswers()) {
                if (testList.get(i).getTasks().size() == 1) {
                    UsualTestFragment usualTestFragment
                            = UsualTestFragment.getNewInstance(i, testList.get(i), testList, topic.isExam());
                    pages.add(usualTestFragment);
                    testFragmentList.add(usualTestFragment);
                } else {
                    MatchingTestFragment matchingTestFragment
                            = MatchingTestFragment.getNewInstance(i, testList.get(i), testList, topic.isExam());
                    pages.add(matchingTestFragment);
                    testFragmentList.add(matchingTestFragment); }
            }
            else {
                OpenTestFragment openTestFragment
                        = OpenTestFragment.getNewInstance(i, testList.get(i), testList, topic.isExam());
                pages.add(openTestFragment);
                testFragmentList.add(openTestFragment);
            }
        }

        testPagerAdapter = new TestPagerAdapter(getSupportFragmentManager());
        testPagerAdapter.setPages(pages);
        viewPager.setAdapter(testPagerAdapter);
    }

    public UsualTestFragment getCurrentItem() {
        return (UsualTestFragment) testPagerAdapter.getItem(viewPager.getCurrentItem());
    }

    @Override
    public void showScoreForTheTest(int score, int maxScore, int percentage) {
        scoreForTheTest.showScore(score, maxScore, percentage);
    }

    @Override
    public void startProgress(String message) {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void showCancelTestConfirmDialog() {
        TestCancelConfirmDialog testCancelConfirmDialog = new TestCancelConfirmDialog(this);
        testCancelConfirmDialog.showDialog("Ви впервнені що хочите вийти з тесту?\nВаші відповіді при цьому не збережуться");
    }

    @Override
    public void goToCategory() {
        startActivity(new Intent(this, CategoryActivity.class));
    }

    @Override
    public void goToTestingHistory() {
        startActivity(new Intent(this, TestingHistoryActivity.class));
    }
}
