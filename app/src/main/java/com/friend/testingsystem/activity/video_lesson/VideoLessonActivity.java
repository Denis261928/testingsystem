package com.friend.testingsystem.activity.video_lesson;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.CommonActivity;
import com.friend.testingsystem.activity.MainActivity;
import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.activity.course.CourseActivity;
import com.friend.testingsystem.activity.friends.FriendsActivity;
import com.friend.testingsystem.activity.profile.ProfileActivity;
import com.friend.testingsystem.activity.testing_history.TestingHistoryActivity;
import com.friend.testingsystem.activity.video_lesson.adapter.VideoLessonAdapter;
import com.friend.testingsystem.activity.video_lesson.dialog.AuthConfirmDialogWithMessageInVideoLessons;
import com.friend.testingsystem.activity.video_lesson.contact.VideoLessonContact;
import com.friend.testingsystem.activity.video_lesson.presenter.VideoLessonPresenter;
import com.friend.testingsystem.dialog.LogoutConfirmDialog;
import com.friend.testingsystem.helper.ActivityEnum;
import com.friend.testingsystem.helper.NetworkUtils;
import com.friend.testingsystem.helper.YoutubeConfig;
import com.friend.testingsystem.model.SubCategory;
import com.friend.testingsystem.model.VideoLesson;
import com.friend.testingsystem.repository.UserDataRepository;
import com.google.android.youtube.player.YouTubeBaseActivity;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayerView;

import java.util.List;

public class VideoLessonActivity extends YouTubeBaseActivity implements YouTubePlayer.OnInitializedListener,
                                                                        VideoLessonContact.View,
                                                                        CommonActivity {
    public static final String TAG = "VideoLessonActivityLog";

    private Handler handler;
    private ProgressDialog progressDialog;
    private ImageView loginLogoutButton;
    private LinearLayout videoLessonContent;
    private RelativeLayout videoLessonContentEmpty;
    private TextView videoLessonsEmptyMessageText;
    private YouTubePlayerView youTubePlayerView;
    private ListView videoLessonListView;
    private View loadingView;

    private YouTubePlayer mYouTubePlayer;

    private VideoLessonAdapter adapter;
    private UserDataRepository repository;
    private VideoLessonPresenter presenter;

    private SubCategory subCategory;
    private String videoId = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_lesson);

        ImageView goBack = findViewById(R.id.go_back);
        loginLogoutButton = findViewById(R.id.login_logout_icon);
        videoLessonContent = findViewById(R.id.video_lesson_content);
        videoLessonContentEmpty = findViewById(R.id.video_lesson_content_empty);
        videoLessonsEmptyMessageText = findViewById(R.id.video_lessons_empty_message_text);
        TextView subCategoryNameText = findViewById(R.id.sub_category_name_text);
        youTubePlayerView = findViewById(R.id.youtube_player);
        videoLessonListView = findViewById(R.id.video_lesson_list);

        if (!NetworkUtils.getNetworkConnection(this)) {
            startActivity(new Intent(this, MainActivity.class));
        }

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.item_loading, null);

        View headerView = inflater.inflate(R.layout.item_video_lesson_header, null);
        subCategoryNameText = headerView.findViewById(R.id.sub_category_name_text);
        subCategory = (SubCategory) getIntent().getSerializableExtra("sub_category");
        if (subCategory != null) {
            subCategoryNameText.setText(subCategory.getName());
        }
        videoLessonListView.addHeaderView(headerView);

        handler = new MyHandler();

        videoLessonListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                VideoLesson videoLesson = adapter.getVideoLessonList().get(i - 1);
                videoId = videoLesson.getVideoId();
                mYouTubePlayer.cueVideo(videoId);
            }
        });

        repository = new UserDataRepository(this);
        setLoginLogoutIcon();
        presenter = new VideoLessonPresenter(this, repository, subCategory);
        presenter.loadInitial();

        loginLogoutButton.setOnClickListener(view -> loginLogoutClick());

        goBack.setOnClickListener(view -> onBackPressed());

        videoLessonListView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                // Check when scroll to last item in listView, in this tut, init data in list view
                if (absListView.getLastVisiblePosition() == i2-1 && videoLessonListView.getCount() >= 10
                        && !presenter.isLoading() && !presenter.isLastPage()) {
                    presenter.setLoading(true);
                    Thread thread = new ThreadGetMoreData();
                    thread.start();
                }
            }
        });

        findViewById(R.id.history_tab).setOnClickListener(view -> presenter.navigateToTestingHistory());

        findViewById(R.id.profile_tab).setOnClickListener(view -> presenter.navigateToProfile());

        findViewById(R.id.home_tab).setOnClickListener(view -> startActivity(new Intent(VideoLessonActivity.this, CategoryActivity.class)));

        findViewById(R.id.friends_tab).setOnClickListener(view -> presenter.navigateToFriends());

        findViewById(R.id.courses_tab).setOnClickListener(view -> presenter.navigateToCourse());
    }

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case 0:
                    videoLessonListView.addFooterView(loadingView);
                    break;
                case 1:
                    adapter.addListItemsToAdapter((List<VideoLesson>) msg.obj);
                    videoLessonListView.removeFooterView(loadingView);
                    presenter.setLoading(false);
                    break;
                case 2:
                    videoLessonListView.removeFooterView(loadingView);
                    presenter.setLoading(false);
                    break;
                default:
                    break;
            }
        }
    }

    public class ThreadGetMoreData extends Thread {
        @Override
        public void run() {
            // Add loading view during search processing
            handler.sendEmptyMessage(0);
            // Search more data
            presenter.loadMore();
        }
    }

    @Override
    public void onInitializationSuccess(YouTubePlayer.Provider provider, YouTubePlayer youTubePlayer, boolean b) {
        if (!b && videoId != null && !videoId.equals("")) {
            youTubePlayer.cueVideo(videoId);
            mYouTubePlayer = youTubePlayer;
        }
    }

    @Override
    public void onInitializationFailure(YouTubePlayer.Provider provider, YouTubeInitializationResult youTubeInitializationResult) {
        Log.d(VideoLessonActivity.TAG, "Failed to initialize youtube player");
    }

    @Override
    public void showVideoLessons(List<VideoLesson> videoLessonList) {
        if (videoLessonList.size() > 0) {
            videoLessonContent.setVisibility(View.VISIBLE);
            videoLessonContentEmpty.setVisibility(View.GONE);
            adapter = new VideoLessonAdapter(getApplicationContext(), videoLessonList);
            videoLessonListView.setAdapter(adapter);
            videoId = videoLessonList.get(0).getVideoId();
            youTubePlayerView.initialize(YoutubeConfig.getApiKey(), this);
        }
        else {
            videoLessonContent.setVisibility(View.GONE);
            videoLessonContentEmpty.setVisibility(View.VISIBLE);
            videoLessonsEmptyMessageText.setText("У розділі \"" + subCategory.getName() + "\" не має жодного відео");
        }
    }

    @Override
    public void addAndShowVideoLessons(List<VideoLesson> videoLessonList) {
        // Update data adapter and UI
        Message message = handler.obtainMessage(1, videoLessonList);
        handler.sendMessage(message);
    }

    @Override
    public void hideLoadingItem() {
        // Remove loading view when there is not any other data
        handler.sendEmptyMessage(2);
    }

    @Override
    public void showAuthDialog(String message) {
        AuthConfirmDialogWithMessageInVideoLessons authConfirmDialog =
                new AuthConfirmDialogWithMessageInVideoLessons(this, presenter);
        authConfirmDialog.showDialog(message);
    }

    @Override
    public void navigateTo(ActivityEnum activity) {
        switch (activity) {
            case LOGIN:
                startActivity(new Intent(this, LoginActivity.class));
                break;
            case TESTING_HISTORY:
                startActivity(new Intent(this, TestingHistoryActivity.class));
                break;
            case FRIENDS:
                startActivity(new Intent(this, FriendsActivity.class));
                break;
            case COURSE:
                startActivity(new Intent(this, CourseActivity.class));
                break;
            case PROFILE:
                startActivity(new Intent(this, ProfileActivity.class));
                break;
        }
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Завантаження відео");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void loginLogoutClick() {
        if (repository.isUserAuthorized()) {
            LogoutConfirmDialog logoutConfirmDialog = new LogoutConfirmDialog(this);
            logoutConfirmDialog.showDialog("Ви впевнені що хочете вийти з акаунту?");
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    @Override
    public void setLoginLogoutIcon() {
        if (repository.isUserAuthorized())
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_logout));
        else
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_login));
    }

    @Override
    public void logout() {
        repository.logoutUser();
        setLoginLogoutIcon();
    }

}
