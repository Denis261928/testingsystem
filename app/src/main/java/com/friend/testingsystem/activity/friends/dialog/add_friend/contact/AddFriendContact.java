package com.friend.testingsystem.activity.friends.dialog.add_friend.contact;

public interface AddFriendContact {
    interface View {
        void friendAdded();
        void userNotFound();
        void showProgress();
        void hideProgress();
    }
    interface Presenter {
        void addFriend(String userName);
    }
}
