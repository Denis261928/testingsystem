package com.friend.testingsystem.activity.section_element.contact;

import com.friend.testingsystem.model.Section;
import com.friend.testingsystem.model.SectionElement;

import java.util.List;

public interface SectionElementContact {
    interface View {
        void setCreateSectionElementButtonVisible(boolean visible);
        void showSectionElements(List<SectionElement> sectionElementList);
        void addAndShowSectionElements(List<SectionElement> sectionElementList);
        void clearSectionElements();
        void hideLoadingItem();
        void downloadFile(SectionElement sectionElement);
        void showCreateSectionElementDialog(Section section);
        void showProgress();
        void hideProgress();
    }

    interface Presenter {
        boolean isLoading();
        void setLoading(boolean loading);
        boolean isLastPage();
        void loadInitial();
        void loadMore();
        void clearList();
        void downloadFile(SectionElement sectionElement);
        void createSectionElementClick();
    }
}
