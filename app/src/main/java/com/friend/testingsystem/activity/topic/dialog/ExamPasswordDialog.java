package com.friend.testingsystem.activity.topic.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.topic.TopicActivity;
import com.friend.testingsystem.dialog.interfaces.MyDialog;
import com.friend.testingsystem.model.Topic;

public class ExamPasswordDialog implements MyDialog {
    private Activity parentActivity;
    private Topic topic;

    public ExamPasswordDialog(Activity parentActivity, Topic topic) {
        this.parentActivity = parentActivity;
        this.topic = topic;
    }

    @Override
    public void showDialog() {
        final Dialog dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_exam_password);

        final EditText examPasswordEditText = dialog.findViewById(R.id.exam_password_text);
        final Button openTestButton = dialog.findViewById(R.id.open_test_button);
        Button cancelButton = dialog.findViewById(R.id.cancel_button);
        final TextView examPasswordWrongText = dialog.findViewById(R.id.exam_password_wrong_text);

        examPasswordEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                if (examPasswordEditText.getText().toString().equals("")) {
                    openTestButton.setEnabled(false);
                    openTestButton.setBackgroundResource(R.drawable.background_dialog_button_disable);
                }
                else {
                    openTestButton.setEnabled(true);
                    openTestButton.setBackgroundResource(R.drawable.background_dialog_positive_button);
                }

                if (examPasswordWrongText.getVisibility() == View.VISIBLE)
                    examPasswordWrongText.setVisibility(View.GONE);
            }
        });

        openTestButton.setOnClickListener(v -> {
            if (examPasswordEditText.getText().toString().equals(topic.getTestPassword())) {
                dialog.dismiss();
                TopicActivity activity = (TopicActivity) parentActivity;
                activity.startTest(topic);
            }
            else {
                examPasswordWrongText.setVisibility(View.VISIBLE);
            }
        });
        cancelButton.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }
}
