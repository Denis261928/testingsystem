package com.friend.testingsystem.activity.menu_fragment.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.menu_fragment.contact.MenuContact;
import com.friend.testingsystem.dialog.interfaces.MyDialogWithMessage;

public class AuthConfirmDialogWithMessage implements MyDialogWithMessage {
    private Activity parentActivity;
    private MenuContact.Presenter presenter;

    public AuthConfirmDialogWithMessage(Activity parentActivity, MenuContact.Presenter presenter) {
        this.parentActivity = parentActivity;
        this.presenter = presenter;
    }

    @Override
    public void showDialog(String message) {
        final Dialog dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_confirm);

        TextView messageText = dialog.findViewById(R.id.dialog_message);
        Button positiveButton = dialog.findViewById(R.id.dialog_positive_button);
        Button negativeButton = dialog.findViewById(R.id.dialog_negative_button);

        messageText.setText(message);

        positiveButton.setOnClickListener(v -> {
            dialog.dismiss();
            presenter.navigateToLogin();
        });
        negativeButton.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }

    @Override
    public void showDialog(String title, String message) { }
}
