package com.friend.testingsystem.activity.profile.dialog.update_info.presenter;

import android.annotation.SuppressLint;
import android.util.Log;

import com.friend.testingsystem.helper.UserFieldName;
import com.friend.testingsystem.activity.profile.dialog.update_info.UpdateUserConfirmDialog;
import com.friend.testingsystem.activity.profile.dialog.update_info.contact.UpdateUserContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.UserDetail;
import com.friend.testingsystem.repository.UserDataRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UpdateUserPresenter implements UpdateUserContact.Presenter {
    private UserFieldName userFieldName;
    private String fieldValue;

    private UpdateUserContact.View view;
    private UserDataRepository repository;

    public UpdateUserPresenter(UpdateUserContact.View view, UserDataRepository repository,
                               UserFieldName userFieldName, String fieldValue) {
        this.view = view;
        this.repository = repository;
        this.userFieldName = userFieldName;
        this.fieldValue = fieldValue;
    }

    @Override
    public void updateUser() {
        view.showProgress(getProgressDialogMessage(userFieldName));
        UserDetail userDetail = createUpdatedUserDetail(userFieldName, fieldValue);
        NetworkService.getInstance().getProfileApi()
                .updateUserDetail("Token "+repository.getAuthToken(), repository.getUserId(), userDetail)
                .enqueue(new Callback<UserDetail>() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void onResponse(Call<UserDetail> call, Response<UserDetail> response) {
                        if (response.isSuccessful()) {
                            Log.d(UpdateUserConfirmDialog.TAG, "User: " + response.body().toString());
                            view.showUpdatedUser(response.body(), userFieldName);
                        } else {
                            Log.d(UpdateUserConfirmDialog.TAG, response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @SuppressLint("LongLogTag")
                    @Override
                    public void onFailure(Call<UserDetail> call, Throwable t) {
                        Log.d(UpdateUserConfirmDialog.TAG, t.toString());
                        view.hideProgress();
                    }
                });
    }

    private UserDetail createUpdatedUserDetail(UserFieldName userFieldName, String fieldValue) {
        UserDetail userDetail = new UserDetail();
        switch (userFieldName) {
            case FIRST_NAME:
                userDetail.setFirstName(fieldValue);
                break;
            case LAST_NAME:
                userDetail.setLastName(fieldValue);
                break;
            case PHONE_NUMBER:
                userDetail.setPhoneNumber("380" + fieldValue);
                break;
        }
        return userDetail;
    }

    private String getProgressDialogMessage(UserFieldName userFieldName) {
        String message = "";
        switch (userFieldName) {
            case FIRST_NAME:
                message = "Оновлення імені користувача";
                break;
            case LAST_NAME:
                message = "Оновлення фамілі користувача";
                break;
            case PHONE_NUMBER:
                message = "Оновлення номера телефона користувача";
                break;
        }
        return message;
    }
}
