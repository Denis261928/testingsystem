package com.friend.testingsystem.activity.profile.dialog.update_info.contact;

import com.friend.testingsystem.helper.UserFieldName;
import com.friend.testingsystem.model.UserDetail;

public interface UpdateUserContact {
    interface View {
        void showUpdatedUser(UserDetail userDetail, UserFieldName userFieldName);
        void showProgress(String message);
        void hideProgress();
    }

    interface Presenter {
        void updateUser();
    }
}
