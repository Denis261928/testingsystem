package com.friend.testingsystem.activity.course.сontact;

import com.friend.testingsystem.model.Course;

import java.util.List;

public interface CourseContact {
    interface View {
        void setCreateCourseButtonVisible(boolean visible);
        void showCourses(List<Course> courseList, boolean isSearch);
        void addAndShowCourses(List<Course> courseList);
        void clearCourses();
        void hideLoadingItem();
        void showCreateCourseDialog();
        void showProgress();
        void hideProgress();
    }

    interface Presenter {
        boolean isLoading();
        void setLoading(boolean loading);
        boolean isLastPage();
        void loadInitial();
        void loadMore();
        void loadBySearchText(String searchText);
        void clearList();
        void createCourseClick();
    }
}
