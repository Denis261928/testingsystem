package com.friend.testingsystem.activity.create_section_element;

import android.Manifest;
import android.annotation.SuppressLint;
import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.webkit.MimeTypeMap;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.Nullable;
import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.CommonActivity;
import com.friend.testingsystem.activity.MainActivity;
import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.activity.file_chooser.utils.FileUtils;
import com.friend.testingsystem.activity.section_element.helper.FileIcon;
import com.friend.testingsystem.dialog.InfoDialog;
import com.friend.testingsystem.dialog.LogoutConfirmDialog;
import com.friend.testingsystem.helper.ChooserType;
import com.friend.testingsystem.helper.CountingRequestBody;
import com.friend.testingsystem.helper.NetworkUtils;
import com.friend.testingsystem.helper.ZipArchiver;
import com.friend.testingsystem.model.Section;
import com.friend.testingsystem.repository.UserDataRepository;
import com.karumi.dexter.Dexter;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.PermissionDeniedResponse;
import com.karumi.dexter.listener.PermissionGrantedResponse;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.single.PermissionListener;

import org.jetbrains.annotations.NotNull;

import java.io.File;
import java.io.IOException;

import okhttp3.Callback;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

public class CreateSectionElementActivity extends AppCompatActivity implements CommonActivity {
    public static final String TAG = "CreateSectionElementActivityLog";
    public static final int GALLERY_REQUEST_CODE = 1;
    public static final int FILE_MANAGER_REQUEST_CODE = 2;

    private static String audioFilePath = null;
    private static String audioFilePathSDCard = null;

    private ImageView loginLogoutButton;
    private EditText sectionElementNameEditText;
    private LinearLayout sectionElementContainer;
    private ImageView sectionElementImage;
    private TextView sectionElementNameText;
    private ProgressBar progressBar;
    private Button addSectionElementButton;
    private Button archiveSectionElementButton;

    private Section section;
    private UserDataRepository repository;

    private File file;

    private String SDPath = Environment.getExternalStorageDirectory().getAbsolutePath();
    private String dataPath = SDPath + "/test24/data/";
    private String zipPathFolder = SDPath + "/test24/zip/";
    private String zipName = "";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_section_element);

        if (!NetworkUtils.getNetworkConnection(this)) {
            startActivity(new Intent(this, MainActivity.class));
        }

        Dexter.withActivity(this)
                .withPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                .withListener(new PermissionListener() {
                    @Override
                    public void onPermissionGranted(PermissionGrantedResponse response) { }
                    @Override
                    public void onPermissionDenied(PermissionDeniedResponse response) { }
                    @Override
                    public void onPermissionRationaleShouldBeShown(PermissionRequest permission, PermissionToken token) { }
                }).check();

        ImageView goBack = findViewById(R.id.go_back);
        loginLogoutButton = findViewById(R.id.login_logout_icon);
        sectionElementNameEditText = findViewById(R.id.section_element_name_edit_text);
        LinearLayout selectFromGalleryButton = findViewById(R.id.select_from_gallery_button);
        LinearLayout selectFromFileManagerButton = findViewById(R.id.select_from_manager_button);
        sectionElementContainer = findViewById(R.id.section_element_container);
        sectionElementImage = findViewById(R.id.section_element_image);
        sectionElementNameText = findViewById(R.id.section_element_name_text);
        progressBar = findViewById(R.id.progress_bar);
        addSectionElementButton = findViewById(R.id.add_section_element_button);
        archiveSectionElementButton = findViewById(R.id.archive_section_element_button);
        Button cancelButton = findViewById(R.id.cancel_button);

        repository = new UserDataRepository(this);

        loginLogoutButton.setOnClickListener(view -> loginLogoutClick());

        goBack.setOnClickListener(view -> onBackPressed());

        Section section = (Section) getIntent().getSerializableExtra("section");
        if (section != null)
            this.section = section;

        selectFromGalleryButton.setOnClickListener(view -> performGallerySearch());

        selectFromFileManagerButton.setOnClickListener(view -> performFileManagerSearch());

        sectionElementNameEditText.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) { }

            @Override
            public void afterTextChanged(Editable editable) {
                stateChanged();
            }
        });

        addSectionElementButton.setOnClickListener(view -> {
            progressBar.setVisibility(View.VISIBLE);
            uploadFile();
        });

        archiveSectionElementButton.setOnClickListener(view -> {
            archiveFile();
        });

        cancelButton.setOnClickListener(view -> onBackPressed());
    }

    private void archiveFile() {
        zipName = file.getName().substring(0, file.getName().lastIndexOf('.'));
        zipName = zipName + ".zip";
        ZipArchiver.deleteFileFromDataFolder();
        ZipArchiver.saveToFile(dataPath, file);
        if (ZipArchiver.zip(dataPath, zipPathFolder, zipName, false)) {
            File zipFile = new File(zipPathFolder + zipName);
            setFile(zipFile);
        }
    }

    private void performGallerySearch() {
        // Use the GET_CONTENT intent from the utility class
        Intent intent = FileUtils.createGetContentIntent(ChooserType.MEDIA_FILE);
        startActivityForResult(intent, GALLERY_REQUEST_CODE);
    }

    @SuppressLint("LongLogTag")
    private void performFileManagerSearch() {
        // Use the GET_CONTENT intent from the utility class
        Intent target = FileUtils.createGetContentIntent(ChooserType.FILE);
        // Create the chooser Intent
        Intent intent = Intent.createChooser(target, getString(R.string.chooser_title));
        try {
            startActivityForResult(intent, FILE_MANAGER_REQUEST_CODE);
        } catch (ActivityNotFoundException e) {
            // The reason for the existence of aFileChooser
            Log.e(TAG, e.toString());
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("LongLogTag")
    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        if (data == null ||resultCode != RESULT_OK) {
            return;
        }

        // If the file selection was successful
        // Get the URI of the selected file
        Uri selectedFileUri = data.getData();

        switch (requestCode) {
            case GALLERY_REQUEST_CODE:
                readImageMetaData(selectedFileUri);
                break;
            case FILE_MANAGER_REQUEST_CODE:
                readFileMetaData(selectedFileUri);
                break;
        }

        super.onActivityResult(requestCode, resultCode, data);
    }

    @SuppressLint("LongLogTag")
    public void readImageMetaData(Uri uri) {
        String[] filePathColumn = {MediaStore.Images.Media.DATA};

        Cursor cursor = getContentResolver().query(uri, filePathColumn, null, null, null);
        if (cursor != null && cursor.moveToFirst()) {
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            String picturePath = cursor.getString(columnIndex);
            cursor.close();

            File file = new File(picturePath);
            setFile(file);
            Toast.makeText(this, "Media file selected: " + picturePath, Toast.LENGTH_SHORT).show();
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    @SuppressLint("LongLogTag")
    private void readFileMetaData(Uri uri) {
        Log.d(TAG, "Uri: " + uri.toString());
        try {
            // Get the file path from the URI
            String filePath = FileUtils.getPath(this, uri);
            File file = new File(filePath);
            setFile(file);
            Toast.makeText(this, "File selected: " + filePath, Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
            Log.e(TAG, "File select error", e);
        }
    }

    public void setFile(File file) {
        this.file = file;
        String fileName = file.getName();
        String filePath = file.getPath();
        String fileExtension = filePath.substring(filePath.lastIndexOf(".")).replace(".", "");

        sectionElementContainer.setVisibility(View.VISIBLE);
        FileIcon.setFileIconByFileExtension(sectionElementImage, fileExtension);
        sectionElementNameText.setText(fileName);

        stateChanged();
    }

    @SuppressLint("LongLogTag")
    public void uploadFile() {
        Uri uris = Uri.fromFile(file);
        String fileExtension = MimeTypeMap.getFileExtensionFromUrl(uris.toString());
        String mime = MimeTypeMap.getSingleton().getMimeTypeFromExtension(fileExtension.toLowerCase());
        String fileName = file.getName();

        RequestBody requestBody = new MultipartBody.Builder()
                .setType(MultipartBody.FORM)
                .addFormDataPart("file", fileName, RequestBody.create(file, MediaType.parse(mime)))
                .addFormDataPart("name", sectionElementNameEditText.getText().toString())
                .addFormDataPart("section", String.valueOf(section.getId()))
                .build();

        final CountingRequestBody.Listener progressListener = new CountingRequestBody.Listener() {
            @Override
            public void onRequestProgress(long bytesRead, long contentLength) {
                if (bytesRead >= contentLength) {
                    if (progressBar != null)
                        CreateSectionElementActivity.this.runOnUiThread(new Runnable() {
                            public void run() {
                                progressBar.setVisibility(View.GONE);
                            }
                        });
                } else {
                    if (contentLength > 0) {
                        final int progress = (int) (((double) bytesRead / contentLength) * 100);
                        if (progressBar != null)
                            CreateSectionElementActivity.this.runOnUiThread(new Runnable() {
                                public void run() {
                                    progressBar.setVisibility(View.VISIBLE);
                                    progressBar.setProgress(progress);
                                }
                            });

                        if(progress >= 100){
                            progressBar.setVisibility(View.GONE);
                        }
                        Log.e("Upload progress called", progress+" ");
                    }
                }
            }
        };

        OkHttpClient imageUploadClient = new OkHttpClient.Builder()
                .addNetworkInterceptor(new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Request originalRequest = chain.request();

                        if (originalRequest.body() == null) {
                            return chain.proceed(originalRequest);
                        }
                        Request progressRequest = originalRequest.newBuilder()
                                .method(originalRequest.method(),
                                        new CountingRequestBody(originalRequest.body(), progressListener))
                                .build();

                        return chain.proceed(progressRequest);
                    }
                }).build();

        Request request = new Request.Builder()
                .url("https://test24.com.ua/api/v1/post/create/element/section/")
                .header("Authorization", repository.getAuthToken())
                .header("Accept", "application/json")
                .header("Content-Type", "multipart/form-data")
                .post(requestBody)
                .build();


        imageUploadClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onResponse(@NotNull okhttp3.Call call, @NotNull Response response) throws IOException {
                final String mMessage = response.body().string();

                CreateSectionElementActivity.this.runOnUiThread(new Runnable() {
                    @SuppressLint("LongLogTag")
                    @Override
                    public void run() {
                        Log.e(TAG, mMessage);
                        progressBar.setVisibility(View.GONE);
                        sectionElementNameEditText.setText("");
                        sectionElementContainer.setVisibility(View.GONE);
                        showInfoDialog();
                    }
                });
            }

            @Override
            public void onFailure(@NotNull okhttp3.Call call, @NotNull IOException e) {
                Log.e("Failure response", e.toString());
            }
        });
    }

    private void showInfoDialog() {
        InfoDialog infoDialog = new InfoDialog(this);
        infoDialog.showDialog("Добавлення матеріалу пройшло успішно");
    }

    public void stateChanged() {
        if (sectionElementNameEditText.getText().toString().equals("")) {
            setButtonsEnable(false);
        } else {
            if (sectionElementContainer.getVisibility() == View.VISIBLE)
                setButtonsEnable(true);
        }
    }

    private void setButtonsEnable(boolean enable) {
        if (enable) {
            addSectionElementButton.setBackgroundResource(R.drawable.background_dialog_positive_button);
            archiveSectionElementButton.setBackgroundResource(R.drawable.background_dialog_additional_button);
            addSectionElementButton.setEnabled(true);
            archiveSectionElementButton.setEnabled(true);
        } else {
            addSectionElementButton.setBackgroundResource(R.drawable.background_button_disable);
            archiveSectionElementButton.setBackgroundResource(R.drawable.background_button_disable);
            addSectionElementButton.setEnabled(false);
            archiveSectionElementButton.setEnabled(false);
        }
    }

    @Override
    public void loginLogoutClick() {
        if (repository.isUserAuthorized()) {
            LogoutConfirmDialog logoutConfirmDialog = new LogoutConfirmDialog(this);
            logoutConfirmDialog.showDialog("Ви впевнені що хочете вийти з акаунту?");
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    @Override
    public void setLoginLogoutIcon() {
        if (repository.isUserAuthorized())
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_logout));
        else
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_login));
    }

    @Override
    public void logout() {
        repository.logoutUser();
        setLoginLogoutIcon();
        startActivity(new Intent(this, CategoryActivity.class));
    }
}
