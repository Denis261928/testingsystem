package com.friend.testingsystem.activity.friends.dialog.remove_friend.contact;

import com.friend.testingsystem.model.Friend;

public interface RemoveFriendContact {
    interface View {
        void friendRemoved();
        void showProgress();
        void hideProgress();
    }
    interface Presenter {
        void removeFriend(Friend friend);
    }
}
