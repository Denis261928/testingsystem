package com.friend.testingsystem.activity.test.model;

import java.util.ArrayList;
import java.util.List;

public class UserTask {
    private List<UserAnswer> userAnswerList = new ArrayList<>();
    private int correctAnswerCount;
    private int questionIndex;

    public UserTask(List<UserAnswer> userAnswerList, int correctAnswerCount) {
        this.userAnswerList = userAnswerList;
        this.correctAnswerCount = correctAnswerCount;
    }

    public void addUserAnswer(UserAnswer userAnswer) {
        userAnswerList.add(userAnswer);
    }

    public int getCountOfCheckedAnswer() {
        int count = 0;
        for (UserAnswer userAnswer: userAnswerList) {
            if (userAnswer.isChecked()) {
                count++;
            }
        }
        return count;
    }

    public void setUserAnswerList(List<UserAnswer> userAnswerList) {
        this.userAnswerList = userAnswerList;
    }

    public void setCorrectAnswerCount(int correctAnswerCount) {
        this.correctAnswerCount = correctAnswerCount;
    }

    public List<UserAnswer> getUserAnswerList() {
        return userAnswerList;
    }

    public int getCorrectAnswerCount() {
        return correctAnswerCount;
    }

    public int getQuestionIndex() {
        return questionIndex;
    }

    public void setQuestionIndex(int questionIndex) {
        this.questionIndex = questionIndex;
    }

    @Override
    public String toString() {
        return "UserTask{" +
                "userAnswerList=" + userAnswerList +
                ", correctAnswerCount=" + correctAnswerCount +
                '}';
    }
}
