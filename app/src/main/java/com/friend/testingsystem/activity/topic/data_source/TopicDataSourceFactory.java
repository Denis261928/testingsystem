package com.friend.testingsystem.activity.topic.data_source;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.friend.testingsystem.model.Topic;

public class TopicDataSourceFactory extends DataSource.Factory {
    private MutableLiveData<PageKeyedDataSource<Integer, Topic>> topicLiveDataSource = new MutableLiveData<>();
    private long subCategoryId;

    public TopicDataSourceFactory(long subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

    @Override
    public DataSource create() {
        TopicDataSource topicDataSource = new TopicDataSource(subCategoryId);
        topicLiveDataSource.postValue(topicDataSource);
        return topicDataSource;
    }

    public MutableLiveData<PageKeyedDataSource<Integer, Topic>> getTopicLiveDataSource() {
        return topicLiveDataSource;
    }
}
