package com.friend.testingsystem.activity.test.model;

import java.util.List;

public class UserAllTests {
    private List<UserTest> userTestList;
    private boolean isFinished = false;

    public UserAllTests(List<UserTest> userTestList) {
        this.userTestList = userTestList;
    }

    public List<UserTest> getUserTestList() {
        return userTestList;
    }

    public void setUserTestList(List<UserTest> userTestList) {
        this.userTestList = userTestList;
    }

    public boolean isFinished() {
        return isFinished;
    }

    public void setFinished(boolean finished) {
        isFinished = finished;
    }

    @Override
    public String toString() {
        return "UserAllTests{" +
                "userTestList=" + userTestList +
                ", isFinished=" + isFinished +
                '}';
    }
}
