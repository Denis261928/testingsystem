package com.friend.testingsystem.activity.sub_category.data_source;

import androidx.lifecycle.MutableLiveData;
import androidx.paging.DataSource;
import androidx.paging.PageKeyedDataSource;

import com.friend.testingsystem.model.SubCategory;

public class SubCategoryDataSourceFactory extends DataSource.Factory {

    private MutableLiveData<PageKeyedDataSource<Integer, SubCategory>> subCategoryLiveDataSource = new MutableLiveData<>();
    private long categoryId;

    public SubCategoryDataSourceFactory(long categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public DataSource create() {
        SubCategoryDataSource subCategoryDataSource = new SubCategoryDataSource(categoryId);
        subCategoryLiveDataSource.postValue(subCategoryDataSource);
        return subCategoryDataSource;
    }

    public MutableLiveData<PageKeyedDataSource<Integer, SubCategory>> getSubCategoryLiveDataSource() {
        return subCategoryLiveDataSource;
    }
}
