package com.friend.testingsystem.activity.course.dialog.presenter;

import android.util.Log;

import com.friend.testingsystem.activity.course.dialog.CreateCourseDialog;
import com.friend.testingsystem.activity.course.dialog.contact.CreateCourseContact;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.Course;
import com.friend.testingsystem.repository.UserDataRepository;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class CreateCoursePresenter implements CreateCourseContact.Presenter {
    private CreateCourseContact.View view;
    private UserDataRepository repository;

    public CreateCoursePresenter(CreateCourseContact.View view, UserDataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void createCourse(String courseName) {
        view.showProgress();
        Course course = new Course();
        course.setName(courseName);
        NetworkService.getInstance().getCourseApi().createCourse("Token "+repository.getAuthToken(), course)
                .enqueue(new Callback<Course>() {
                    @Override
                    public void onResponse(Call<Course> call, Response<Course> response) {
                        if (response.isSuccessful()) {
                            view.courseCreated();
                        }
                        else {
                            Log.d(CreateCourseDialog.TAG, response.errorBody().toString());
                        }
                        view.hideProgress();
                    }

                    @Override
                    public void onFailure(Call<Course> call, Throwable t) {
                        Log.d(CreateCourseDialog.TAG, t.getMessage());
                        view.hideProgress();
                    }
                });
    }
}
