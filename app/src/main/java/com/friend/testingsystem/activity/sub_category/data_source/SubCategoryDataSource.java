package com.friend.testingsystem.activity.sub_category.data_source;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.paging.PageKeyedDataSource;

import com.friend.testingsystem.activity.sub_category.SubCategoryActivity;
import com.friend.testingsystem.api.NetworkService;
import com.friend.testingsystem.model.ResponseListData;
import com.friend.testingsystem.model.SubCategory;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class SubCategoryDataSource extends PageKeyedDataSource<Integer, SubCategory> {

    private long categoryId;

    public SubCategoryDataSource(long categoryId) {
        this.categoryId = categoryId;
    }

    @Override
    public void loadInitial(@NonNull LoadInitialParams<Integer> params, @NonNull final LoadInitialCallback<Integer, SubCategory> callback) {
        NetworkService.getInstance().getSubCategoryApi().getSubCategoryListByCategoryIdAndByPage(categoryId, 1)
                .enqueue(new Callback<ResponseListData<SubCategory>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<SubCategory>> call, Response<ResponseListData<SubCategory>> response) {
                        if (response.body() != null) {
                            callback.onResult(response.body().getResults(), null, 2);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<SubCategory>> call, Throwable t) {
                        Log.d(SubCategoryActivity.TAG, t.getMessage());
                    }
                });
    }

    @Override
    public void loadBefore(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, SubCategory> callback) {
        NetworkService.getInstance().getSubCategoryApi().getSubCategoryListByCategoryIdAndByPage(categoryId, params.key)
                .enqueue(new Callback<ResponseListData<SubCategory>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<SubCategory>> call, Response<ResponseListData<SubCategory>> response) {
                        // Integer key = (params.key > 1) ? params.key - 1 : null;
                        if (response.body() != null) {
                            // Integer key = response.body().getPrevious() != null ? params.key - 1 : null;
                            callback.onResult(response.body().getResults(), params.key-1);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<SubCategory>> call, Throwable t) {

                    }
                });
    }

    @Override
    public void loadAfter(@NonNull final LoadParams<Integer> params, @NonNull final LoadCallback<Integer, SubCategory> callback) {
        NetworkService.getInstance().getSubCategoryApi().getSubCategoryListByCategoryIdAndByPage(categoryId, params.key)
                .enqueue(new Callback<ResponseListData<SubCategory>>() {
                    @Override
                    public void onResponse(Call<ResponseListData<SubCategory>> call, Response<ResponseListData<SubCategory>> response) {
                        if (response.body() != null) {
                            // Integer key = response.body().getNext() != null ? params.key + 1 : null;
                            callback.onResult(response.body().getResults(), params.key+1);
                        }
                    }

                    @Override
                    public void onFailure(Call<ResponseListData<SubCategory>> call, Throwable t) {

                    }
                });
    }
}
