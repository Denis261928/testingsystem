package com.friend.testingsystem.activity.menu_fragment.presenter;

import com.friend.testingsystem.activity.menu_fragment.contact.MenuContact;
import com.friend.testingsystem.helper.ActivityEnum;
import com.friend.testingsystem.repository.UserDataRepository;

public class MenuPresenter implements MenuContact.Presenter {
    private MenuContact.View view;
    private UserDataRepository repository;

    public MenuPresenter(MenuContact.View view, UserDataRepository repository) {
        this.view = view;
        this.repository = repository;
    }

    @Override
    public void navigateToTestingHistory() {
        if (repository.isUserAuthorized())
            view.navigateTo(ActivityEnum.TESTING_HISTORY);
        else
            view.showAuthDialog("Щоб вести історію тестувань вам потрібно авторизуватись.\nПерейти на сторінку авторизації?");
    }

    @Override
    public void navigateToFriends() {
        if (repository.isUserAuthorized())
            view.navigateTo(ActivityEnum.FRIENDS);
        else
            view.showAuthDialog("Щоб перейти до списку друзів вам потрібно авторизуватись.\nПерейти на сторінку авторизації?");
    }

    @Override
    public void navigateToCourse() {
        if (repository.isUserAuthorized())
            view.navigateTo(ActivityEnum.COURSE);
        else
            view.showAuthDialog("Щоб перейти до курсів вам потрібно авторизуватись.\nПерейти на сторінку авторизації?");
    }

    @Override
    public void navigateToLogin() {
        view.navigateTo(ActivityEnum.LOGIN);
    }

    @Override
    public void navigateToProfile() {
        if (repository.isUserAuthorized())
            view.navigateTo(ActivityEnum.PROFILE);
        else
            view.showAuthDialog("Щоб перейти до профілю вам потрібно авторизуватись.\nПерейти на сторінку авторизації?");
    }
}
