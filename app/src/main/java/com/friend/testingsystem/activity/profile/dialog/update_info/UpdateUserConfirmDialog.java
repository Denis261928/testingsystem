package com.friend.testingsystem.activity.profile.dialog.update_info;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.helper.UserFieldName;
import com.friend.testingsystem.activity.profile.ProfileActivity;
import com.friend.testingsystem.activity.profile.dialog.update_info.contact.UpdateUserContact;
import com.friend.testingsystem.activity.profile.dialog.update_info.presenter.UpdateUserPresenter;
import com.friend.testingsystem.dialog.interfaces.MyDialogWithMessage;
import com.friend.testingsystem.model.UserDetail;
import com.friend.testingsystem.repository.UserDataRepository;

public class UpdateUserConfirmDialog implements MyDialogWithMessage, UpdateUserContact.View {
    public static final String TAG = "UpdateUserConfirmDialogLog";

    private Dialog dialog;
    private ProgressDialog progressDialog;

    private Activity parentActivity;
    private UpdateUserPresenter updateUserPresenter;

    public UpdateUserConfirmDialog(Activity parentActivity, UserFieldName userFieldName, String fieldValue) {
        this.parentActivity = parentActivity;
        UserDataRepository repository = new UserDataRepository(parentActivity);
        updateUserPresenter = new UpdateUserPresenter(this, repository, userFieldName, fieldValue);
    }

    @Override
    public void showDialog(String message) {
        dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_confirm);

        TextView messageText = dialog.findViewById(R.id.dialog_message);
        Button positiveButton = dialog.findViewById(R.id.dialog_positive_button);
        Button negativeButton = dialog.findViewById(R.id.dialog_negative_button);

        messageText.setText(message);

        positiveButton.setOnClickListener(view -> updateUserPresenter.updateUser());

        negativeButton.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }

    @Override
    public void showDialog(String title, String message) {
        dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_confirm);

        TextView messageText = dialog.findViewById(R.id.dialog_message);
        TextView titleText = dialog.findViewById(R.id.dialog_title);
        Button positiveButton = dialog.findViewById(R.id.dialog_positive_button);
        Button negativeButton = dialog.findViewById(R.id.dialog_negative_button);

        titleText.setText(title);
        messageText.setText(message);

        positiveButton.setOnClickListener(view -> updateUserPresenter.updateUser());

        negativeButton.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }

    @Override
    public void showUpdatedUser(UserDetail userDetail, UserFieldName userFieldName) {
        dialog.dismiss();
        ProfileActivity profileActivity = (ProfileActivity) parentActivity;
        profileActivity.showUpdatedUser(userDetail, userFieldName);
    }

    @Override
    public void showProgress(String message) {
        progressDialog = new ProgressDialog(parentActivity);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }
}
