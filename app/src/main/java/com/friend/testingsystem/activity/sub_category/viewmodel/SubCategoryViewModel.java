package com.friend.testingsystem.activity.sub_category.viewmodel;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import androidx.paging.LivePagedListBuilder;
import androidx.paging.PageKeyedDataSource;
import androidx.paging.PagedList;

import com.friend.testingsystem.activity.sub_category.data_source.SubCategoryDataSourceFactory;
import com.friend.testingsystem.model.SubCategory;

import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

public class SubCategoryViewModel extends ViewModel {

    public LiveData<PagedList<SubCategory>> subCategoryPageList;
    public LiveData<PageKeyedDataSource<Integer, SubCategory>> liveDataSource;

    public SubCategoryViewModel(long categoryId) {
        Executor executor = Executors.newFixedThreadPool(5);

        SubCategoryDataSourceFactory subCategoryDataSourceFactory = new SubCategoryDataSourceFactory(categoryId);
        liveDataSource = subCategoryDataSourceFactory.getSubCategoryLiveDataSource();

        PagedList.Config config = (new PagedList.Config.Builder()
                .setEnablePlaceholders(false)
                .setPageSize(10)
                .setInitialLoadSizeHint(20))
                .build();

        subCategoryPageList = (new LivePagedListBuilder(subCategoryDataSourceFactory, config))
                .setFetchExecutor(executor)
                .build();
    }
}
