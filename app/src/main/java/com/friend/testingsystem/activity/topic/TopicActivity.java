package com.friend.testingsystem.activity.topic;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModel;
import androidx.lifecycle.ViewModelProvider;
import androidx.lifecycle.ViewModelProviders;
import androidx.paging.PagedList;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.CommonActivity;
import com.friend.testingsystem.activity.MainActivity;
import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.test.TestActivity;
import com.friend.testingsystem.activity.theory.TheoryActivity;
import com.friend.testingsystem.activity.topic.adapter.TopicAdapter;
import com.friend.testingsystem.activity.topic.dialog.ExamPasswordDialog;
import com.friend.testingsystem.activity.topic.viewmodel.TopicViewModel;
import com.friend.testingsystem.dialog.LogoutConfirmDialog;
import com.friend.testingsystem.helper.NetworkUtils;
import com.friend.testingsystem.model.SubCategory;
import com.friend.testingsystem.model.Topic;
import com.friend.testingsystem.repository.UserDataRepository;

import java.io.Serializable;

public class TopicActivity extends AppCompatActivity implements CommonActivity, TopicAdapter.OnTopicListener {
    public static final String TAG = "TopicActivityLog";

    private ImageView loginLogoutButton;

    private UserDataRepository repository;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_topic);

        ImageView goBack = findViewById(R.id.go_back);
        loginLogoutButton = findViewById(R.id.login_logout_icon);
        TextView categoryNameText = findViewById(R.id.sub_category_name_text);
        RecyclerView recyclerView = findViewById(R.id.topics_list);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        if (!NetworkUtils.getNetworkConnection(this)) {
            startActivity(new Intent(this, MainActivity.class));
        }

        repository = new UserDataRepository(this);
        setLoginLogoutIcon();

        final SubCategory subCategory = (SubCategory) getIntent().getSerializableExtra("sub_category");
        if (subCategory != null) {
            categoryNameText.setText(subCategory.getName()+":");
        }

        loginLogoutButton.setOnClickListener(view -> loginLogoutClick());

        goBack.setOnClickListener(view -> onBackPressed());

        TopicViewModel topicViewModel = ViewModelProviders.of(this, new ViewModelProvider.Factory() {
            @NonNull
            @Override
            public <T extends ViewModel> T create(@NonNull Class<T> modelClass) {
                return (T) new TopicViewModel(subCategory.getId());
            }
        }).get(TopicViewModel.class);

        final TopicAdapter adapter = new TopicAdapter(this, this);

        topicViewModel.topicPageList.observe(this, new Observer<PagedList<Topic>>() {
            @Override
            public void onChanged(PagedList<Topic> topics) {
                adapter.submitList(topics);
            }
        });
        recyclerView.setAdapter(adapter);

    }

    @Override
    public void loginLogoutClick() {
        if (repository.isUserAuthorized()) {
            LogoutConfirmDialog logoutConfirmDialog = new LogoutConfirmDialog(this);
            logoutConfirmDialog.showDialog("Ви впевнені що хочете вийти з акаунту?");
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    @Override
    public void setLoginLogoutIcon() {
        if (repository.isUserAuthorized())
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_logout));
        else
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_login));
    }

    @Override
    public void logout() {
        repository.logoutUser();
        setLoginLogoutIcon();
    }

    public void startTest(Topic topic) {
        Intent intent = new Intent(TopicActivity.this, TestActivity.class);
        intent.putExtra("topic", (Serializable) topic);
        startActivity(intent);
    }

    @Override
    public void onTopicClick(Topic topic) {
        if (topic.isTestOpen()) {
            Intent intent = new Intent(TopicActivity.this, TestActivity.class);
            intent.putExtra("topic", (Serializable) topic);
            startActivity(intent);
        } else {
            ExamPasswordDialog dialog = new ExamPasswordDialog(this, topic);
            dialog.showDialog();
        }
    }

    @Override
    public void onTheoryClick(Topic topic) {
        Intent intent = new Intent(TopicActivity.this, TheoryActivity.class);
        intent.putExtra("topic", (Serializable) topic);
        startActivity(intent);
    }
}
