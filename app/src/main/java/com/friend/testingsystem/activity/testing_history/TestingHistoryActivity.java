package com.friend.testingsystem.activity.testing_history;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.CommonActivity;
import com.friend.testingsystem.activity.MainActivity;
import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.activity.testing_history.adapter.TestingHistoryListAdapter;
import com.friend.testingsystem.activity.testing_history.contact.TestingHistoryContact;
import com.friend.testingsystem.activity.testing_history.presenter.TestingHistoryPresenter;
import com.friend.testingsystem.dialog.LogoutConfirmDialog;
import com.friend.testingsystem.helper.NetworkUtils;
import com.friend.testingsystem.model.Friend;
import com.friend.testingsystem.model.TestingHistoryItem;
import com.friend.testingsystem.repository.UserDataRepository;

import java.util.List;

public class TestingHistoryActivity extends AppCompatActivity implements TestingHistoryContact.View, CommonActivity {
    public static final String TAG = "TestingHistoryActivityLog";

    public boolean isLoading = false;
    public Handler handler;

    private ProgressDialog progressDialog;
    private ImageView loginLogoutButton;
    private LinearLayout testingHistoryContent;
    private RelativeLayout testingHistoryContentEmpty;
    private TextView testingHistoryEmptyMessageText;
    private ListView listView;
    private View loadingView;

    private TestingHistoryListAdapter adapter;
    private UserDataRepository repository;
    private TestingHistoryPresenter presenter;

    private Friend friend;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_testing_history);

        ImageView goBack = findViewById(R.id.go_back);
        loginLogoutButton = findViewById(R.id.login_logout_icon);
        testingHistoryContent = findViewById(R.id.testing_history_content);
        testingHistoryContentEmpty = findViewById(R.id.testing_history_content_empty);
        testingHistoryEmptyMessageText = findViewById(R.id.testing_history_empty_message_text);
        listView = findViewById(R.id.testing_history_list_view);

        repository = new UserDataRepository(this);
        setLoginLogoutIcon();
        presenter = new TestingHistoryPresenter(this, repository);

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.item_loading, null);
        View headerView = inflater.inflate(R.layout.item_testing_history_header, null);
        TextView testingHistoryTitle = headerView.findViewById(R.id.testing_history_title);
        friend = (Friend) getIntent().getSerializableExtra("friend");
        if (friend != null) {
            presenter.setFriend(friend);
            testingHistoryTitle.setText("Історія тестуваннь друга " + friend.getUserName());
        }
        listView.addHeaderView(headerView);
        handler = new MyHandler();

        if (!NetworkUtils.getNetworkConnection(this)) {
            startActivity(new Intent(this, MainActivity.class));
        }

        presenter.loadInitial();

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) { }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                // Check when scroll to last item in listView, in this tut, init data in list view
                if (absListView.getLastVisiblePosition() == i2-1 && listView.getCount() >= 10
                        && !presenter.isLoading() && !presenter.isLastPage()) {
                    presenter.setLoading(true);
                    Thread thread = new ThreadGetMoreData();
                    thread.start();
                }
            }
        });

        goBack.setOnClickListener(v -> onBackPressed());
    }

    @Override
    public void onBackPressed() {
        startActivity(new Intent(this, CategoryActivity.class));
    }

    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case 0:
                    listView.addFooterView(loadingView);
                    break;
                case 1:
                    adapter.addListItemsToAdapter((List<TestingHistoryItem>) msg.obj);
                    listView.removeFooterView(loadingView);
                    presenter.setLoading(false);
                    break;
                case 2:
                    listView.removeFooterView(loadingView);
                    presenter.setLoading(false);
                    break;
                default:
                    break;
            }
        }
    }

    public class ThreadGetMoreData extends Thread {
        @Override
        public void run() {
            // Add loading view during search processing
            handler.sendEmptyMessage(0);
            // Search more data
            presenter.loadMore();
        }
    }

    @Override
    public void showTestingHistory(List<TestingHistoryItem> testingHistoryList) {
        if (testingHistoryList.size() > 0) {
            testingHistoryContent.setVisibility(View.VISIBLE);
            testingHistoryContentEmpty.setVisibility(View.GONE);
            adapter = new TestingHistoryListAdapter(getApplicationContext(), testingHistoryList);
            listView.setAdapter(adapter);
        } else {
            testingHistoryContent.setVisibility(View.GONE);
            testingHistoryContentEmpty.setVisibility(View.VISIBLE);
            if (friend != null) {
                testingHistoryEmptyMessageText.setText("Ваш друг " + friend.getUserName() + " не проходив жодного тесту");
            }
        }
    }

    @Override
    public void addAndShowTestingHistory(List<TestingHistoryItem> testingHistoryList) {
        // Thread.sleep(1000);
        // Update data adapter and UI
        Message message = handler.obtainMessage(1, testingHistoryList);
        handler.sendMessage(message);
    }

    @Override
    public void hideLoadingItem() {
        // Remove loading view when there is not any other data
        handler.sendEmptyMessage(2);
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Завантаження...");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void loginLogoutClick() {
        if (repository.isUserAuthorized()) {
            LogoutConfirmDialog logoutConfirmDialog = new LogoutConfirmDialog(this);
            logoutConfirmDialog.showDialog("Ви впевнені що хочете вийти з акаунту?");
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    @Override
    public void setLoginLogoutIcon() {
        if (repository.isUserAuthorized())
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_logout));
        else
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_login));
    }

    @Override
    public void logout() {
        repository.logoutUser();
        setLoginLogoutIcon();
        startActivity(new Intent(this, CategoryActivity.class));
    }
}
