package com.friend.testingsystem.activity.friends;

import android.annotation.SuppressLint;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.friend.testingsystem.R;
import com.friend.testingsystem.activity.CommonActivity;
import com.friend.testingsystem.activity.MainActivity;
import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.activity.friends.adapter.FriendsAdapter;
import com.friend.testingsystem.activity.friends.dialog.add_friend.AddFriendDialog;
import com.friend.testingsystem.activity.friends.dialog.remove_friend.RemoveFriendConfirmDialog;
import com.friend.testingsystem.activity.friends.contact.FriendsContact;
import com.friend.testingsystem.activity.friends.presenter.FriendsPresenter;
import com.friend.testingsystem.activity.testing_history.TestingHistoryActivity;
import com.friend.testingsystem.dialog.LogoutConfirmDialog;
import com.friend.testingsystem.helper.NetworkUtils;
import com.friend.testingsystem.model.Friend;
import com.friend.testingsystem.repository.UserDataRepository;
import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.List;

public class FriendsActivity extends AppCompatActivity implements FriendsContact.View, CommonActivity, FriendsAdapter.OnFriendListener {
    public static final String TAG = "FriendsActivityLog";

    private Handler handler;
    private ProgressDialog progressDialog;
    private ImageView loginLogoutButton;
    private LinearLayout friendsContent;
    private RelativeLayout friendsContentEmpty;
    private ListView listView;
    private View loadingView;

    private FriendsAdapter adapter;
    private UserDataRepository repository;
    private FriendsPresenter presenter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_friends);

        ImageView goBack = findViewById(R.id.go_back);
        loginLogoutButton = findViewById(R.id.login_logout_icon);
        friendsContent = findViewById(R.id.friends_content);
        friendsContentEmpty = findViewById(R.id.friends_content_empty);
        listView = findViewById(R.id.friends_list);
        FloatingActionButton addFriendButton = findViewById(R.id.add_friend_button);

        if (!NetworkUtils.getNetworkConnection(this)) {
            startActivity(new Intent(this, MainActivity.class));
        }

        LayoutInflater inflater = (LayoutInflater) getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        loadingView = inflater.inflate(R.layout.item_loading, null);
        View headerView = inflater.inflate(R.layout.item_friend_header, null);
        listView.addHeaderView(headerView);
        handler = new MyHandler();

        repository = new UserDataRepository(this);
        setLoginLogoutIcon();
        presenter = new FriendsPresenter(this, repository);
        presenter.loadInitial();

        loginLogoutButton.setOnClickListener(view -> loginLogoutClick());

        goBack.setOnClickListener(view -> onBackPressed());

        addFriendButton.setOnClickListener(view -> presenter.addFriendClick());

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
                Friend friend = adapter.getFriendList().get(i - 1);
                Intent intent = new Intent(FriendsActivity.this, TestingHistoryActivity.class);
                intent.putExtra("friend", friend);
                startActivity(intent);
            }
        });

        listView.setOnScrollListener(new AbsListView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(AbsListView absListView, int i) {

            }

            @Override
            public void onScroll(AbsListView absListView, int i, int i1, int i2) {
                // Check when scroll to last item in listView, in this tut, init data in list view
                if (absListView.getLastVisiblePosition() == i2-1 && listView.getCount() >= 10
                        && !presenter.isLoading() && !presenter.isLastPage()) {
                    presenter.setLoading(true);
                    Thread thread = new ThreadGetMoreData();
                    thread.start();
                }
            }
        });
    }

    @SuppressLint("HandlerLeak")
    public class MyHandler extends Handler {
        @Override
        public void handleMessage(@NonNull Message msg) {
            switch (msg.what) {
                case 0:
                    listView.addFooterView(loadingView);
                    break;
                case 1:
                    adapter.addListItemsToAdapter((List<Friend>) msg.obj);
                    listView.removeFooterView(loadingView);
                    presenter.setLoading(false);
                    break;
                case 2:
                    listView.removeFooterView(loadingView);
                    presenter.setLoading(false);
                    break;
                case 3:
                    adapter.clearFriendList();
                    presenter.loadInitial();
                    break;
                default:
                    break;
            }
        }
    }

    public class ThreadGetMoreData extends Thread {
        @Override
        public void run() {
            // Add loading view during search processing
            handler.sendEmptyMessage(0);
            // Search more data
            presenter.loadMore();
        }
    }

    @Override
    public void showUserFriends(List<Friend> friendList) {
        if (friendList.size() > 0) {
            friendsContent.setVisibility(View.VISIBLE);
            friendsContentEmpty.setVisibility(View.GONE);
            adapter = new FriendsAdapter(getApplicationContext(), friendList, this);
            listView.setAdapter(adapter);
        }
        else {
            friendsContent.setVisibility(View.GONE);
            friendsContentEmpty.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void addAndShowUserFriends(List<Friend> friendList) {
        // Update data adapter and UI
        Message message = handler.obtainMessage(1, friendList);
        handler.sendMessage(message);
    }

    @Override
    public void clearUserFriends() {
        handler.sendEmptyMessage(3);
    }

    @Override
    public void hideLoadingItem() {
        // Remove loading view when there is not any other data
        handler.sendEmptyMessage(2);
    }

    @Override
    public void showAddFriendDialog() {
        AddFriendDialog dialog = new AddFriendDialog(this, presenter);
        dialog.showDialog();
    }

    @Override
    public void showRemoveFriendDialog(Friend friend) {
        RemoveFriendConfirmDialog dialog = new RemoveFriendConfirmDialog(this, presenter, friend);
        dialog.showDialog("Видалення друга", "Ви впевнені що хочете видалити "+ friend.getUserName() +" з друзів?");
    }

    @Override
    public void onRemoveFriendClick(Friend friend) {
        presenter.removeFriendClick(friend);
    }

    @Override
    public void showProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Завантаження друзів");
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    public void hideProgress() {
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    @Override
    public void loginLogoutClick() {
        if (repository.isUserAuthorized()) {
            LogoutConfirmDialog logoutConfirmDialog = new LogoutConfirmDialog(this);
            logoutConfirmDialog.showDialog("Ви впевнені що хочете вийти з акаунту?");
        } else {
            startActivity(new Intent(this, LoginActivity.class));
        }
    }

    @Override
    public void setLoginLogoutIcon() {
        if (repository.isUserAuthorized())
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_logout));
        else
            loginLogoutButton.setImageDrawable(getResources().getDrawable(R.drawable.ic_login));
    }

    @Override
    public void logout() {
        repository.logoutUser();
        setLoginLogoutIcon();
        startActivity(new Intent(this, CategoryActivity.class));
    }
}
