package com.friend.testingsystem.repository;

import android.content.Context;
import android.content.SharedPreferences;

import com.friend.testingsystem.model.User;
import com.friend.testingsystem.model.UserDetail;
import com.google.gson.Gson;

public class UserDataRepository {
    public static final String STORED_DATA = "user";

    private static SharedPreferences preferences;

    private Gson gson = new Gson();

    public UserDataRepository(Context context) {
        preferences = context.getSharedPreferences(STORED_DATA, Context.MODE_PRIVATE);
    }

    private SharedPreferences.Editor getEditor() {
        return preferences.edit();
    }

    public void setUserId(long userId) {
        getEditor().putLong("user_id", userId).commit();
    }

    public long getUserId() {
        return preferences.getLong("user_id", -1);
    }

    public void setUserDetail(UserDetail userDetail) {
        String userDetailJson = gson.toJson(userDetail);
        getEditor().putString("user_detail", userDetailJson).commit();
    }

    public UserDetail getUserDetail() {
        String userJson = preferences.getString("user_detail", "");
        if (userJson.equals("")) {
            return null;
        }
        return gson.fromJson(userJson, UserDetail.class);
    }

    public void saveUser(User user) {
        String userJson = gson.toJson(user);
        getEditor().putString("user_json", userJson).commit();
    }

    public void logoutUser() {
        getEditor().putString("user_json", "").commit();
        getEditor().putString("auth_token", "").commit();
        getEditor().putLong("user_id", -1).commit();
    }

    public boolean isUserAuthorized() {
        return getUserId() != -1;
    }

    public User getAuthorizedUser() {
        String userJson = preferences.getString("user_json", "");
        if (userJson.equals("")) {
            return null;
        }
        return gson.fromJson(userJson, User.class);
    }

    public void setAuthToken(String authToken) {
        getEditor().putString("auth_token", authToken).commit();
    }

    public String getAuthToken() {
        return preferences.getString("auth_token", "");
    }

    public void setEmail(String email) {
        getEditor().putString("email", email).commit();
    }

    public String getEmail() {
        return preferences.getString("email", "");
    }
}
