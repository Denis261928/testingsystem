package com.friend.testingsystem.repository;

import com.friend.testingsystem.activity.test.model.UserAllTests;
import com.friend.testingsystem.activity.test.model.UserTest;

import java.util.ArrayList;
import java.util.List;

public class TestDataRepository {
    private static UserAllTests userAllTests;

    public static List<UserTest> getUserTestList() {
        return userAllTests.getUserTestList();
    }

    public static void setUserTestList(List<UserTest> userTestList1) {
        userAllTests = new UserAllTests(userTestList1);
    }

    public static void clearUserTestList() {
        userAllTests = null;
    }

    public static void finishTest() {
        userAllTests.setFinished(true);
        for (UserTest userTest : userAllTests.getUserTestList()) {
            userTest.setActive(false);
        }
    }

    public static boolean isTestFinished() {
        return userAllTests.isFinished();
    }
}
