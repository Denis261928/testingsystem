package com.friend.testingsystem.dialog.interfaces;

public interface MyDialogWithMessage {
    void showDialog(String message);
    void showDialog(String title, String message);
}
