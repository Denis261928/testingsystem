package com.friend.testingsystem.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.view.View;
import android.view.Window;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.friend.testingsystem.R;
import com.friend.testingsystem.dialog.interfaces.MyDialogWithMessage;

public class InfoDialog implements MyDialogWithMessage {
    private Activity parentActivity;

    public InfoDialog(Activity parentActivity) {
        this.parentActivity = parentActivity;
    }

    @Override
    public void showDialog(String message) {
        final Dialog dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_alert);

        TextView messageText = dialog.findViewById(R.id.dialog_message);
        RelativeLayout hideDialogButton = dialog.findViewById(R.id.hide_dialog_button);

        messageText.setText(message);

        hideDialogButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
            }
        });

        dialog.show();
    }

    @Override
    public void showDialog(String title, String message) {
        final Dialog dialog = new Dialog(parentActivity);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.dialog_alert);

        TextView titleText = dialog.findViewById(R.id.dialog_title);
        TextView messageText = dialog.findViewById(R.id.dialog_message);
        RelativeLayout hideDialogButton = dialog.findViewById(R.id.hide_dialog_button);

        titleText.setText(title);
        messageText.setText(message);

        hideDialogButton.setOnClickListener(view -> dialog.dismiss());

        dialog.show();
    }

}
