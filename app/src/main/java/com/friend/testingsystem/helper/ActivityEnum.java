package com.friend.testingsystem.helper;

public enum ActivityEnum {
    LOGIN, TESTING_HISTORY, FRIENDS, COURSE, PROFILE
}
