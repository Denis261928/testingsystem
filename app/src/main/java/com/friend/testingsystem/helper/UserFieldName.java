package com.friend.testingsystem.helper;

public enum UserFieldName {
    FIRST_NAME, LAST_NAME, PHONE_NUMBER
}
