package com.friend.testingsystem;

import androidx.test.espresso.Espresso;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.espresso.intent.Intents;
import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.friend.testingsystem.activity.auth.LoginActivity;
import com.friend.testingsystem.activity.category.CategoryActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;

@RunWith(AndroidJUnit4.class)
public class LoginActivityTest {

    @Rule
    public final ActivityTestRule<LoginActivity> loginActivityRule = new ActivityTestRule<>(LoginActivity.class);

    @Before
    public void setUp() {
        Intents.init();
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    @Test
    public void testAuthorisation() {
        Espresso.onView(ViewMatchers.withId(R.id.user_name_login_text))
                .perform(ViewActions.typeText("DenisPitsul"));
        ViewActions.closeSoftKeyboard();
        Espresso.onView(ViewMatchers.withId(R.id.password_login_text))
                .perform(ViewActions.typeText("Denia261298"));
        ViewActions.closeSoftKeyboard();
        Espresso.onView(ViewMatchers.withId(R.id.login_button))
                .perform(ViewActions.click());

        Intents.intended(hasComponent(CategoryActivity.class.getName()));
    }

    @Test
    public void testFailAuthorisationWithUnknownUserName() {
        Espresso.onView(ViewMatchers.withId(R.id.user_name_login_text))
                .perform(ViewActions.typeText("Unknown"));
        ViewActions.closeSoftKeyboard();
        Espresso.onView(ViewMatchers.withId(R.id.password_login_text))
                .perform(ViewActions.typeText("testtest"));
        ViewActions.closeSoftKeyboard();
        Espresso.onView(ViewMatchers.withId(R.id.login_button))
                .perform(ViewActions.click());

        Espresso.onView(ViewMatchers.withText("Ви ввели не корректні дані."))
                .check(ViewAssertions.matches(ViewMatchers.isDisplayed()));
        Espresso.onView(ViewMatchers.withId(R.id.hide_dialog_button))
                .perform(ViewActions.click());
    }
}
