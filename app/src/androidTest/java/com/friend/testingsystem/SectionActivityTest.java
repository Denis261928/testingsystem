package com.friend.testingsystem;

import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.friend.testingsystem.activity.course.CourseActivity;
import com.friend.testingsystem.activity.section.SectionActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.closeSoftKeyboard;
import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.anything;

@RunWith(AndroidJUnit4.class)
public class SectionActivityTest {
    @Rule
    public final ActivityTestRule<CourseActivity> courseActivityRule = new ActivityTestRule<>(CourseActivity.class);

    @Before
    public void setUp() {
        Intents.init();
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    @Test
    public void testCreateCourse() throws InterruptedException {
        Thread.sleep(500);
        onData(anything()).inAdapterView(withId(R.id.course_list)).atPosition(1).perform(click());
        Intents.intended(hasComponent(SectionActivity.class.getName()));
        onView(withId(R.id.create_section_button)).perform(click());
        Thread.sleep(500);
        onView(withText("Створення розділу")).check(matches(isDisplayed()));
        onView(withId(R.id.section_name_edit_text)).perform(typeText("New section"));
        closeSoftKeyboard();
        onView(withId(R.id.create_section_button)).perform(click());
        Thread.sleep(500);
    }
}
