package com.friend.testingsystem;

import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.friend.testingsystem.activity.course.CourseActivity;
import com.friend.testingsystem.activity.section.SectionActivity;
import com.friend.testingsystem.activity.section_element.SectionElementActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onData;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static org.hamcrest.Matchers.anything;

@RunWith(AndroidJUnit4.class)
public class SectionElementActivityTest {

    @Rule
    public final ActivityTestRule<CourseActivity> courseActivityRule = new ActivityTestRule<>(CourseActivity.class);

    @Before
    public void setUp() {
        Intents.init();
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    @Test
    public void testDownloadSectionElement() throws InterruptedException {
        Thread.sleep(500);
        clickCourseItem();
        clickSectionItem();

        downloadSectionElement();
    }

    private void clickCourseItem() throws InterruptedException {
        onData(anything()).inAdapterView(withId(R.id.course_list)).atPosition(1).perform(click());
    }

    private void clickSectionItem() {
        Intents.intended(hasComponent(SectionActivity.class.getName()));
        onData(anything()).inAdapterView(withId(R.id.section_list)).atPosition(1).perform(click());
    }

    private void downloadSectionElement() throws InterruptedException {
        Intents.intended(hasComponent(SectionElementActivity.class.getName()));
        onData(anything())
                .inAdapterView(withId(R.id.section_element_list))
                .atPosition(6)
                .onChildView(withId(R.id.section_element_download_image_container))
                .perform(click());
        Thread.sleep(500);
        onView(withId(R.id.dialog_positive_button)).check(matches(isDisplayed()));
        onView(withId(R.id.dialog_positive_button)).perform(click());
    }
}
