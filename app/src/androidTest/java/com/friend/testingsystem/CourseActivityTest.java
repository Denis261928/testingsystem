package com.friend.testingsystem;

import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.friend.testingsystem.activity.course.CourseActivity;

import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.closeSoftKeyboard;
import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class CourseActivityTest {
    @Rule
    public final ActivityTestRule<CourseActivity> courseActivityRule = new ActivityTestRule<>(CourseActivity.class);

    @Before
    public void setUp() {
        Intents.init();
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    @Test
    public void testCreateCourse() throws InterruptedException {
        Thread.sleep(500);
        onView(withId(R.id.create_course_button)).perform(click());
        Thread.sleep(500);
        onView(withText("Створення курсу")).check(matches(isDisplayed()));
        onView(withId(R.id.course_name_edit_text)).perform(typeText("New course"));
        closeSoftKeyboard();
        onView(withId(R.id.create_course_button)).perform(click());
        Thread.sleep(500);
    }
}
