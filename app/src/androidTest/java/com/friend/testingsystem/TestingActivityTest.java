package com.friend.testingsystem;

import android.view.View;

import androidx.test.espresso.UiController;
import androidx.test.espresso.ViewAction;
import androidx.test.espresso.action.ViewActions;
import androidx.test.espresso.assertion.ViewAssertions;
import androidx.test.espresso.contrib.RecyclerViewActions;
import androidx.test.espresso.intent.Intents;
import androidx.test.rule.ActivityTestRule;
import androidx.test.runner.AndroidJUnit4;

import com.friend.testingsystem.activity.category.CategoryActivity;
import com.friend.testingsystem.activity.sub_category.SubCategoryActivity;
import com.friend.testingsystem.activity.test.TestActivity;
import com.friend.testingsystem.activity.test.fragment.usual_test.UsualTestFragment;
import com.friend.testingsystem.activity.test.fragment.usual_test.presenter.UsualTestPresenter;
import com.friend.testingsystem.activity.topic.TopicActivity;

import org.hamcrest.Matcher;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.swipeUp;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.intent.matcher.IntentMatchers.hasComponent;
import static androidx.test.espresso.matcher.ViewMatchers.isCompletelyDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@RunWith(AndroidJUnit4.class)
public class TestingActivityTest {

    @Rule
    public final ActivityTestRule<CategoryActivity> categoryActivityRule = new ActivityTestRule<>(CategoryActivity.class);
    @Rule
    public final ActivityTestRule<TestActivity> testActivityRule = new ActivityTestRule<>(TestActivity.class);

    @Before
    public void setUp() {
        Intents.init();
    }

    @After
    public void tearDown() {
        Intents.release();
    }

    @Test
    public void testOpenTestHelp() throws InterruptedException {
        Thread.sleep(500);
        clickCategoryItem();
        clickSubCategoryItem();
        clickTopicItem();

        openTestHelp();
    }

    @Test
    public void testPerformTesting() throws InterruptedException {
        Thread.sleep(500);
        clickCategoryItem();
        clickSubCategoryItem();
        clickTopicItem();

        performTest();
    }

    private void clickCategoryItem() {
        onView(withId(R.id.categories_list))
                .perform(RecyclerViewActions.actionOnItemAtPosition(1,
                        MyViewAction.clickChildViewWithId(R.id.category_item))
                );
    }

    private void clickSubCategoryItem() {
        Intents.intended(hasComponent(SubCategoryActivity.class.getName()));
        onView(withId(R.id.sub_categories_list))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0,
                        MyViewAction.clickChildViewWithId(R.id.sub_category_item))
                );
    }

    private void clickTopicItem() {
        Intents.intended(hasComponent(TopicActivity.class.getName()));
        onView(withId(R.id.topics_list))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0,
                        MyViewAction.clickChildViewWithId(R.id.topic_item))
                );
    }

    private void openTestHelp() throws InterruptedException {
        Intents.intended(hasComponent(TestActivity.class.getName()));
        onView(withId(R.id.test_numbers_list))
                .perform(RecyclerViewActions.actionOnItemAtPosition(5,
                        MyViewAction.clickChildViewWithId(R.id.test_number_container))
                );
        onView(allOf(withId(R.id.show_help_icon), isCompletelyDisplayed())).perform(click());
        Thread.sleep(500);
        onView(withText("Підсказка")).check(matches(isDisplayed()));
        Thread.sleep(1000);
    }

    private void performTest() throws InterruptedException{
        Intents.intended(hasComponent(TestActivity.class.getName()));

        for (int i = 0; i < 16; i++) {
            onView(allOf(withId(R.id.right_direction_tab), isCompletelyDisplayed())).perform(click());
            onView(allOf(withId(R.id.usual_test_scroll_view), isCompletelyDisplayed())).check(matches(isCompletelyDisplayed()));
            onView(allOf(withId(R.id.usual_test_scroll_view), isCompletelyDisplayed())).perform(swipeUp());
            testActivityRule.getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    UsualTestFragment usualTestFragment = testActivityRule.getActivity().getCurrentItem();
                    UsualTestPresenter presenter = usualTestFragment.getPresenter();
                    int answerNumber = (int) (Math.random()*4+0);
                    presenter.answerClick(answerNumber);
                }
            });
            Thread.sleep(500);
        }
        onView(allOf(withId(R.id.menu_tests_tab), isCompletelyDisplayed())).perform(click());
        Thread.sleep(500);
        onView(allOf(withId(R.id.test_list_scroll_view), isCompletelyDisplayed())).perform(swipeUp());
        onView(allOf(withId(R.id.finish_test), isCompletelyDisplayed())).perform(click());
        onView(withText("Ви впервнені що хочите завершити тест?")).check(ViewAssertions.matches(isDisplayed()));
        onView(withId(R.id.dialog_positive_button)).perform(ViewActions.click());
        Thread.sleep(500);
        onView(withId(R.id.test_score_container)).check(ViewAssertions.matches(isDisplayed()));
        Thread.sleep(1000);
    }

    public static class MyViewAction {

        public static ViewAction clickChildViewWithId(final int id) {
            return new ViewAction() {
                @Override
                public Matcher<View> getConstraints() {
                    return null;
                }

                @Override
                public String getDescription() {
                    return "Click on a child view with specified id.";
                }

                @Override
                public void perform(UiController uiController, View view) {
                    View v = view.findViewById(id);
                    v.performClick();
                }
            };
        }

    }
}
